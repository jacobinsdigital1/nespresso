<!-- GWP BY PRODUCT PROMO -->
<form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
    method="post"
    role="form"
    id="form-coy-promo-pages"
>

    <div class="panel panel-default">

        <div class="panel-body">

            <input type="hidden" name="action" value="nespresso_coys_promo">

            <input type="hidden" name="type" value="update-or-create">

            <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

            <h2>GWP Promo</h2>
            <button type="button" id="add_more_coy" class="btn btn-success btn-sm btn-flat pull-right">Add More</button>
            <br>
            <div class="form-group">
                <label for="coys_promo_status"> Status:</label>
                <br>
                <select name="coys_promo_status" class="form-control" required>
                    <option value="Inactive" <?php echo (isset($coys_promo['coys_promo_status']) && $coys_promo['coys_promo_status'] == 'Inactive') ? 'selected' : '' ?> >Inactive</option>
                    <option value="Active" <?php echo (isset($coys_promo['coys_promo_status']) && $coys_promo['coys_promo_status'] == 'Active') ? 'selected' : '' ?> >Active</option>
                </select>
            </div>
            <div class="coy-container">
                <?php
                    $main_product = [];
                    if (isset($coys_promo['main_product'])) {
                        $main_product = json_decode($coys_promo['main_product']);
                        $conditional_product = json_decode($coys_promo['conditional_product']);
                        $gift = json_decode($coys_promo['gift']);
                        $conditional_product_quantity = isset($coys_promo['conditional_product_quantity']) ? json_decode($coys_promo['conditional_product_quantity']) : [];
                    }
                ?>
                <?php if ($main_product):
                        foreach ($main_product as $key => $value):?>
                            <div id="coy-promo-<?= $key ?>">
                                <?php if ($key != 0 ): ?>
                                    <hr>
                                    <button type="button" data-id="<?= $key ?>" class="btn btn-danger btn-sm btn-flat pull-right remove_coy">Remove</button>
                                    <br>
                                <?php endif;?>
                                <div class="form-group">
                                    <label for="main_product">Main Product SKU Number <small>e.g ITESSE001A1</small></label>
                                    <br>
                                    <input type="text" class="form-control" name="main_product[]" placeholder="ITESSE001A1" value="<?= $value ?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="conditional_product">Conditional Products SKU Number (Separated by comma): <small>e.g ITESSE001A1, ITESSE099A</small></label>
                                    <br>
                                    <textarea class="form-control" name="conditional_product[]" placeholder="ITESSE001A1, ITESSE099A" required><?= $conditional_product[$key] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="conditional_product">Conditional Products Quantity: </label>
                                    <br>
                                    <input type="number" class="form-control" name="conditional_product_quantity[]" value="<?= @$conditional_product_quantity[$key] ?>" >
                                </div>
                                <div class="form-group">
                                    <label for="gift[]">Gift SKU Number <small>e.g ITESSE001A1</small></label>
                                    <br>
                                    <input type="text" class="form-control" name="gift[]" required placeholder="ITESSE001A1" value="<?= $gift[$key] ?>">
                                </div>
                            </div>
                        <?php endforeach;?>
                <?php else: ?>
                    <div class="form-group">
                        <label for="main_product">Main Product SKU Number <small>e.g ITESSE001A1</small></label>
                        <br>
                        <input type="text" class="form-control" name="main_product[]" placeholder="ITESSE001A1" value="" required>
                    </div>
                    <div class="form-group">
                        <label for="conditional_product">Conditional Products SKU Number (Separated by comma): <small>e.g ITESSE001A1, ITESSE099A</small></label>
                        <br>
                        <textarea class="form-control" name="conditional_product[]" placeholder="ITESSE001A1, ITESSE099A" required></textarea>
                    </div>
                    <div class="form-group">
                        <label for="conditional_product">Conditional Products Quantity: </label>
                        <br>
                        <input type="number" class="form-control" name="conditional_product_quantity[]" value="">
                    </div>
                    <div class="form-group">
                        <label for="gift[]">Gift SKU Number <small>e.g ITESSE001A1</small></label>
                        <br>
                        <input type="text" class="form-control" name="gift[]" required placeholder="ITESSE001A1" value="">
                    </div>
                <?php endif; ?>

            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat">
                    Submit
                </button>
            </div>

        </div><!-- .panel-body -->

    </div><!-- .panel -->
</form>
<!--END: COYS PRODUCT PROMO -->

<!-- COYS PRODUCT DISCOUNT -->
<!-- <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
    method="post"
    role="form"
    id="form-coy-discount-pages"
>

    <div class="panel panel-default">

        <div class="panel-body">

            <input type="hidden" name="action" value="nespresso_coys_discount">

            <input type="hidden" name="type" value="update-or-create">

            <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

            <h4>COYS Discount</h4>
            <br>
            <div class="form-group">
                <label for="coys_discount_status"> Status:</label>
                <br>
                <select name="coys_discount_status" class="form-control" required>
                    <option value="Inactive" <?php echo (isset($coys_discount['coys_discount_status']) && $coys_discount['coys_discount_status'] == 'Inactive') ? 'selected' : '' ?> >Inactive</option>
                    <option value="Active" <?php echo (isset($coys_discount['coys_discount_status']) && $coys_discount['coys_discount_status'] == 'Active') ? 'selected' : '' ?> >Active</option>
                </select>
            </div>

            <div class="form-group">
                <label for="discount_label">Discount Label</label>
                <input type="text" class="form-control" name="discount_label" required placeholder="COYS Discount" value="<?= @$coys_discount['discount_label'] ?>">
            </div>

            <div class="form-group">
                <label for="discounted_product">Discounted Product </label>
                <br>
                <input type="text" class="form-control" name="discounted_product" required placeholder="ITESSE099A" value="<?= @$coys_discount['discounted_product'] ?>">
            </div>

            <div class="form-group">
                <label for="conditional_product">Conditional Product SKU Number Separated by comma: <small>e.g ITESSE001A1</small></label>
                <br>
                <textarea class="form-control" name="conditional_product" placeholder="ITESSE001A1, ITESSE099A" required><?= @$coys_discount['conditional_product'] ?></textarea>
            </div>
            <div class="form-group">
                <label for="coys_discount_type">Discount Type </label>
                <select name="coys_discount_type" class="form-control" required>
                    <option value="Percentage" <?php echo (isset($coys_discount['coys_discount_type']) && $coys_discount['coys_discount_type'] == 'Percentage') ? 'selected' : '' ?> >Percentage</option>
                    <option value="Fixed Amount" <?php echo (isset($coys_discount['coys_discount_type']) && $coys_discount['coys_discount_type'] == 'Fixed Amount') ? 'selected' : '' ?> >Fixed Amount</option>
                </select>
            </div>
            <div class="form-group">
                <label for="coys_amount_percentage">Fixed Amount/Percentage</label>
                <input type="number" class="form-control" name="coys_amount_percentage" required placeholder="10" value="<?= @$coys_discount['coys_amount_percentage'] ?>">
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-flat">
                    Submit
                </button>
            </div>
        </div>
    </div>
</form> -->

<!-- END: COYS PRODUCT DISCOUNT -->
<script type="text/javascript">
    jQuery(function () {
        // coy
        var coy_count = jQuery('.remove_coy').length + 2;
        jQuery('#add_more_coy').on('click',function () {
            jQuery('.coy-container').append(
                '<div id="coy-promo-'+coy_count+'">'+
                    '<hr>'+
                    '<button type="button" data-id="'+coy_count+'" class="btn btn-danger btn-sm btn-flat pull-right remove_coy">Remove</button>'+
                    '<br>'+
                    '<div class="form-group">'+
                        '<label for="main_product">Main Product SKU Number <small>e.g ITESSE001A1</small></label>'+
                        '<br>'+
                        '<input type="text" class="form-control" name="main_product[]" placeholder="ITESSE001A1" value="" required>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="conditional_product">Conditional Products SKU Number (Separated by comma): <small>e.g ITESSE001A1, ITESSE099A</small></label>'+
                        '<br>'+
                        '<textarea class="form-control" name="conditional_product[]" placeholder="ITESSE001A1, ITESSE099A" required></textarea>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="conditional_product">Conditional Products Quantity: </label>'+
                        '<br>'+
                        '<input type="number" class="form-control" name="conditional_product_quantity[]" >'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label for="gift">Gift SKU Number <small>e.g ITESSE001A1</small></label>'+
                        '<br>'+
                        '<input type="text" class="form-control" name="gift[]" placeholder="ITESSE001A1" value=""> required'+
                    '</div>'+
                '</div>'
            );
        });
        jQuery(document).on('click','.remove_coy', function(){
            var r = confirm("Are you sure you want to delete this?");
            if (r == true) {
                var data_id = jQuery(this).data('id');
                jQuery('#coy-promo-'+data_id).remove();
            }
        });
    })
</script>
