
/**
 * Product list
 */
$(document).ready(function() {

    $('.discovery-offers__list .discovery-offers__list-item' ).on('click', function () {
        var discovery_offer_tab = $(this).data('tab');
        $('.discovery-offers__list-item').removeClass('ui-tabs-active ui-state-active');
        $('.main-product').addClass('hide');
        $('#' + discovery_offer_tab).removeClass('hide');
        $(this).addClass('ui-tabs-active ui-state-active');
    });

    $('.discovery-offer-gift__block').on('click', function () {
        var gift_id = $(this).data('gift');
        $("#" + gift_id).prop("checked", true)
        $('.discovery-offer-gift__block').removeClass('active');
        $(this).addClass('active');
        $("#addDiscoveryOfferButton").removeClass('disabled').removeAttr('disabled');
    });

    $("#addDiscoveryOfferButton").on('click', function() {
        if (!$(this).hasClass('disabled')) {
            var discovery_id = $('ul#discover-products').find('li.ui-tabs-active').data('tab');
            var discovery_name = $('ul#discover-products').find('li.ui-tabs-active').data('name');
            var discovery_img = $('ul#discover-products').find('li.ui-tabs-active').data('img');
            var gift_id = $('input[name=productCodes]:checked').val();
            var gift_name = $('#'+gift_id).data('name');
            var gift_img = $('#'+gift_id).data('img');
            
            //discovery id
            var discovery_item = {
                id: discovery_id,
                name: discovery_name,
                image_url: discovery_img,
                price: 0,
                type: 'Coffee',
                quantity: 1
            };
            
            Products.add(discovery_item);
            launchToBasket(discovery_item,1);
            resetQtySelector();

            //gift id
            var gift_item = {
                id: gift_id,
                name: gift_name,
                image_url: gift_img,
                price: 0,
                type: 'Discovery Gift',
                quantity: 1
            };

            //check gift stock
            var gift_data = {
                action: 'check_product_stock',
                product_id: gift_item.id
            };

            $.ajax({
                dataType: 'json',
                type: 'post',
                url: ajaxurl,
                data: gift_data,
                dataType: 'json',
                error: function (jqXHR, textStatus, errorThrown) {
                    // console.log(errorThrown);
                    // return false;
                },
                success: function (data) {
                    if(data.status) {
                        var data = {
                            action: 'add_to_cart', 
                            products: [gift_item]
                        };
                        $.ajax({
                            dataType: 'json',
                            type: 'post',   
                            url: ajaxurl,
                            data: data,
                            error: function (jqXHR, textStatus, errorThrown) {
                                // console.log(errorThrown);
                                // return false; 
                            },
                            success: function (data) {
                                if ( data.status != 'failed') {
                                    
                                    if (typeof generateShoppingBag == 'function'){
                                        generateShoppingBag(data); //refresh shopping bag : shopping-bag.php
                                    }

                                    if (typeof generateMinibasket == 'function'){

                                        generateMinibasket(); //refresh minibasket : header.php

                                    } // endif

                                    updateBasketItems();
                                    resetQtySelector();
                                    setTimeout( function() { window.location.replace("/checkout"); }, 4000 );
                                }
                            } // success()

                        }); // ajax()
                    }

                } // success()

            });
        }
    });

});