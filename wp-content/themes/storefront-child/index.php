<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


get_header();

?>

<?php get_header(); ?>

<!-- content -->

<?php get_template_part('components/home'); ?>

<?php //get_template_part('../storefront/content'); ?>

<?php if ( have_posts() ) :

	get_template_part( 'loop' );

else :

	get_template_part( 'content', 'none' );

endif; ?>

<!-- /content -->

<!-- get_footer -->
<?php get_footer(); ?>
