<script type="text/html" id="minibasket-line">

    <li class="basket-list-item">
        <div class="basket-product-image">
            <img data-src="image_url" data-alt="name"/>
        </div>
        <div class="basket-product-title">
            <span data-content="name"></span>
            <span class="span">
                <strong>
                    <span data-content="currency_symbol"></span>
                    <span data-content="total_price"></span>
                </strong>
                &nbsp;
                (<span data-content="quantity"></span> X <span data-content="currency_symbol"></span><span data-content="price_formatted"></span>)
            </span>
        </div>
        <a href="javascript:void(0)"
            class="basket-product-close"
            data-cart-remove="true"
            data-id="cart_item_key"
            data-class="discovery_offer"
        ><i class="icon icon-Picto_croisrefermer"></i> <span class="hidden">close</span></a>
    </li>

</script>