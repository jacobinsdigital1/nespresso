<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$products = get_coffee_products();
$original_origins = [
'Arpeggio Decaffeinato',
'Volluto Decaffeinato',
'Ristretto Decaffeinato',
'Vivalto Lungo Decaffeinato',
'Volluto',
'Livanto',
'Cosi',
'Capriccio',
'Dharkan',
'Arpeggio',
'Kazaar',
'Ristretto',
'Roma',
'Fortissio Lungo',
'Envivo Lungo',
'Vivalto Lungo',
'Nicaragua',
'India',
'Indonesia',
'Ethiopia',
'Colombia',
'Caramelito',
'Ciocattino',
'Vanilio'
];

global $scripts;
$scripts[] = 'js/components/coffee-origins.js';

?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/coffee-origins.css">
<?php get_template_part('components/generics/qty-selector');?>
<main id="main" class="fullPage fullPage__plp coffee-list">
	<div class="vue v_coffeeOrigins">
		<section id="introduction" class="vue_introduction v_parallax" pv-height="auto" id="introduction" data-label="Introduction" style="height: 472px;">
			<div class="bg_container v_parallaxLayer">
				<div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/introduction_placeholder_L.jpg');"></div>
				<div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/introduction_XL.jpg');" lazy="loaded"></div>
			</div>
			<div class="v_sectionRestrict">
				<div class="v_sectionContent">
					<header>
						<div class="v_cell">
							<div class="v_menu v_menuDropdown">
								<p class="v_visually_hidden">Navigation to other resources</p>
								<div class="v_subMenu">
									<div class="v_subMenuCell">
										<ul>
											<li>
												<a href="/coffee-range"><span>Discover Our Coffee Range</span></a>
											</li>
											<li>
												<a class="v_active" href="/coffee-origins"><span>Coffee Origins</span></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</header>
					<article>
						<div class="v_articleContent">
							<h2 data-wow="" class="wow">Coffee Origins</h2>
							<div class="v_wysiwyg wow" data-wow="" data-wow-delay="1">
								Perfect coffee is no coincidence. Which means we push the boundaries uncompromisingly at every step of the long value chain from tree to cup, deliberately creating our <strong><em>coffee</em></strong> for your pleasure.
							</div>
							<div class="v_buttonContainer wow" data-wow="" data-wow-delay="2">
								<a href="#come_from" class="v_btnRoundM" style="opacity: 0.5;">
									<i class="fn_angleDownCircle"></i>
									<span class="v_visually_hidden">Go to the Origins section</span>
								</a>
							</div>
						</div>
					</article>
				</div>
			</div>
		</section>
		<div>
			<section class="vue_comeFrom v_sectionLight" id="come_from" data-label="Coffees come from">
			    <div class="bg_container">
			        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/map_placeholder_L.jpg');"></div>
			        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/map_XL.jpg');" lazy="loaded"></div>
			    </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2>Where does <strong class="v_brand" term="nespresso">Nespresso</strong> Coffee come from?</h2>
			            <div class="coffee-map">
			                <div class="worldmap">
			                    <div class="worldmap-container">
			                        <svg id="map-svg" viewBox="0 0 996 450" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMin meet">
			                            <clipPath id="clip">
			                                <use xlink:href="#Fill-1" x="0" y="0"></use>
			                                <use xlink:href="#Fill-2" x="0" y="0"></use>
			                                <use xlink:href="#Fill-3" x="0" y="0"></use>
			                                <use xlink:href="#Fill-4" x="0" y="0"></use>
			                                <use xlink:href="#Fill-5" x="0" y="0"></use>
			                                <use xlink:href="#Fill-6" x="0" y="0"></use>
			                                <use xlink:href="#Fill-7" x="0" y="0"></use>
			                                <use xlink:href="#Fill-8" x="0" y="0"></use>
			                                <use xlink:href="#Fill-9" x="0" y="0"></use>
			                                <use xlink:href="#Fill-10" x="0" y="0"></use>
			                                <use xlink:href="#Fill-11" x="0" y="0"></use>
			                                <use xlink:href="#Fill-12" x="0" y="0"></use>
			                                <use xlink:href="#Fill-13" x="0" y="0"></use>
			                                <use xlink:href="#Fill-14" x="0" y="0"></use>
			                                <use xlink:href="#Fill-15" x="0" y="0"></use>
			                            </clipPath>
			                            <g id="map">
			                                <use xlink:href="#Fill-1" x="0" y="0"></use>
			                                <use xlink:href="#Fill-2" x="0" y="0"></use>
			                                <use xlink:href="#Fill-3" x="0" y="0"></use>
			                                <use xlink:href="#Fill-4" x="0" y="0"></use>
			                                <use xlink:href="#Fill-5" x="0" y="0"></use>
			                                <use xlink:href="#Fill-6" x="0" y="0"></use>
			                                <use xlink:href="#Fill-7" x="0" y="0"></use>
			                                <use xlink:href="#Fill-8" x="0" y="0"></use>
			                                <use xlink:href="#Fill-9" x="0" y="0"></use>
			                                <use xlink:href="#Fill-10" x="0" y="0"></use>
			                                <use xlink:href="#Fill-11" x="0" y="0"></use>
			                                <use xlink:href="#Fill-12" x="0" y="0"></use>
			                                <use xlink:href="#Fill-13" x="0" y="0"></use>
			                                <use xlink:href="#Fill-14" x="0" y="0"></use>
			                                <use xlink:href="#Fill-15" x="0" y="0"></use>
			                            </g>
			                            <g class="lines">
			                                <path class="line map-stroke-transition map-path path-brazil-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,2,1,0" id="line-YqaucG7m" d="M  275 320 C 285 420 55.333333333333336 420 55.333333333333336 500"></path>
			                            	<path class="line map-stroke-transition map-path path-brazil-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-qAMopLl6" d="M  275 320 C 285 420 166 420 166 500"></path>
										    <path class="line map-stroke-transition map-path path-brazil-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-2RPx44cI" d="M  275 320 C 285 420 276.6666666666667 420 276.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-brazil-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-hKr5R8KN" d="M  275 320 C 285 420 387.3333333333333 420 387.3333333333333 500"></path>
    										<path class="line map-stroke-transition map-path path-brazil-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1,1,0" id="line-LZoB264W" d="M  275 320 C 285 420 498 420 498 500"></path>
    										<path class="line map-stroke-transition map-path path-brazil-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,6,1,0" id="line-Y7LuVoaO" d="M  275 320 C 285 420 608.6666666666667 420 608.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-brazil-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,3,1,0" id="line-PxqGEr04" d="M  275 320 C 285 420 719.3333333333334 420 719.3333333333334 500"></path>
    										<path class="line map-stroke-transition map-path path-brazil-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,3,1,0" id="line-tTh5duCq" d="M  275 320 C 285 420 830.0000000000001 420 830.0000000000001 500"></path>
    										<path class="line map-stroke-transition map-path path-brazil-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,4,1,0" id="line-vV80cuj7" d="M  275 320 C 285 420 940.6666666666667 420 940.6666666666667 500"></path>

    										<path class="line map-stroke-transition map-path path-guatemala-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,11,1,0" id="line-hyvluDzB" d="M  105 182 C 115 351 55.333333333333336 351 55.333333333333336 500"></path>
										    <path class="line map-stroke-transition map-path path-guatemala-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-IRlOXSUd" d="M  105 182 C 115 351 166 351 166 500"></path>
    										<path class="line map-stroke-transition map-path path-guatemala-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,8,1,0" id="line-5XGalYxj" d="M  105 182 C 115 351 276.6666666666667 351 276.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-guatemala-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,3,1,0" id="line-ooCnE4e4" d="M  105 182 C 115 351 387.3333333333333 351 387.3333333333333 500"></path>
    										<path class="line map-stroke-transition map-path path-guatemala-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-GtRhORQp" d="M  105 182 C 115 351 498 351 498 500"></path>
    										<path class="line map-stroke-transition map-path path-guatemala-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,2,1,0" id="line-c9nlLCJG" d="M  105 182 C 115 351 608.6666666666667 351 608.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-guatemala-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,9,1,0" id="line-aNYYjzzu" d="M  105 182 C 115 351 719.3333333333334 351 719.3333333333334 500"></path>
    										<path class="line map-stroke-transition map-path path-guatemala-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,5,1,0" id="line-gZHWlaM9" d="M  105 182 C 115 351 830.0000000000001 351 830.0000000000001 500"></path>
    										<path class="line map-stroke-transition map-path path-guatemala-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,4,1,0" id="line-lCS5nnVf" d="M  105 182 C 115 351 940.6666666666667 351 940.6666666666667 500"></path>

    										<path class="line map-stroke-transition map-path path-costa_rica-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,5,1,0" id="line-KqxfS9rl" d="M  128 208 C 138 364 55.333333333333336 364 55.333333333333336 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,8,1,0" id="line-1OzvHFOr" d="M  128 208 C 138 364 166 364 166 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,11,1,0" id="line-OodzFb1m" d="M  128 208 C 138 364 276.6666666666667 364 276.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,6,1,0" id="line-3aWoTPRY" d="M  128 208 C 138 364 387.3333333333333 364 387.3333333333333 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,2,1,0" id="line-9c7iFIIG" d="M  128 208 C 138 364 498 364 498 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,5,1,0" id="line-pPhpu0g7" d="M  128 208 C 138 364 608.6666666666667 364 608.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1" id="line-XLDcvpqR" d="M  128 208 C 138 364 719.3333333333334 364 719.3333333333334 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-VaURM350" d="M  128 208 C 138 364 830.0000000000001 364 830.0000000000001 500"></path>
    										<path class="line map-stroke-transition map-path path-costa_rica-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,9,1,0" id="line-ZnrlKtfa" d="M  128 208 C 138 364 940.6666666666667 364 940.6666666666667 500"></path>

    										<path class="line map-stroke-transition map-path path-indonesia-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-J1Zaj2Hp" d="M  833 255 C 843 387.5 55.333333333333336 387.5 55.333333333333336 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-f494zaH7" d="M  833 255 C 843 387.5 166 387.5 166 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-n4PbGUbH" d="M  833 255 C 843 387.5 276.6666666666667 387.5 276.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,3,1,0" id="line-SG82KtTx" d="M  833 255 C 843 387.5 387.3333333333333 387.5 387.3333333333333 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,11,1,0" id="line-LpZQia0m" d="M  833 255 C 843 387.5 498 387.5 498 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1" id="line-vrkMpN08" d="M  833 255 C 843 387.5 608.6666666666667 387.5 608.6666666666667 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,6,1,0" id="line-qWKlbyhZ" d="M  833 255 C 843 387.5 719.3333333333334 387.5 719.3333333333334 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-FkhxQqgn" d="M  833 255 C 843 387.5 830.0000000000001 387.5 830.0000000000001 500"></path>
    										<path class="line map-stroke-transition map-path path-indonesia-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-ySsAJ9wb" d="M  833 255 C 843 387.5 940.6666666666667 387.5 940.6666666666667 500"></path>

 											<path class="line map-stroke-transition map-path path-mexico-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1,1,0" id="line-pTYu4wzY" d="M  70 160 C 80 340 55.333333333333336 340 55.333333333333336 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,5,1,0" id="line-kKFFENGf" d="M  70 160 C 80 340 166 340 166 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,8,1,0" id="line-0zMWkQHC" d="M  70 160 C 80 340 276.6666666666667 340 276.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,7,1,0" id="line-KXTt8Uqx" d="M  70 160 C 80 340 387.3333333333333 340 387.3333333333333 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,4,1,0" id="line-4mwipIzM" d="M  70 160 C 80 340 498 340 498 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,6,1,0" id="line-YUxMJMKx" d="M  70 160 C 80 340 608.6666666666667 340 608.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1" id="line-qsIFG5U0" d="M  70 160 C 80 340 719.3333333333334 340 719.3333333333334 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-SIPcDD7h" d="M  70 160 C 80 340 830.0000000000001 340 830.0000000000001 500"></path>
 											<path class="line map-stroke-transition map-path path-mexico-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,8,1,0" id="line-4rOpxgFO" d="M  70 160 C 80 340 940.6666666666667 340 940.6666666666667 500"></path>

 											<path class="line map-stroke-transition map-path path-colombia-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,5,1,0" id="line-0qs3jQLI" d="M  170 242 C 180 381 55.333333333333336 381 55.333333333333336 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-y6K4cqUx" d="M  170 242 C 180 381 166 381 166 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,11,1,0" id="line-K0GLKkIF" d="M  170 242 C 180 381 276.6666666666667 381 276.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1,1,0" id="line-egt8vbu9" d="M  170 242 C 180 381 387.3333333333333 381 387.3333333333333 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-4AAeTxyq" d="M  170 242 C 180 381 498 381 498 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1" id="line-IEaMU4xA" d="M  170 242 C 180 381 608.6666666666667 381 608.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,9,1,0" id="line-prQOTEV0" d="M  170 242 C 180 381 719.3333333333334 381 719.3333333333334 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,7,1,0" id="line-iQAm7nYi" d="M  170 242 C 180 381 830.0000000000001 381 830.0000000000001 500"></path>
 											<path class="line map-stroke-transition map-path path-colombia-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,7,1,0" id="line-gvUoyptS" d="M  170 242 C 180 381 940.6666666666667 381 940.6666666666667 500"></path>

 											<path class="line map-stroke-transition map-path path-kenya-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,5,1,0" id="line-jE15yzIf" d="M  587 250 C 597 385 55.333333333333336 385 55.333333333333336 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,10,1,0" id="line-5bCc2hQA" d="M  587 250 C 597 385 166 385 166 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,6,1,0" id="line-HTIosSZP" d="M  587 250 C 597 385 276.6666666666667 385 276.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-nCCSoe1v" d="M  587 250 C 597 385 387.3333333333333 385 387.3333333333333 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,11,1,0" id="line-eGJW54Ay" d="M  587 250 C 597 385 498 385 498 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,4" id="line-B47KtezU" d="M  587 250 C 597 385 608.6666666666667 385 608.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,4,1,0" id="line-mqM3hAzY" d="M  587 250 C 597 385 719.3333333333334 385 719.3333333333334 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1,1,0" id="line-rQegOmiL" d="M  587 250 C 597 385 830.0000000000001 385 830.0000000000001 500"></path>
 											<path class="line map-stroke-transition map-path path-kenya-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1" id="line-3uBvIGX3" d="M  587 250 C 597 385 940.6666666666667 385 940.6666666666667 500"></path>

 											<path class="line map-stroke-transition map-path path-india-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,2,1,0" id="line-jUwutZGA" d="M  738 170 C 748 345 55.333333333333336 345 55.333333333333336 500"></path>
 											<path class="line map-stroke-transition map-path path-india-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,7,1,0" id="line-dz8CT7cB" d="M  738 170 C 748 345 166 345 166 500"></path>
 											<path class="line map-stroke-transition map-path path-india-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,2,1,0" id="line-8yXPmAsP" d="M  738 170 C 748 345 276.6666666666667 345 276.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-india-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,3,1,0" id="line-ieu6jf31" d="M  738 170 C 748 345 387.3333333333333 345 387.3333333333333 500" class="line map-stroke-transition map-path"></path>
 											<path class="line map-stroke-transition map-path path-india-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-OrmiDMf0" d="M  738 170 C 748 345 498 345 498 500"></path>
 											<path class="line map-stroke-transition map-path path-india-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-u2vD8ski" d="M  738 170 C 748 345 608.6666666666667 345 608.6666666666667 500"></path>
 											<path class="line map-stroke-transition map-path path-india-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,5,1,0" id="line-rz36VF7M" d="M  738 170 C 748 345 719.3333333333334 345 719.3333333333334 500"></path>
 											<path class="line map-stroke-transition map-path path-india-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,7,1,0" id="line-JRJWObLF" d="M  738 170 C 748 345 830.0000000000001 345 830.0000000000001 500"></path>
 											<path class="line map-stroke-transition map-path path-india-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,11,1,0" id="line-gHGqzngx" d="M  738 170 C 748 345 940.6666666666667 345 940.6666666666667 500"></path>

											<path class="line map-stroke-transition map-path path-ethiopia-1" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,9,1,0" id="line-tYQBTk5D" d="M  595 207 C 605 363.5 55.333333333333336 363.5 55.333333333333336 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-2" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,4,1,0" id="line-rTAanhIE" d="M  595 207 C 605 363.5 166 363.5 166 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-3" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,3,1,0" id="line-eMI2eSQF" d="M  595 207 C 605 363.5 276.6666666666667 363.5 276.6666666666667 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-4" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,1" id="line-FAYc6kKs" d="M  595 207 C 605 363.5 387.3333333333333 363.5 387.3333333333333 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-5" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,2,1,0" id="line-KTFino4I" d="M  595 207 C 605 363.5 498 363.5 498 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-6" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,6,1,0" id="line-BQCi7sVu" d="M  595 207 C 605 363.5 608.6666666666667 363.5 608.6666666666667 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-7" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,0" id="line-TFtP1tUn" d="M  595 207 C 605 363.5 719.3333333333334 363.5 719.3333333333334 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-8" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,2,1,0" id="line-c7XDIM8A" d="M  595 207 C 605 363.5 830.0000000000001 363.5 830.0000000000001 500"></path>
											<path class="line map-stroke-transition map-path path-ethiopia-9" stroke-dasharray="1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,12,1,9,1,0" id="line-bcjGVmqp" d="M  595 207 C 605 363.5 940.6666666666667 363.5 940.6666666666667 500"></path>

			                            </g>

			                            <g class="tooltips">

			                                <svg class="marker-on map_mark_on mark_on_brazil" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="256" y="268">
			                                    <g class="map_first_g first_g_brazil" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_brazil" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_brazil" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_brazil" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_brazil" x="55" y="33" style="fill-opacity: 0;">Brazil</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on mark_on_guatemala" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="86" y="130">
			                                    <g class="map_first_g first_g_guatemala" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_guatemala" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_guatemala" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_guatemala" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_guatemala" x="55" y="33" style="fill-opacity: 0;">Guatemala</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on map_mark_on mark_on_costa_rica" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="109" y="156">
			                                    <g class="map_first_g first_g_costa_rica" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_costa_rica" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_costa_rica" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_costa_rica" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_costa_rica" x="55" y="33" style="fill-opacity: 0;">Costa Rica</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on map_mark_on mark_on_indonesia" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="814" y="203">
			                                    <g class="map_first_g first_g_indonesia" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_indonesia" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_indonesia" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_indonesia" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_indonesia" x="55" y="33" style="fill-opacity: 0;">Indonesia</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on map_mark_on mark_on_colombia" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="151" y="190">
			                                    <g class="map_first_g first_g_colombia" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_colombia" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_colombia" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_colombia" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_colombia" x="55" y="33" style="fill-opacity: 0;">Colombia</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on map_mark_on mark_on_mexico" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="51" y="108">
			                                    <g class="map_first_g first_g_mexico" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_mexico" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_mexico" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_mexico" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_mexico" x="55" y="33" style="fill-opacity: 0;">Mexico</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on map_mark_on mark_on_kenya" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="568" y="198">
			                                    <g class="map_first_g first_g_kenya" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_kenya" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_kenya" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_kenya" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_kenya" x="55" y="33" style="fill-opacity: 0;">Kenya</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on map_mark_on mark_on_india" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="719" y="118">
			                                    <g class="map_first_g first_g_india" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_india" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_india" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_india" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_india" x="55" y="33" style="fill-opacity: 0;">India</text>
			                                    </g>
			                                </svg>

			                                <svg class="marker-on map_mark_on mark_on_ethiopia" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" width="180" height="32" x="576" y="155">
			                                    <g class="map_first_g first_g_ethiopia" width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(0.75,0,0,0.75,7.5,16.913015365600586)">
			                                        <g transform="translate(5,69) scale(0.1,-0.1)" fill="#000000" stroke="none" class="bubble map_second_g second_g_ethiopia" style="fill: rgb(255, 255, 255);">
			                                            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
			                                        </g>
			                                        <rect class="map_rect rect_ethiopia" width="60" height="50" x="0" y="0" style="fill: rgb(255, 255, 255);"></rect>
			                                        <g class="map_N N_ethiopia" id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(0, 0, 0);">
			                                            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
			                                            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
			                                        </g>
			                                        <text class="map_text text_ethiopia" x="55" y="33" style="fill-opacity: 0;">Ethiopia</text>
			                                    </g>
			                                </svg>
			                            </g>
			                        </svg>
			                    </div>
			                </div>
			                <div class="v_slider">
			                    <button class="v_sliderArrow v_sliderPrev hide" tabindex="-1" aria-hidden="true" disabled> <i class="fn_angleLeft"></i> </button>
			                    <svg class="capsule-maker marker-on fade-transition marker-active" viewBox="0 0 280 28" preserveAspectRatio="xMidYMin meet" style="left: 55px;" width="180" height="32" x="0" y="0">
								    <g width="100%" height="100%" data-svg-origin="30 67.65206146240234" style="transform-origin: 0px 0px 0px;" transform="matrix(1,0,0,1,26.22222137451172,0)">
								        <g transform="matrix(-0.09999,0,0,-0.09999,148.80762195587158,83.82735328674316)" fill="#000000" stroke="none" class="bubble" style="fill: rgb(0, 0, 0); fill-opacity: 1;" data-svg-origin="1.8453121185302734 13.479412078857422">
								            <path d="M180 681 c-82 -27 -149 -92 -170 -167 -16 -58 -8 -140 20 -195 12 -23 55 -77 95 -119 74 -77 96 -111 110 -165 8 -33 6 -35 36 44 7 19 48 72 90 117 89 94 119 145 126 220 15 142 -84 261 -226 270 -31 2 -67 0 -81 -5z"></path>
								        </g>
								        <rect width="227.55555725097656" height="50" x="0" y="0" style="fill: rgb(0, 0, 0);"></rect>
								        <g id="N" transform="translate(12,10)" stroke="none" stroke-width="1" fill="#ffffff" style="fill: rgb(255, 255, 255);">
								            <path d="M1.96351213,0.718486951 C1.27672314,0.718486951 0.614588103,0.781882858 0,0.913957666 L0,0.788926848 C1.49332582,0.265910612 2.97256366,0 4.65079421,0 C8.50209559,0 12.3886169,1.46338886 14.7923784,3.81784243 C15.3347656,4.27922375 17.3475857,6.33606875 18.0449407,7.17606452 C19.0680802,8.38939175 20.3307153,10.0658613 21.5475645,11.6895009 C22.2889445,12.6774205 23.0549783,13.693516 23.7628993,14.6021907 L23.8210122,14.6761526 L23.8210122,4.49582644 L26.4220054,4.49582644 L26.4220054,24.1556017 C24.2612616,22.3593844 22.1005177,19.3322298 20.1951185,16.6572747 C19.4907195,15.6763991 18.8303455,14.7448315 18.2210404,13.9594266 C18.1805375,13.8995527 13.686472,7.99668932 11.8744057,5.91342937 C10.4321488,4.12601698 7.97203536,2.23118374 6.27443384,1.59018068 C4.84274293,1.02137851 3.43746698,0.718486951 1.96351213,0.718486951 L1.96351213,0.718486951 Z" id="Path"></path>
								            <path d="M34.7937872,27.7075335 C33.2987004,28.2305498 31.6926707,28.4964604 30.0126792,28.4964604 C26.1543338,28.4964604 22.2730955,27.0313105 19.869334,24.678618 C19.3234248,24.2172366 17.4479625,22.1656746 16.7453246,21.3203959 C15.722185,20.1088296 14.4613109,18.4288381 13.2391787,16.8034375 C12.5030817,15.8155179 11.7370479,14.8011834 11.0291269,13.8942697 L10.974536,13.8203078 L10.974536,23.9372381 L8.37002078,23.9372381 L8.37002078,4.34085866 C10.5360476,6.13531504 12.6950305,9.16775262 14.6021907,11.8391857 C15.3065897,12.8200613 15.9669637,13.7533899 16.5709858,14.5387948 C16.6150107,14.5969077 20.9752404,20.5015321 22.7855457,22.586553 C24.2278026,24.3722044 26.687916,26.2670377 28.3837566,26.9027577 C29.8189695,27.4768429 31.3545592,27.7814954 32.8302751,27.7814954 C33.5170641,27.7814954 34.1774381,27.7145775 34.7937872,27.5842637 L34.7937872,27.7075335 Z" id="Path"></path>
								        </g>
								        <text x="55" y="33" style="fill-opacity: 1;">Among others</text>
								    </g>
								</svg>
			                    <div class="v_slideContainer" style="touch-action: pan-y; -moz-user-select: none;">
			                        <div class="v_slide" style="transform: translateX(0);">
			                            <ul aria-hidden="true">
			                            	<?php $counter = 1;
			                            	foreach ($products as $category => $product): ?><?php if ($category != 'Discovery Offer'): ?><?php foreach ($product as $k => $product): ?><li class="v_slide_item capsule-sequence-<?= $counter ?>" data-product="<?= strtolower(str_replace(' ','_',$product->name)) ?>" style="width: 110.667px;"><button data-product-item-id="<?= $product->post_id ?>" tabindex="0"><div class="v_capsImage"><div class="v_capsPlaceholder"></div> <img src="<?= $product->image[0] ?>" style="top: 16px; right: 0; left: 15px;" lazy="loaded"></div><div class="v_capsName"> <span><strong class="v_brand" term="<?= $product->name?>"><?= ucwords(strtolower($product->name))?></strong></span> </div> <span class="v_link">Read more</span> </button></li><?php $counter++; endforeach; ?><?php endif; ?><?php endforeach; ?>
			                            </ul>
			                        </div>
			                    </div>
			                    <button class="v_sliderArrow v_sliderNext" tabindex="-1" aria-hidden="true"> <i class="fn_angleRight"></i> </button>
			                    <div class="v_caret" style="left: 55.3333px;"></div>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_coffeeSelector coffee_origins_description v_sectionLight">
				<div class="v_sectionRestrict">
					<div class="v_coffeeDescription">
						<div class="m_circle"></div>
						<div class="v_slider">
							<div class="v_overflow">
								<div class="v_padding">
								<?php $counter = 0; foreach ($products as $category => $product): ?>
									<?php if ($category != 'Discovery Offer'): ?>
										<?php foreach ($product as $k => $product): ?>
											<div class="v_row capsule-section <?= $counter != 0 ? 'hide' : ''?> <?= strtolower(str_replace(' ','_',$product->name)) ?>">
												<div class="v_col33">
													 <img alt="" src="<?= $product->product_view_image['url'] ?>" lazy="loaded">
													 <!-- <img alt="" src="https://www.nespresso.com/ecom/medias/sys_master/public/9188611751966.png" lazy="loaded"> -->
												</div>
												<div class="v_col66">
													<h5><strong class="v_brand" term="<?= $product->post_id?>"><?=$product->name?></strong></h5>
													<!-- <p class="v_headline"><?= $product->property ?></p> -->
													<div class="capsule-description">
														<p class="v_wysiwyg">
															<div class="show-less">
																<?php echo $product->description ?>
															</div>
															<div>
																<a class="v_link v_iconLeft" href="<?= $product->url ?>"> <i class="fn_arrowLink"></i> <span>See more</span> </a>
															</div>
														</p>
													</div>
													<div class="v_row">
													    <ul class="v_inlineTable custom_v_inlineTable">
													        <li class="v_features"> <strong>Intensity</strong>
													            <div class="v_intensityLevel">
													                <ul class="v_inline">
													                    <li class="v_value"><?= $product->intensity ?></li>
													                    <?php for ($i=0; $i < $product->intensity; $i++): ?>
													                    	<li class="v_filled"></li>
													                    <?php endfor; ?>
													                </ul>
													            </div>
													        </li>
													        <li class="v_features"> <strong>Cup size</strong>
													            <div class="v_intensityLevel v_level">
													                <div class="cupsize">
													                    <?php if(is_array($product->size_of_cup)):
													                        foreach ($product->size_of_cup as $key => $value): ?>
													                            <i class="cupsize__<?=$value?>"></i>
													                        <?php endforeach; ?>
													                    <?php else: ?>
													                        <i class="cupsize__<?=$product->size_of_cup?>"></i>
													                    <?php endif; ?>

													                </div>
													            </div>
													        </li>
													    </ul>
													    <ul class="v_inlineTable custom_v_inlineTable">
													        <!-- <li class="v_features"> <strong>Cup size</strong> <span>Ristretto 25 ml<br>Espresso 40 ml</span> </li> -->
													        <li class="v_features"> <strong>Aromatic profile</strong> <span><?=$product->aromatic_profile?></span> </li>
													        <li class="v_features"> <strong>Aromatic notes</strong> <span><?= $product->property ?></span> </li>
													    </ul>
													</div>
													<div class="v_row v_more">
														<div class="custom_v_inlineTable"></div>
													    <div class="custom_v_inlineTable">
													        <div class="v_addToCart">
													            <div class="v_priceAndButton">
													                <p class="v_productPrice"><?= wc_price($product->price)?> </p>
													                <div class="product-add pull-right" style="position: relative;">

																        <button class="btn btn-icon btn-block btn-green btn-icon-right btn-coffee-origin"
																                data-id="<?=$product->post_id?>"
																                data-cart="true"
																                data-name="<?=$product->name?>"
																                data-price="<?=$product->price?>"
																                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
																                data-type="<?=$product->product_type?>"
																                data-vat="1"
																                data-qty-step="10"
																                data-url="<?= $product->url ?>"
																                data-aromatic-profile="<?= $product->aromatic_profile?>"
																            >
																            <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
																        </button>

																    </div>
													            </div>
													        </div>
													    </div>
													</div>
												</div>
											</div>
										<?php $counter++; endforeach; ?>
									<?php endif; ?>
								<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<section class="vue_magicContent v_origins v_sectionLeft" pv-height="auto" id="origins" data-label="Origins">
		    <div class="bg_container bg-xl" pv-speed="4">
		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/origins_placeholder_L.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/origins_XL.jpg');" lazy="loaded"></div>
		    </div>
		    <div class="bg_container bg-small" pv-speed="4">
		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/origins_placeholder_S.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/origins_S.jpg');" lazy="loaded"></div>
		    </div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">The Origins of <strong><em>Nespresso</em></strong> Coffee</h2>
		                <!-- <p class="v_headline wow" data-wow="">We're often asked why we call our coffees Grands Crus.</p> -->
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p>Come on a journey to the tropics of Capricorn and Cancer, where the fruits of the exotic Coffea Rubiaceae tree, luscious red in colour, dray their character from their terroir, their environment.</p>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_photo v_parallax" id="photo_origins" data-label="Photo origins" style="height: calc(30em + 15vw);">
		    <div class="bg_container v_parallaxLayer bg-xl" pv-speed="4" style="transform: translate3d(0px, 89.0917px, 0px);">

		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/photoOrigins_placeholder_L.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/photoOrigins_XL.jpg');" lazy="loaded"></div>
		    </div>
		    <div class="bg_container v_parallaxLayer bg-small" pv-speed="4" style="transform: translate3d(0px, 2.092px, 0px);">


		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/photoOrigins_placeholder_S.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/photoOrigins_S.jpg');" lazy="loaded"></div>
		    </div>
		</section>
		<section class="vue_magicContent v_species v_sectionCenter v_sectionOverflow" pv-height="auto" id="origin_species" data-label="Origin and Species">
		    <a href="#origin_species" class="v_btnRoundScroll"> <i class="fn_angleDown"></i> </a>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">Origin and Species</h2>
		                <p class="v_headline wow" data-wow="">The taste of a Grand Cru depends on its blend composition and terroir.</p>
		                <div class="v_row v_gridList" slot="content">
						    <div class="v_gridItem v_col40">
						        <div class="mobile-div">
						        	<h3 data-wow="" data-wow-delay="0" class="wow">Blend</h3>
						        	<p data-wow="" data-wow-delay="0" class="wow">Various types of Arabicas and Robustas</p>
						        </div>
						        <div class="v_imageContainer v_imageRound wow" data-wow="" data-wow-delay="0">
						            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/blend_XL.jpg');" lazy="loaded">
						                <div class="v_dotted"> <span data-wow="" data-wow-delay="0" class="wow"></span><span data-wow="" data-wow-delay="1" class="wow"></span><span data-wow="" data-wow-delay="2" class="wow"></span><div class="v_and wow" data-wow="" data-wow-delay="3"></div><span data-wow="" data-wow-delay="4" class="wow"></span><span data-wow="" data-wow-delay="5" class="wow"></span><span data-wow="" data-wow-delay="6" class="wow"></span> </div>
						            </div>
						        </div>
						        <div class="desktop-div">
						        	<h3 data-wow="" data-wow-delay="0" class="wow">Blend</h3>
						        	<p data-wow="" data-wow-delay="0" class="wow">Various types of Arabicas and Robustas</p>
						        </div>
						    </div><div class="v_gridItem v_col60">
						        <div class="v_imageContainer v_imageRound wow last_v_imageRound" data-wow="" data-wow-delay="7">
						            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/terroir_XL.jpg');" lazy="loaded"></div>
						        </div><div class="v_imageContainer v_imageRound wow last_v_imageRound" data-wow="" data-wow-delay="8">
						            <div class="v_image v_imageRound" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/terroir2_XL.jpg');" lazy="loaded"></div>
						        </div>
						        <div class="v_imageContainer v_imageRound wow last_v_imageRound" data-wow="" data-wow-delay="9">
						            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/terroir3_XL.jpg');" lazy="loaded"></div>
						        </div>
						        <h3 data-wow="" data-wow-delay="1" class="wow">Terroir</h3>
						        <p data-wow="" data-wow-delay="1" class="wow">Altitude, soil composition and climate temperature</p>
						    </div>
						</div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_carousel v_key_carousel v_slide4 origin-carousel" id="carousel" data-label="Carousel">
		    <h2 class="v_visually_hidden">Gallery</h2>
		    <p class="v_visually_hidden">Use ENTER or SPACE key on thumbnails buttons to enter slider mode</p>
		    <div class="vue_imageGrid origin-imageGrid">
		        <div>
		        	<span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/placeholder_1_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/1_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/1_XL.jpg');" lazy="loaded">
		        	<button tabindex="-1" aria-hidden="true"></button></span>
		        </div><div>
		            <div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/placeholder_2_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/2_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/2_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> </span> </div><div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/placeholder_3_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/3_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/3_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> <img class="v_visually_hidden" alt="Image 3" src="/shared_res/agility/grandCruCoffeeRange/coffeeOrigins/img/carousel/3_XL.jpg" lazy="loaded"> </span> </div><div>
		            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/placeholder_4_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/4_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/4_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div><div>
		            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/placeholder_5_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/5_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/5_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div>
		        </div>
		    </div>
		    <div id="modal-container" class="image-modal">
  				<img class="image-modal-content" id="modal_img" src="">
			</div>

		    <!-- <div class="vue_carouselSlider">
		        <ul class="v_carouselSliderInner">
		            <li class="v_item0" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/1_XL.jpg');" lazy="loaded"></li>
		            <li class="v_item1" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/2_XL.jpg');" lazy="loaded"></li>
		            <li class="v_item2" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/3_XL.jpg');" lazy="loaded"></li>
		            <li class="v_item3" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/4_XL.jpg');" lazy="loaded"></li>
		            <li class="v_item4" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/5_XL.jpg');" lazy="loaded"></li>
		        </ul>
		        <nav>
		            <button class="v_btnRound v_btnPrev" tabindex="-1" aria-hidden="true"> <span>Previous image</span> <i class="fn_angleLeft v_blendDifference"></i> <i class="fn_angleLeft v_blendDodge"></i> <i class="fn_angleLeft v_blendSaturate"></i> </button>
		            <button class="v_btnClose" tabindex="-1" aria-hidden="true"> <span class="v_visually_hidden">Leave slider mode</span>
			                                    </button>
		            <button class="v_btnRound v_btnNext" tabindex="-1" aria-hidden="true"> <span>Next image</span> <i class="fn_angleRight v_blendDifference"></i> <i class="fn_angleRight v_blendDodge"></i> <i class="fn_angleRight v_blendSaturate"></i> </button>
		        </nav>
		    </div> -->
		</section>
		<section class="vue_testimonial v_sectionLight v_sectionAutoHeight" id="testimonial" data-label="Testimonial">
		    <div class="bg_normal" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/background_XL.jpg');" lazy="loaded"></div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_tableRow">
		                <div class="v_col20">
		                    <div class="v_image wow" data-wow="" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/testimonial_XL.jpg');" lazy="loaded"></div>
		                </div>
		                <div class="v_col80">
		                    <div class="v_wysiwyg v_quote wow" data-wow="" data-wow-delay="1">In our AAA Sustainable Quality Program, we’re learning together with our farmers how to improve their farms. At the end of the day, it’s the farmers who best understand their coffee trees.</div>
		                    <p class="v_sign wow" data-wow="" data-wow-delay="2"><strong>JUAN DIEGO</strong>, <span><strong class="v_brand" term="nespresso">Nespresso</strong> AAA Manager in Costa Rica</span></p>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_magicContent v_responsibleSourcing v_sectionLeft" pv-height="auto" id="responsible_sourcing" data-label="Responsible sourcing">
		    <div class="bg_container" pv-speed="4">
		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/responsibleSourcing_placeholder_L.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/responsibleSourcing_XL.jpg');" lazy="loaded"></div>
		    </div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">A cup of coffee that has a positive effect</h2>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p>We believe that each cup of <strong class="v_brand" term="nespresso">Nespresso</strong> coffee has the potential not only to deliver a moment of pleasure for our consumers, but also restore, replenish and revive environmental and human resources. The Positive Cup program is based on continuous improvement and innovative sustainability solutions to create positive impact along the value chain from farmers to <strong class="v_brand" term="nespresso">Nespresso</strong> consumers. It is our sustainable consumption commitment to our Club Members.</p>
		                </div>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p><strong>Our ambition and goals for 2020:</strong></p>
		                    <ul>
		                        <li>100% Sustainably sourced coffee</li>
		                        <li>100% Responsibly managed aluminium</li>
		                        <li>100% Carbon efficient operations</li>
		                    </ul>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_navigationBottom v_sectionAutoHeight">
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_nav">
		                <p class="v_visually_hidden"></p>
		                <ul class="v_row">
		                    <li>
		                        <a href="/coffee-range">
		                            <div class="v_thumb" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/introduction_thumb3.jpg');" lazy="loaded"></div> <span><strong class="v_brand" term="nespresso">Nespresso</strong> Coffee</span> </a>
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </div>
		</section>
	</div>
</main>
