<?php

require_once 'ProductList.php';

abstract class CoffeeList extends ProductList
{

    protected static $technology = [
        'original' => 'Original',
        'vertuo' => 'Vertuo'
    ];

    protected static $aromatic = [
        1 => 'Intense',
        2 => 'Fruity',
        3 => 'Balanced'
    ];


    /**
     * @var array
     *
     * collection
     *   - category
     *     - product
     *       - value : name
     *       - value : name
     *       - ...
     */
    protected static $data = [
        'original' => [
            'espresso' => [
                'original-livanto' => [
                    'name' => 'Volluto Decaffeinato',
                    'desc' => 'Balanced',
                    'cupsize' => [2],
                    'intensity' => 6,
	                'visual' => 'images/products/Livanto.jpg',
	                'image' => 'images/products/volluto-deca.png',
                    'price' => '0.56',
                    'type' => 'capsules',
                    'text' => '
                        <h3>BALANCED</h3>
                        <p>A blend of pure Arabica from South and Central America, Livanto is a well-balanced espresso characterized by a roasted caramelized note.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">This blend is composed of the most prestigious Central and South American Arabicas, found in Costa Rica and Colombia and is cultivated according to traditional methods to preserve their malted and fruity profiles.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">A medium roasting, accentuates malted notes, whilst fruity notes evolve to create a complex and delicate caramelized bouquet.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">A round and balanced profile, typical of freshly roasted coffee results in a combination of cereal, malted and caramelized notes as well as fine fruity notes.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle js-toggle--desktop" data-toggle="+">INGREDIENTS AND ALLERGENS</h3>
                            <div class="btn-mobile-toggle__content" style="display: none"><p>10 Vintage 2011 capsules of roast and ground coffee for the Nespresso system. </p><p>INGREDIENTS & ALLERGENS <br>Roast and ground coffee </p><p>NET WEIGHT (FOR 10 CAPSULES) <br>60 g </p><p>MADE IN SWITZERLAND</p></div>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle js-toggle--desktop" data-toggle="+">Recycling</h3>
                            <div class="btn-mobile-toggle__content" style="display: none">
                                <img src="images/recycling.jpg" alt="">
                                <p>Your used aluminium capsules are collected from your home!</p> 
								<p>Add the set of 3 Recycling bags to your order for free. <br><a href="#" class="link">Find out more</a></p>
								<div class="container-btn">
									<a href="#" class="btn btn-icon btn-green btn-icon-right"
									   data-cart="true"
									   data-type="capsules"
									   data-name="Volluto"
									   data-price="0.70"
									   data-picture="images/product-03.png"
									   data-qty-step="10">
										<i class="icon-basket"></i><span class="btn-add-qty"></span><span>Add to basket</span><i class="icon-plus"></i>
									</a>
								</div>
                            </div>
                        </div>
                    ',
                    'technology' => 'original',
                    'aromatic' => 3
                ],
                'original-capriccio' => [
                    'name' => 'Capriccio',
                    'desc' => 'Rich and distinctive',
                    'cupsize' => [2],
                    'intensity' => 5,
                    'visual' => 'images/products/Livanto.jpg',
                    'image' => 'images/products/capriccio.png',
                    'price' => '0.70',
                    'type' => 'capsules',
                    'text' => '
                        <h3>RICH AND DISTINCTIVED</h3>
                        <p>Blending South American Arabicas with a touch of Robusta, Capriccio is an espresso with a rich aroma and a strong typical cereal note.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">The presence of South American Arabicas grown at high altitude gives this blend a light acidity, which is balanced with the perfect amount of Brazilian Arabica and a touch of Robusta.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">The light roasting of this blend creates a rich character and preserves a light acidity.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">A typical cereal note balanced with a light acidity.</p>
                        </div>
                        ',
                    'technology' => 'original',
                    'aromatic' => 3
                ],
                'original-volluto' => [
                    'name' => 'Volluto',
                    'desc' => 'Sweet and light',
                    'cupsize' => [2],
                    'intensity' => 4,
                    'visual' => 'images/products/Livanto.jpg',
                    'image' => 'images/products/volluto.png',
                    'price' => '0.70',
                    'type' => 'capsules',
                    'text' => '
                        <h3>Sweet and light</h3>
                        <p>A blend of pure and lightly roasted Arabica from South America, Volluto reveals sweet and biscuity flavours, reinforced by a little acidity and a fruity note.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">The Brazilian and Colombian Arabicas that go into making Volluto come from small plantations that produce very high quality coffee. They are grown in respect of the environment and local traditions.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Light roasting preserves the cereal note of the Brazilian Arabica and the fresh and fruity note of the Colombian coffee.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Quite present and diverse: fruity notes harmoniously balanced with sweet and biscuity notes.</p>
                        </div>
                        ',
                    'technology' => 'original',
                    'aromatic' => 3
                ],
                'original-volluto-deca' => [
                    'name' => 'Livanto',
                    'desc' => 'Sweet and light',
                    'cupsize' => [2],
                    'intensity' => 4,
	                'visual' => 'images/products/Livanto.jpg',
	                'image' => 'images/products/livanto.png',
                    'price' => '0.70',
                    'type' => 'capsules',
                    'text' => '
                        <h3>Sweet and light</h3>
                        <p>A blend of pure and lightly roasted Arabica from South America, Volluto reveals sweet and biscuity flavours, reinforced by a little acidity and a fruity note.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">The Brazilian and Colombian Arabicas that go into making Volluto come from small plantations that produce very high quality coffee. They are grown in respect of the environment and local traditions.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Light roasting preserves the cereal note of the Brazilian Arabica and the fresh and fruity note of the Colombian coffee.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Quite present and diverse: fruity notes harmoniously balanced with sweet and biscuity notes.</p>
                        </div>
                        ',
                    'technology' => 'original',
                    'aromatic' => 3
                ],
                'original-cosi' => [
                    'name' => 'Cosi',
                    'desc' => 'Mild and delicately toasted',
                    'cupsize' => [2],
                    'intensity' => 4,
                    'visual' => 'images/products/Livanto.jpg',
                    'image' => 'images/products/cosi.png',
                    'price' => '0.70',
                    'type' => 'capsules',
                    'text' => '
                        <h3>MILD AND DELICATELY TOASTED</h3>
                        <p>A blend of East African, Central and South American Arabicas which has been lightly roasted to create a delicate and balanced marriage of lightly toasted cereal and fruity notes.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Arabicas from Costa Rica give this blend its characteristic cereal and lightly toasted notes, while the finest Arabicas from Kenya bring out its delicate fruity notes, both harmonised by Latin American Arabicas.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">A light roasting, perfectly executed by the Nespresso Master Roasters, creates a delicate and balanced marriage of lightly toasted cereal and fruity notes. </p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Mild cereal and lightly toasted notes enhanced by subtle fruity notes</p>
                        </div>
                        ',
                    'technology' => 'original',
                    'aromatic' => 2
                ]
            ],
            'intenso' => [
                'original-kazaar' => [
                        'name' => 'Kazaar',
                        'desc' => 'Exceptionally intense and syrupy',
                        'cupsize' => [1, 2],
                        'intensity' => 12,
                        'visual' => 'images/products/Livanto.jpg',
                        'image' => 'images/products/kazaar.png',
                        'price' => '0.75',
                        'type' => 'capsules',
                        'text' => '
                        <h3>EXCEPTIONALLY INTENSE AND SYRUPY</h3>
                        <p>A daring blend of two Robustas from Brazil and Guatemala, specially prepared for Nespresso, and a separately roasted Arabica from South America, Kazaar is a coffee of exceptional intensity. Its powerful bitterness and notes of pepper are balanced by a full and creamy texture.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Arabicas from Costa Rica give this blend its characteristic cereal and lightly toasted notes, while the finest Arabicas from Kenya bring out its delicate fruity notes, both harmonised by Latin American Arabicas.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">A light roasting, perfectly executed by the Nespresso Master Roasters, creates a delicate and balanced marriage of lightly toasted cereal and fruity notes.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Mild cereal and lightly toasted notes enhanced by subtle fruity notes</p>
                            </div>'
                    ,
                    'technology' => 'original',
                    'aromatic' => 1
                ],
            'original-dharkan' => [
                'name' => 'Dharkan',
                'desc' => 'Long roasted and velvety',
                'cupsize' => [1, 2],
                'intensity' => 11,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/dharkan.png',
                'price' => '0.75',
                'type' => 'capsules',
                'text' => '
                        <h3>Long roasted and velvety</h3>
                        <p>This blend of Arabicas from Latin America and Asia fully unveils its character thanks to the technique of long roasting at a low temperature. Its powerful personality reveals intense roasted notes together with hints of bitter cocoa powder and toasted cereals that express themselves in a silky and velvety texture.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Dharkan blends four origins from Latin America and Asia, including Costa Rica, Java and two other Latin American origins. It is a coffee enhanced with washed Arabicas, combined to produce a sublime experience and finesse in the cup.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Dharkan\'s long roasting technique at low temperatures ensures the heat diffuses right to the heart of each bean, revealing intensely roasted notes with surprising roundness. It gives the coffee its extraordinary silky texture and a long, bitter and pleasant aftertaste.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Its powerful personality reveals intense roasted notes together with hints of bitter cocoa powder and toasted cereals that express themselves in a velvety texture.</p>
                        </div>',
                'technology' => 'original',
                'aromatic' => 1
            ],
            'original-ristretto' => [
                'name' => 'Ristretto',
                'desc' => 'Powerful and contrasting',
                'cupsize' => [1, 2],
                'intensity' => 10,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/ristretto.png',
                'price' => '0.70',
                'type' => 'capsules',
                'text' => '
                        <h3>Powerful and contrasting</h3>
                        <p>A blend of South American and East African Arabicas, with a touch of Robusta, roasted separately to create the subtle fruity note of this full-bodied, intense espresso.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Composed of some of the best South American Arabicas from sources such as Colombia and Brazil, Ristretto also contains the great, lightly acidic East African Arabicas and a touch of Robusta for added zing.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">The beans are roasted slowly and separately to obtain an original bouquet bringing together acidic, fruity, and roasted notes. Its finely ground texture creates an Italian-style coffee: intense, with a rich flavour and full body</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Strong roasted notes softened by notes of chocolate. A subtle contrast between strength and bitterness, acidic and fruity notes.</p>
                        </div>',
                'technology' => 'original',
                'aromatic' => 1
            ],
            'original-arpeggio' => [
                'name' => 'Arpeggio',
                'desc' => 'Intense and creamy',
                'cupsize' => [1, 2],
                'intensity' => 9,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/arpeggio.png',
                'price' => '0.70',
                'type' => 'capsules',
                'text' => '
                        <h3>Intense and creamy</h3>
                        <p>A dark roast of pure South and Central American Arabicas, Arpeggio has a strong character and intense body, enhanced by cocoa notes.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">A selection of the best South and Central American Arabicas, with fruity or malted profiles, characteristic of Costa Rican coffees.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Arpeggio is roasted intensely to bring out the characteristics of each origin. Its full body develops from the fine grinding of the beans.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Intense, grilled notes alongside subtle cocoa notes and woody hints.</p>
                        </div>',
                'technology' => 'original',
                'aromatic' => 1
            ],
            'original-roma' => [
                'name' => 'Roma',
                'desc' => 'Full and balanced',
                'cupsize' => [1, 2],
                'intensity' => 8,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/roma.png',
                'price' => '0.70',
                'type' => 'capsules',
                'text' => '
                        <h3>Full and balanced</h3>
                        <p>The balance of lightly roasted South and Central American Arabicas with Robusta, gives Roma sweet and woody notes and a full, lasting taste on the palate.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Cultivated at high altitude, the Central American Arabicas bring finesse, whilst the Brazilian Arabicas and Robustas provide body and a lingering taste on the palate.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Light roasting and fine grinding bring finesse to the blend and create a short espresso, which is not too strong.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Roasted and woody notes are revealed through the light roasting of intensely flavoured beans from different origins.</p>
                        </div>',
                'technology' => 'original',
                'aromatic' => 1
            ]
        ],
        'lungo' => [
            'original-fortissimo' => [
                'name' => 'Fortissimo lungo',
                'desc' => 'Rich and full-bodied',
                'cupsize' => [3],
                'intensity' => 8,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/fortissio.png',
                'price' => '0.70',
                'type' => 'capsules',
                'text' => '
                        <h3>Rich and full-bodied</h3>
                        <p>As in the age of sailing ships, West Indian Malabar Arabica beans are exposed to monsoon winds after harvest to reveal a distinguished aromatic profile, rich with cereal notes. We blend these with Latin American Arabica beans to create a lungo with a truly intense character.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">A fine base of Monsooned Malabar Arabica from Southwest India and washed Colombian Arabicas to bring intensity, texture and richness.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">The Indian Monsooned Malabar Arabica and Latin American coffees that form the structural base of Fortissio Lungo are split roasted in order to generate a prominent body yet retain a rich coffee flavour, not overshadowed by roasted notes.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">A rich coffee, flecked with sweet cereal and malty toasted notes, that offers a pleasant bitterness with an exquisite fullness on the palate thanks to its round and smooth texture.</p>
                        </div>',
                'technology' => 'original',
                'aromatic' => 1
            ],
            'original-vivalto' => [
                'name' => 'Vivalto Lungi',
                'desc' => 'Complex and balanced',
                'cupsize' => [3],
                'intensity' => 4,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/vivalto.png',
                'price' => '0.70',
                'type' => 'capsules',
                'text' => '
                        <h3>Complex and balanced</h3>
                        <p>Vivalto Lungo is a balanced coffee made from a complex blend of separately roasted South American and East African Arabicas, combining roasted and subtle floral notes.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">A complex and contrasting blend. The South American Arabicas, grown at high altitude, bring a light acidity, the Ethiopian Arabica adds its floral touch, and the "Cerrado" coffee from Brazil reinforces the character and develops the bitterness.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Separate roasting brings out the characters of the different origins. The grinding of the beans results in a coffee best enjoyed in a large cup.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">The perfect mix of several origins results in a rich and complex character: roasted, lightly woody, sweet cereal and subtle floral notes.</p>
                        </div>',
                'technology' => 'original',
                'aromatic' => 2
            ],
            'original-linizo' => [
                'name' => 'Linizo lungo',
                'desc' => 'Round and smooth',
                'cupsize' => [3],
                'intensity' => 4,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/linizio.png',
                'price' => '0.70',
                'type' => 'capsules',
                'text' => '
                        <h3>Round and smooth</h3>
                        <p>Mild and well-rounded on the palate, Linizio Lungo is a blend of fine Arabicas enhancing malt and cereal notes</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Arabicas from Brazil and Colombia that go into making Linizio L</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">The split roasting reveals the specificities of each origin. On one hand, longer roasting yields a darker bean that reveals the malted cereal notes of the Bourbon beans. On the other hand, shorter roasting of the Arabica from Colombia yields lighter beans and highlights the softer notes reminding of sugar cane, specific to its region of origin.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Cereal.</p>
                        </div>',
                'technology' => 'original',
                'aromatic' => 3
            ],
        ],
        'Pure Origin' => [
            'original-indriya' => [
                'name' => 'Indriya from India',
                'desc' => 'Powerful and spicy',
                'cupsize' => [1, 2],
                'intensity' => 10,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/indriya.png',
                'price' => '0.72',
                'type' => 'capsules',
                'text' => '
                        <h3>Rich and full-bodied</h3>
                        <p>Indriya from India is the noble marriage of Arabicas with a hint of Robusta from southern India. It is a full-bodied espresso, which has a distinct personality with notes of spices.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">The Arabica and Robusta chosen for this blend grow in southern India, in the shade of large trees that also provide shelter for pepper and spice growing. Grown on high altitude plantations, the Robusta we use is a very pure coffee, very intense and slightly bitter. Only absolutely perfect beans, which have passed a rigorous selection process, are used for Indriya from India.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">The Arabica is lightly roasted, to preserve its subtle aromas. In contrast, the Robusta is well roasted to reveal its intensity and develop a full body. Very fine grinding supports the body and enhances the flavour.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Cocoa and dry plant notes, as well as a spicy bouquet reminiscent of cloves, pepper and nutmeg.</p>
                        </div>
                    ',
                'aromatic' => 1
            ],
            'original-rosabaya' => [
                'name' => 'Rosabaya de Colombia',
                'desc' => 'Fruity and balanced',
                'cupsize' => [2],
                'intensity' => 6,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/rosabaya.png',
                'price' => '0.72',
                'type' => 'capsules',
                'text' => '
                        <h3>Fruity and balanced</h3>
                        <p>This blend of fine, individually roasted Colombian Arabicas, develops a subtle acidity with typical red fruit and winey notes.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">The varieties that make up this "Pure Origin" Espresso are cultivated by small coffee growers in the highest region of Colombia. Hand-picked and treated with the wet method to intensify its aromas, the coffee is then dried in parchment and transferred to the Paramo de Letras region, where the cold, dry climate preserves it.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Split roasting of the different varieties allows the fruity and acidic notes to come to the fore, whilst adding body.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Notes of red fruit, reminiscent of wine: blackcurrants, cranberries, and redcurrants.</p>
                        </div>
                    ',
                'aromatic' => 2
            ],
            'original-dulsao' => [
                'name' => 'Dulsao do Brasil',
                'desc' => 'Sweet and satiny smooth',
                'cupsize' => [2],
                'intensity' => 4,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/dulsao.png',
                'price' => '0.72',
                'type' => 'capsules',
                'text' => '
                        <h3>Sweet and satiny smooth</h3>
                        <p>A pure Arabica coffee, Duls�o do Brasil is a delicate blend of red and yellow Bourbon beans from Brazil. Its satiny smooth, elegantly balanced flavor is enhanced with a note of delicately toasted grain.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Subtly sweet, red and yellow Bourbon coffee beans are grown in the hill-top plantations of southern Brazil. They are harvested by hand, then pulped and sun-dried to allow the mucillage sugars to infuse the bean with a mellow flavour.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Some beans are roasted lightly to retain their natural sweet flavour and achieve a roundness on the palate. The remaining beans undergo a longer roasting for a distinctive toasted flavour.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Lightly toasted grain.</p>
                        </div>
                    ',
                'aromatic' => 3
            ],
            'original-bukeela' => [
                'name' => 'Indriya from India',
                'desc' => 'Floral and wild',
                'cupsize' => [3],
                'intensity' => 3,
                'visual' => 'images/products/Livanto.jpg',
                'image' => 'images/products/bukeela.png',
                'price' => '0.72',
                'type' => 'capsules',
                'text' => '
                        <h3>Floral and wild</h3>
                        <p>Composed of two very different Arabicas from Ethiopia, the birthplace of coffee, this delicately fresh and floral Pure Origin Lungo reveals unexpectedly wild notes of musk and wood.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Ethiopian Arabicas from Sidama and other Western regions are hand-picked to create Bukeela ka Ethiopia. Treated differently, the Sidamo beans are washed and naturally sundried on raised African drying beds while Western regions Arabica as there, water is more scarce are un-washed and naturally processed.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Bukeela ka Ethiopia is roasted in two parts. One split is roasted shorter and darker to enhance wild musk and body, the other split roasted lighter and longer brings the final flourish, the perfume, the definitive aromatic coffee reminiscent of jasmine, orange blossom and white lily, with its delirious, heady perfume.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Bukeela ka Ethiopia is a delicately fresh and floral Pure Origin Lungo that reveals unexpectedly wild notes of musk and wood.</p>
                        </div>
                    ',
                'aromatic' => 2
            ]
        ]

    ],
        'vertuo' => [
            'assortments' => [
                /*'vertuo-flavored-limited' => [
                    'name' => 'VertuoLine FLAVORED ASSORTMENT LIMITED EDITION',
                    'image' => 'images/assortment-1.png',
                    'price' => '44.00',
                    'type' => 'accessories'
                ],*/
                'vertuo-indulgent-pack' => [
                    'name' => 'INDULGENT PACK FOR VERTUO <br>(100 CAPSULES)',
                    'image' => 'images/assortment-2.png',
                    'price' => '102.50',
                    'cupsize' => [1,2],
                    'type' => 'accessories',
                    'text' => '
                        <p style="padding-top: 50px; padding-bottom: 50px;">Explore the rich tastes and differing notes of our VertuoLine Indulgent Pack. It’s the perfect choice allowing you to discover a distinctive range of extraordinary Grand Cru coffees.  Our coffee experts have specially selected 10 Grand Cru varieties for the Indulgent Pack including Stormio, Odacio, Melozio, Elvazio, Vanizio, Hazelino, Half Caffeinato, Diavolitto, Altissio and Voltesso..</p>
                    ',
                    'technology' => 'vertuo',
                ],
            ],
            'espresso' => [
                'vertuo-diavollito' => [
                    'name' => 'Diavollito',
                    'desc' => 'Highly intense and powerful',
                    'cupsize' => [2],
                    'intensity' => 11,
                    'image' => 'images/products/Diavolitto_FB_OMBRE.png',
                    'price' => '0.85',
                    'type' => 'capsules',
                    'text' => '
                        <h3>Highly intense and powerful</h3>
                        <p>A high intensity Espresso for coffee aficionados. Energized by highly roasted Robusta and Brazilian Arabica coffee beans, with an aroma that hints at oak wood and leather, balance is achieved with a creamy texture.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Diavolitto is an original blend that combines Central and South America Robusta beans together with Brazilian Arabica. The resulting brew is a surprisingly powerful Espresso. The washed Robusta and Arabica off-set this intense blend, with a smooth and fine texture.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Diavolitto is a highly roasted blend, which delights with its bitterness and rich body. Split roasting the beans reveals aromatic specificities of each of the beans’ origins, delivering an intense, powerful Espresso without harshness.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">The long prewetting of Diavolitto helps to better extract the intensely roasted Robusta coffee in this blend. Thanks to a slow flow and a high extraction temperature the strength and deep intensity are balanced with a smooth texture.</p>
                        </div>
                       ',
                    'aromatic' => 1
                ],
                'vertuo-altissio' => [
                    'name' => 'Altissio',
                    'desc' => 'Full-bodied and creamy',
                    'cupsize' => [2],
                    'intensity' => 9,
                    'image' => 'images/products/Altissio_FB_OMBRE.png',
                    'price' => '0.85',
                    'type' => 'capsules',
                    'text' => '
                        <h3>Full-bodied and creamy</h3>
                        <p>This highly roasted full-bodied Espresso is made with Arabicas from Costa Rica and South America. The hint of Robusta contrasts with smooth cereal notes.</p>
                        <p>The addition of milk complements its natural full body.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">The presence of South American coffee beans create this intense Espresso, while Costa Rican beans balance the full body by bringing soft cereal notes to the blend.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Altissio is intensely and shortly roasted to reveal the full-bodied character of this bold blend.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">This strong but very balanced, smooth coffee is obtained through the combination of a short prewetting, a slow flow and a high extraction temperature.</p>
                        </div>
                       ',
                    'aromatic' => 1
                ],
                'vertuo-voltesso' => [
                    'name' => 'Voltesso',
                    'desc' => 'Light and sweet',
                    'cupsize' => [2],
                    'intensity' => 4,
                    'image' => 'images/products/Voltesso_FB_OMBRE.png',
                    'price' => '0.85',
                    'type' => 'capsules',
                    'text' => '
                        <h3>Light and sweet</h3>
                        <p>Lightly roasted, Voltesso is a balanced, round coffee that reveals biscuity notes and pronounces a distinctly sweet aroma thanks to its South American Arabicas.</p>
                        <p>For an even smoother result, blend Voltesso with milk for a delicious new take on this Espresso.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">The sweet, light taste and biscuity fragrance of this Espresso are achieved by the inclusion of fine Brazilian Bourbon coffee beans. At the same time, the mildness of the cup is enhanced by coffee from the highlands in the central region of Colombia.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">Colombian coffee beans are lightly roasted to reveal a slight acidity. This is perfectly balanced by the long roasting of Brazilian Arabica, which enhances the sweet and biscuity notes, to deliver a harmonious blend.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">The low extraction temperature and slow, gentle flow emphasize the sweet character of this rewarding Espresso.</p>
                        </div>
                       ',
                    'aromatic' => 1
                ],
                'vertuo-decaffeinato' => [
                    'name' => 'Decaffeinato Intenso',
                    'desc' => 'Dense and harmonious',
                    'cupsize' => [2],
                    'intensity' => 7,
                    'image' => 'images/products/Decaffeinato_FB_OMBRE.png',
                    'price' => '0.85',
                    'type' => 'capsules',
                    'text' => '
                        <h3>Dense and harmonious</h3>
                        <p>The pleasure of an intense decaffeinated Espresso with body. Colombian and Brazilian coffees subtly blend with fine Asian Robusta to create harmony, releasing cocoa and fruity notes.</p>
                        <p>Add milk for a sweet taste and creamy texture.</p>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Origin</h3>
                            <p class="btn-mobile-toggle__content">Decaffeinato Intenso is created with coffee beans from South Colombia and unwashed Brazilian Arabica. Together, they reveal fruity and cocoa notes, while a touch of fine unwashed Asian Robusta provides a dense body.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Roasting</h3>
                            <p class="btn-mobile-toggle__content">The depth of this Espresso comes mainly from the dark roasted coffee beans. The flavor is further enhanced by a medium-roasting, which generates delicious fruity notes.</p>
                        </div>
                        <div class="btn-mobile-toggle">
                            <h3 class="btn-mobile-toggle__header js-toggle" data-toggle="+">Aromatic Profile</h3>
                            <p class="btn-mobile-toggle__content">Prewetting is key to this intense decaffeinated Espresso which is well infused prior to the extraction beginning. The fast flow ensures a balance between the cocoa and fruity notes.</p>
                        </div>
                       ',
                    'aromatic' => 1
                ],
            ]
        ]
    ];


    /**
     * Get technologyTerm
     * @param $idtechno
     * @return array
     */
    public static function technologyTerm($idtechno)
    {
        return isset(static::$technology[$idtechno])
            ? static::$technology[$idtechno]
            : [];
    }

    /**
     * Get aromatic
     * @param $idAromatic
     * @return array
     */
    public static function aromatic($idAromatic)
    {
        return isset(static::$aromatic[$idAromatic])
            ? static::$aromatic[$idAromatic]
            : [];
    }


    /**
     * Find product by slug
     * @param $search
     * @return array
     */
    public static function findAromaticProducts($aromatic, $technology, $exclude)
    {
        $collection = static::collection($technology);

        $products = array();

        foreach($collection as $category) {
            foreach ($category as $slug => $product) {
                if(isset($product['aromatic']) && $slug != $exclude) {
                    if($product['aromatic'] == $aromatic) {
                        array_push($products, $product);
                    }
                }
            }
        }

        return $products;
    }
}