<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$products = get_machine_products();
$arrays = get_nespresso_product_list();
$machines = [];
$lattissima_touch = 8903;

foreach ($products as $value) {
	if($lattissima_touch == $value->post_id) {
		$lattissima_touch = $value;
	}
    $machines[] = $value->name;
}

if ($arrays) {
	$machines = $arrays['machine'];
}

global $scripts;
$scripts[] = 'js/components/machine-discover-more.js';

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/machine-discover-more.css">

<main id="main" class="fullPage fullPage__plp coffee-list desktop">
	<div id="block-8809565345269" class="free-html" data-label="">
		<div class="vue vue_machines v_lattissimaTouch">
			<section class="vue_introduction v_introduction v_sectionnull v_backgroundVideoPlaying" id="introduction" data-label="Introduction">
			    <div class="v_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/placeholder_XL.jpg');" lazy="loaded"> <iframe tabindex="-1" aria-hidden="true" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" src="https://www.youtube.com/embed/xZ-1YH91RtU?rel=0&fs=0&autoplay=1&playlist=xZ-1YH91RtU&controls=0&loop=1&enablejsapi=1&origin=https%3A%2F%2Fwww.nespresso.com&widgetid=1" id="widget2" class="play gtm-video-start gtm-video-progress25 gtm-video-progress50 gtm-video-progress75 gtm-video-complete" data-gtm-yt-inspected-2212929_367="true" height="360" frameborder="0" width="640"></iframe> </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <header>
			                <div class="v_cell">
			                    <div class="v_menu">
			                        <p class="v_visually_hidden"></p>
			                        <ul>
			                            <li> <a href="/machine-list">All machines</a> </li>
			                            <li> <a href="machine-assistance-responsive#!/Lattissima-Touch/instructions">Assistance</a> </li>
			                            <li> <a href="machines/cafetera-lattissima-touch-glam-white">Specifications</a> </li>
			                        </ul>
			                    </div>
			                </div>
			            </header>
			            <article>
			                <div class="v_articleContent">
			                    <h2> <span><strong class="v_brand" term="lattissima touch">Lattissima Touch</strong></span> </h2>
			                    <div class="v_wysiwyg">
			                        <p><strong class="v_brand" term="lattissima touch">Lattissima Touch</strong> offers an exceptional convenience to enjoy at home the pleasure of many excellent coffee & milk recipes at the simple touch of a button.</p>
			                    </div>
			                    <div class="v_buttonContainer"> <a class="v_btnRoundM" tabindex="-1" aria-hidden="true" href="#visualVariations"> <i class="fn_angleDownCircle"></i> </a>  </div>
			                </div>
			            </article>
			        </div>
			    </div>
			    <div class="v_video">
			        <button type="button" name="button" class="v_btnRoundSM v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			        <div></div>
			    </div>
			</section>
			<section class="vue_visualVariations vue_productConfigurator v_sectionLight v_toggleVariation0 v_toggle_lattissimaTouch v_key_visualVariations" id="visualVariations" data-label="Visual Variations">
			    <h2 class="v_visually_hidden">Customize and Buy your <strong class="v_brand" term="lattissima touch">Lattissima Touch</strong></h2>
			    <p class="v_visually_hidden">Use the buttons below to expand front or side view</p>
			    <div class="v_visualVariationsSlider" style="touch-action: pan-y; -moz-user-select: none;">
			        <article class="v_product">
	                    <div class="v_restrict">
			                <button style="box-shadow: none !important;" class="v_activeView"><span class="v_visually_hidden">Press ENTER or SPACE to zoom on front view</span><span class="v_visually_hidden">Front</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/red_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/white_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/silver_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-3">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/blacktitanium_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-4">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/black_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button><button style="box-shadow: none !important;"> <span class="v_visually_hidden">Press ENTER or SPACE to zoom on side view</span> <span class="v_visually_hidden">Side</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/red_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/white_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/silver_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-3">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/blacktitanium_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-4">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/black_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button>
			            </div>
	                </article>
			    </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_productConfiguratorPosition">
			                <div class="v_productConfigurator">
			                    <p class="v_visually_hidden">Use the form below to choose your machine model, change color, and add to bag</p>
			                    <form>
			                        <div class="v_cell v_toggleColor">
			                            <fieldset>
			                                <legend>Choose your color</legend>
			                                <div class="machine-product-color machine-product-color-1 v_bullets0">
			                                    <p aria-hidden="true">Colors</p>
			                                    <div class="v_inputs">
			                                        <input name="colorToggle0" id="radio_colorToggle0_0" value="0" type="radio"><label style="background-color: rgb(218, 31, 30);" for="radio_colorToggle0_0"> <b>Glam Red <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle0" id="radio_colorToggle0_1" value="1" type="radio"><label style="background-color: rgb(255, 255, 255);" for="radio_colorToggle0_1"> <b>Glam White <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle0" id="radio_colorToggle0_2" value="2" type="radio"><label style="background-color: rgb(219, 219, 219);" for="radio_colorToggle0_2"> <b>Palladium Silver <span class="v_visually_hidden">color</span></b> </label>
			                                        <!-- <input name="colorToggle0" id="radio_colorToggle0_3" value="3" type="radio"><label style="background-color: rgb(88, 88, 88);" for="radio_colorToggle0_3"> <b>Black Titanium <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle0" id="radio_colorToggle0_4" value="4" type="radio"><label style="background-color: rgb(0, 0, 0);" for="radio_colorToggle0_4"> <b>Glam Black <span class="v_visually_hidden">color</span></b> </label>  -->
			                                        <div class="v_bullet_selected"><span></span></div>
			                                    </div>
			                                </div>
			                            </fieldset>
			                        </div>
			                        <div class="v_cell v_buyProduct">
				                        <fieldset>
				                            <legend>Review your product: Lattissima Touch Glam Red color</legend>
				                            <div class="v_addToCart">
				                                <div class="v_priceAndButton">
				                                    <p class="v_productPrice"><?= wc_price(@$lattissima_touch->price) ?></p>
				                                    <div class="v_addToCartCustom">
				                                        <button type="button" class="view_buy" data-link="<?= @$lattissima_touch->url ?>" style="padding: 0.7em 1em .7em 1em;"><span class="v_label" aria-hidden="true">View Details & Buy</span> </button>
				                                    </div>
				                                </div>
				                            </div>
				                        </fieldset>
				                    </div>
			                    </form>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_benefits v_key_benefits showBenefits_lattissimaTouch" id="benefits" data-label="benefits">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover the key benefits of the <strong class="v_brand" term="lattissima touch">Lattissima Touch</strong> range</h2>
			            <div class="v_wysiwyg wow" data-wow=""></div>
			            <ul class="v_row6">
			                <li data-wow="" class="v_icon_programmable6Buttons wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/programmable6Buttons.svg"></div>
			                    <div>
			                        <p>6 capacitive buttons</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_digitalTouchscreen wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/digitalTouchscreen.svg"></div>
			                    <div>
			                        <p>“One-touch” fresh milk system</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_descallingAlert wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/descallingAlert.svg"></div>
			                    <div>
			                        <p>Intuitive descaling and cleaning alerts</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_25Seconds wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/25Seconds.svg"></div>
			                    <div>
			                        <p>Fast heat-up in 25 seconds</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_19Bar wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/19Bar.svg"></div>
			                    <div>
			                        <p>19 Bar pressure</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_autoPoweroff wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/autoPoweroff.svg"></div>
			                    <div>
			                        <p>Auto power off</p>
			                    </div>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>
			<section class="vue_video v_key_video" id="video" data-label="Video">
			    <h2 class="v_visually_hidden">Watch the <strong class="v_brand" term="lattissima touch">Lattissima Touch</strong> video</h2>
			    <div class="v_backgroundImage" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/video_L.jpg');" lazy="loaded"></div>
			    <button type="button" name="button" class="v_btnRoundLG v_btnOpen video-modal" data-source="video" title="Press ENTER or SPACE to start the Maestria presentation"> <i class="fn_videoCircle"></i> <span tabindex="-1" aria-hidden="true">Watch the <strong class="v_brand" term="lattissima touch">Lattissima Touch</strong> video</span> <span class="v_visually_hidden">Play video</span> </button>

				<div id="video-modal-container" class="video-modal">
	  				<iframe class="video-modal-content" id="modal_video video_iframe"  allowfullscreen="1" allow="encrypted-media" title="YouTube video player" src="http://www.youtube.com/embed/pm3usfaSiuE?enablejsapi=1&autoplay=0&rel=0"  data-gtm-yt-inspected-2212929_367="true" class="gtm-video-start gtm-video-complete gtm-video-progress25 gtm-video-progress50 gtm-video-progress75" height="360" frameborder="0" width="640"></iframe>
				</div>

			    <div class="v_videoContainer">
			        <button type="button" name="button" class="v_btnRoundMD v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			    </div>
			</section>
			<section class="vue_details v_sectionLeft v_key_lensdetails" id="addyourmilk" data-label="addyourmilk">
			   <div class="bg_normal bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/lensdetails_L.jpg');" lazy="loaded">
			      <div class="v_lens v_tab_6TouchRecipes" id="v_lens" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/lensdetails_L_zoom.jpg');" lazy="loaded"></div>
			   </div>
			   <div class="bg_normal bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/lensdetails_S.jpg');" lazy="loaded">
			      <div class="v_lens v_tab_6TouchRecipes" id="v_lens" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/lensdetails_L_zoom.jpg');" lazy="loaded"></div>
			   </div>
			   <div class="v_sectionRestrict">
			      <div class="v_sectionContent">
			         <div class="v_text">
			            <h2 data-wow="" class="wow">Just add your touch of milk</h2>
			            <div class="vue_accordion">
			               <ul role="accordion" class="v_isCollapsable">
			                  <li class="v_accordionItem">
			                     <button class="addyourmilk-item v_title v_open" data-touch="v_tab_6TouchRecipes" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 1 of 6</span> 6 one touch recipes </button>
			                     <div class="v_wysiwyg" aria-hidden="true" style="">
			                        <p>For coffee and milk lovers to prepare in an instant incomparable cappuccino, latte macchiato or warm milk froth suitable for many coffee & milk recipes.</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="addyourmilk-item v_title" data-touch="v_tab_milkFrothRegulator" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 2 of 6</span> Milk froth regulator </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>You can easily adjust the milk froth texture according to your taste by turning the regulator knob.</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="addyourmilk-item v_title" data-touch="v_tab_alerts" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 3 of 6</span> Alerts </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>Intuitive descaling and cleaning alerts to ensure a proper functioning of the machine and a perfect coffee experience time after time.</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="addyourmilk-item v_title" data-touch="v_tab_interface" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 4 of 6</span> Interface </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>A tactile interface for an optimized convenience</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="addyourmilk-item v_title" data-touch="v_tab_cleanSystem" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 5 of 6</span> Clean system </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>Manual clean system with a hygienic certification granted by an external laboratory to ensure higher standards of cleanliness.</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="addyourmilk-item v_title" data-touch="v_tab_upliftedDesign" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 6 of 6</span> Uplifted design </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>Sleek and modern design with a wide choice of colors available.</p>
			                     </div>
			                  </li>
			               </ul>
			            </div>
			         </div>
			      </div>
			   </div>
			</section>
			<section class="vue_carousel v_key_carousel v_slide4" id="carousel" data-label="Carousel">
			    <h2 class="v_visually_hidden">Gallery</h2>
			    <p class="v_visually_hidden">Use ENTER or SPACE key on thumbnails buttons to enter slider mode</p>
			    <div class="vue_imageGrid">
			        <div>
			        	<span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/1_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/1_L.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/1_L.jpg');" lazy="loaded">
			        	<button tabindex="-1" aria-hidden="true"></button></span>
			        </div><div>
			            <div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/2_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/2_L.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/2_L.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> </span> </div><div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/3_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/3_L.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/3_L.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> <img class="v_visually_hidden" alt="Image 3" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/3_L.jpg" lazy="loaded"> </span> </div><div>
			            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/4_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/4_L.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/4_L.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div><div>
			            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/5_L.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/5_L.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/5_L.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div>
			        </div>
			    </div>
			    <div id="modal-container" class="image-modal">
	  				<img class="image-modal-content" id="modal_img" src="">
				</div>
			</section>
			<section class="vue_faq v_sectionLeft v_key_faq" id="faq" data-label="Faq">
			   <div class="bg_normal bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/faq_L.jpg');" lazy="loaded"></div>
			   <div class="bg_normal bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/faq_S.jpg');" lazy="loaded"></div>
			   <div class="v_sectionRestrict">
			      <div class="v_sectionContent">
			         <div class="v_text">
			            <h2 data-wow="" class="wow">Frequently asked questions</h2>
			            <div class="vue_accordion">
			               <ul role="accordion" class="v_isCollapsable">
			                  <li class="v_accordionItem">
			                     <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 1 of 4</span> What kind of beverages can I prepare with the machine? </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>This machine allows the preparation of 6 beverages accessible through 6 tactile buttons (<strong class="v_brand" term="ristretto">Ristretto</strong>, Espresso, Lungo, Cappuccino, Latte macchiato, Warm milk froth). All the beverages are preprogramed with factory setting but can be reprogrammed to fit one’s taste.</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 2 of 4</span> Can I use different cup sizes and mugs? </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>The machine integrates a sliding drip tray allowing the use of a wide range of cups, mugs, and jug sizes. The maximum height of the jug must be 143.5 mm</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 3 of 4</span> What are the factory settings concerning the beverages volume? </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p><strong class="v_brand" term="lattissima touch">Lattissima Touch</strong> machines are pre-programmed at 25ml for the small size (<strong class="v_brand" term="ristretto">Ristretto</strong>), 40 ml for the medium one (Espresso) and 110 ml for the large one (Lungo).<br>Pre-programmed volume for milk recipes are the following: 50 ml milk froth and 40 ml coffee for cappuccino, 150ml milk froth and 40 ml coffee for latte macchiato, 120 ml for warm milk.<br>We recommend you to keep factory settings for <strong class="v_brand" term="ristretto">Ristretto</strong>, Espresso and Lungo to ensure the best in cup results for each of our coffee varieties.</p>
			                     </div>
			                  </li>
			                  <li class="v_accordionItem">
			                     <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 4 of 4</span> How do I reprogram the beverages volumes to the quantity I want? </button>
			                     <div class="v_wysiwyg" aria-hidden="true">
			                        <p>Volume programing with the machine is very easy. For <strong class="v_brand" term="ristretto">Ristretto</strong>, Espresso or Lungo, simply touch and hold the button for at least 3 sec to enter the programming mode. Brewing starts and button blinks fast (programming mode). Release button when desired volume is reached.<br>For Cappuccino, Latte Macchiatto and warm milk froth, touch and hold the button for at least 3 sec, button blinks fast (programming mode). Release button when desired milk froth volume is reached. Button still blinks fast as we are still in programming mode for Cappuccino and Latte Macchiato. When programming the Warm milk froth beverage, the programming ends here.<br>Touch and hold the same button. Brewing starts. Release button when desired coffee volume is reached. Recipe volume is now stored for the next preparations. Button blinks 3 times to confirm the new volume.</p>
			                        <p>We recommend you to keep factory settings for <strong class="v_brand" term="ristretto">Ristretto</strong>, Espresso and Lungo to ensure the best in cup results for each of our coffee varieties.</p>
			                     </div>
			                  </li>
			               </ul>
			            </div>
			         </div>
			      </div>
			   </div>
			</section>
			<section class="vue_photo v_key_photo" id="photo" data-label="Photo">
			   <div class="bg_parallax skrollable skrollable-between bg-xl" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/photo_L.jpg'); transform: translate3d(0px, -45.5975%, 0px);" lazy="loaded">  </div>
			   <div class="bg_parallax bg-small" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/lattisima-touch/photo_S.jpg');" lazy="loaded">  </div>
			</section>
			<section class="vue_relativeProducts v_sectionLight v_key_relativeProducts" id="relativeProducts" data-label="Relative Products">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover all the <strong class="v_brand" term="nespresso">Nespresso</strong> machines</h2>
			            <div class="v_slider v_sliderFlex">
			                <button class="v_sliderArrow v_sliderPrev" tabindex="-1" aria-hidden="true"> <i class="fn_angleLeft"></i> </button>
			                <div class="v_slideContainer" style="touch-action: pan-y; -moz-user-select: none;">
			                    <div class="v_slide" style="transform: translateX(0); transition: all 1.3s ease 0.15s;">
			                        <ul aria-hidden="true">
			                        	<?php foreach ($machines as $key => $machine):?>
	                                        <?php foreach($products as $k => $product): ?>
	                                            <?php if (html_entity_decode($machine) == html_entity_decode($product->name)): ?>
	                                                <li class="nepresso-machine-product" style="width: 199.2px;">
						                                <div data-product-item-id="<?= $product->post_id ?>">
						                                    <div class="v_imageContainer">
						                                        <a class="v_cell" href="<?= $product->url ?>" tabindex="-1" style="background-image: url('<?= $product->image ? $product->image[0] : '' ?>');" lazy="loading"> <span class="v_visually_hidden"><strong class="v_brand" term="<?= $product->name ?>"><?= $product->name ?></strong></span> </a>
						                                    </div>
						                                    <div class="v_sliderItemText">
						                                        <h3 aria-hidden="true"><span><strong class="v_brand" term="prodigio"><?= $product->name ?></strong></span></h3>

						                                    </div>
						                                </div>
						                            </li>
	                                            <?php endif ?>
	                                        <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </ul>
			                    </div>
			                </div>
			                <button class="v_sliderArrow v_sliderNext" tabindex="-1" aria-hidden="true"> <i class="fn_angleRight"></i> </button>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_corporate v_sectionLeft v_sectionTop v_key_corporate" id="corporate" data-label="Corporate">
			    <div class="bg_parallax skrollable skrollable-between bg-xl" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_XL.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="bg_parallax skrollable skrollable-between bg-small" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_S.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Why <strong class="v_brand" term="nespresso">Nespresso</strong>?</h2>
			                <p data-wow="" class="wow">A cup of coffee is much more than a break. It is your small ritual. Make it an unparalleled experience.</p>
			                <div class="v_wysiwyg wow" data-wow="">
			                    <p>Choose <strong class="v_brand" term="nespresso">Nespresso</strong>, do not settle for less: strictly-selected coffee coming in a matchless range of exclusive varieties, coffee machines combining smart design with simple state-of-the-art technology, refined accessories, indulgent sweet treats and services anticipating your every desire. What else?</p>
			                </div>
			                <!-- <a class="v_link v_iconLeft" href="https://www.nespresso.com/es/en/discover-le-club"> <i class="fn_arrowLink"></i> Read more about <strong class="v_brand" term="nespresso">Nespresso</strong> </a> -->
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_services v_sectionDark v_key_services" id="services" data-label="Services">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 class="v_visually_hidden"><strong class="v_brand" term="nespresso">Nespresso</strong> Services</h2>
			            <ul class="v_row5">
			                <li data-wow="" class="wow"> <img alt="" class="freeDelivery" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/freeDelivery.svg">
			                    <h3>Free delivery in the next 24 hours</h3>
			                    <p>Receive your coffee in just 24 hours at an address of your choice.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="pickup" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/pickup.svg">
			                    <h3>Pick up in a boutique today</h3>
			                    <p>Order online and collect your order at your favourite Boutique just 2 hours later.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="assistance247" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/assistance247.svg">
			                    <h3>Assistance for your machine</h3>
			                    <p>Need help with your machine? Our experts are here for you.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="payment" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/payment.svg">
			                    <h3>Secured payment transactions</h3>
			                    <p>SSL encryption means sensitive information is always transmitted securely.</p>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>
		</div>
	</div>
</main>
