<?php
$i = 0;
$with_icon = 'icon' == get_option( 'yith_wcms_timeline_step_count_type' ) && 'text' != $style ? true : false;
$enable_checkout_login_reminder = 'yes' == get_option( 'woocommerce_enable_checkout_login_reminder', 'yes' ) ? true : false;
$image_class = apply_filters( 'yith_wcms_timeline_icon_class', '' );
$show_login_step = ! $is_user_logged_in && $enable_checkout_login_reminder;
?>
<style>
span.step {
    display: block;
    font-size: 25px;
    font-weight: bold;
}
ul.steps{
	max-width: 995px;
    margin: 0 auto 15px !important;
    font-weight: bold;
    font-size: 18px;
}
.timeline.active {
    border-bottom: 3px solid #3D8705 !important;
}
li.timeline {
    padding-bottom: 10px;
}
</style>
<ul id="checkout_timeline" class="steps">
    <?php if( $show_login_step ) : ?>
        <li id="timeline-0" data-step="0" class="timeline login <?php echo ! $is_user_logged_in ? 'active' : '';?>" >
            <div class="timeline-wrapper">
                 <span class="timeline-step <?php echo $with_icon ? 'with-icon' : '' ?>">
                <?php if( $with_icon ) : ?>
                    <img src="<?php echo yith_wcms_checkout_timeline_get_icon( $style, 'login' ); ?>" alt="<?php echo $labels['login'] ?>" class="<?php echo $image_class; ?>" width="<?php echo YITH_Multistep_Checkout()->sizes['yith_wcms_timeline_' . $style]['width']; ?>" height="<?php echo YITH_Multistep_Checkout()->sizes['yith_wcms_timeline_' . $style]['width']; ?>" />
                <?php else : ?>
                    <?php echo $i= $i + 1 ?>
                <?php endif; ?>
            </span>
                <span class="timeline-label"><?php echo $labels['login'] ?></span>
            </div>
        </li>
    <?php endif; ?>
    <li id="timeline-1" data-step="1" class="timeline step1 <?php echo ! $show_login_step ? 'active' : '';?>" >
        <div class="timeline-content">            
			<span class="step">1</span>
            <span class="timeline-label"><?php echo $labels['step1'] ?></span>
        </div>
    </li>
	<li id="timeline-2" data-step="2" class="timeline step2 <?php echo $step2 ? 'active' : '';?>" >
        <div class="timeline-content">            
			<span class="step">2</span>
            <span class="timeline-label"><?php echo $labels['step2'] ?></span>
        </div>
    </li>
	<li id="timeline-3" data-step="3" class="timeline step3 <?php echo $step3 ? 'active' : '';?>" >
        <div class="timeline-content">            
			<span class="step">3</span>
            <span class="timeline-label"><?php echo $labels['step3'] ?></span>
        </div>
    </li>
</ul>