<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/**
 * get machine products
 * @var array
 */
$machine_products = get_machine_products();
$machine_products['zzz'] = null;
?>

<script>
    var machine_products = <?php echo json_encode($machine_products); ?>;
</script>

<div data-step="3" class="checkister step3 hide" >
    <section>
        <div class="main-content clearfix">

            <div class="register__machine">

                <div class="pd-20 mr-top-5 gray-bgcolor">
                    <h1 class="h1">Your machine</h1>
                    <p>Register your coffee machine</p>

                    <?php
                        $has_machine_registered = isset($_SESSION['registration_data']) && isset($_SESSION['registration_data']['has_machine_registered']) ? $_SESSION['registration_data']['has_machine_registered'] : '0';
                    ?>

                    <p>
                        <input type="radio"
                            name="has_machine_registered"
                            value="1"
                            style="margin-right: 10px;"
                            <?= $has_machine_registered == '1' ? 'checked="checked"' : '' ?>
                        >
                        I already own a machine
                    </p>
                    <p>
                        <input type="radio"
                            name="has_machine_registered"
                            value="0"
                            checked="checked"
                            style="margin-right: 10px;"
                            <?= $has_machine_registered == '0' ? 'checked="checked"' : '' ?>
                        >
                        I don't have any machine yet.
                    </p>
                </div><!-- .pd-20 -->

                <div id="has_machine_registered_yes" class="hide">

                    <div class="pd-top-20 pd-bottom-20 col-md-12 my-expresscheckout-container">
                        <h3 class="my-expresscheckout-step__title">
                           ADD A NEW MACHINE
                        </h3>
                    </div>

                    <div class="">
                        <div class="pd-top-10 pd-bottom-10 col-md-12">
                            <span class="text-gray" style="font-size: 1.2em;">
                                Mandatory fields are marked with a <span class="text-red">*</span>
                            </span>
                        </div>

                        <?php if ( $machine_products ) : ?>

                            <div class="table-responsive">

                                <table class="my-expresscheckout-machine-table mr-bottom-20">
                                    <tbody>

                                        <tr>

                                            <?php $count=1; foreach ( $machine_products as $key_name => $product ) : ?>
                                                <?php if ( $count % 4 == 1 ) : ?>
                                                    </tr>
                                                    <tr>
                                                <?php endif; ?>

                                                <td class="text-center"
                                                    data-key-name="<?= $key_name == 'zzz' ? '' : $key_name ?>"
                                                    <?= $key_name == 'zzz' ? 'style="vertical-align: middle;"' : '' ?>
                                                >
                                                    <div>
                                                        <?php if ( $key_name == 'zzz' ) : ?>
                                                            <i class="fa fa-plus-circle fa-4x" aria-hidden="true"></i>
                                                        <?php else : ?>
                                                            <img src="<?= $product->image[0] ?>" class="img-responsive">
                                                        <?php endif; ?>
                                                    </div>
                                                    <div>
                                                        <?= $key_name == 'zzz' ? 'OTHER' : $product->name ?>
                                                    </div>
                                                </td>

                                            <?php $count++; endforeach; ?>

                                        </tr>

                                    </tbody>
                                </table>

                            </div><!-- .table-responsive -->

                            <?php include ( __DIR__ . '/../my-machines/machine-details-form.php'); ?>

                        <!--     <div class="col-md-12 pd-left-20 mr-bottom-20">
                                <a href="javasript:void(0)" class="btn btn-primary"><span class="fa fa-save"></span> SAVE CHANGES</a>
                            </div>
 -->
                        <?php endif; ?>

                    </div>

                </div><!-- #has_machine_registered_yes -->
            </div><!-- .register__machine -->

            <div class="checkister-wrapper clearfix">
                 <button type="button" class="btn btn-primary btn-icon-right pull-left btn-previous-step"
                    href="javascript:void(0)"
                    title="Previous"
                    data-step="3"
                ><i class="icon icon-arrow_right"></i>Previous</button>
                <button type="button" class="btn btn-primary btn-icon-right pull-right btn-next-step"
                    href="javascript:void(0)"
                    title="Create Account"
                    data-step="3"
                ><i class="icon icon-arrow_left"></i>Create Account</button>
            </div>
        </div><!-- .main-content -->
    </section>
</div><!-- .checkister -->
