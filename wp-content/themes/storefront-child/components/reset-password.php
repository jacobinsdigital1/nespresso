<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

global $nespresso_reset_password_hook_script;
$nespresso_reset_password_hook_script = '';

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

if ( is_user_logged_in() ) {
	wp_redirect('/my-account');
    exit;
}

// this page must have token param on url
if ( !isset($_GET['token']) ) {
    wp_redirect('/lost-password');
    exit;
}

// check if there is a user with the token
$user = get_users( ['meta_key' => 'token', 'meta_value' => sanitize_text_field($_GET['token']), 'number' => 1, 'count_total' => false] );
if ( !$user || empty($user) ) {
    $nespresso_reset_password_hook_script = 'validation.error_invalid_token();';
}
$user = $user ? $user[0] : null;

// check if there is a post from my persona information form
if ( $_POST && isset($_POST['password'])) :

    $validation = new Validation($user);

    // validation with javascript modal message
    if ( !$validation->valid_password() ) :
        $nespresso_reset_password_hook_script = 'validation.set_valid(false).validate_password();';

    elseif ( !$validation->valid_password_confirmation() ) :
        $nespresso_reset_password_hook_script = 'validation.set_valid(false).validate_password_confirmation();';

    elseif ( !$validation->valid_password_same() ) :
        $nespresso_reset_password_hook_script = 'validation.set_valid(false).validate_password_same();';

    else :

        // update the password
        wp_set_password( sanitize_text_field( $_POST['password']), $user->ID );

        // remove the token
        update_user_meta( $user->ID, 'token', null );

        // show the modal message then redirect
        $nespresso_reset_password_hook_script = 'validation.success_password_reset();';

    endif;

endif;

 // put some scripts on the bottom
if ( $nespresso_reset_password_hook_script ) :
    function nespresso_lost_password_hook() {
        global $nespresso_reset_password_hook_script;
    ?>
        <script type="text/javascript">
            var validation = new Validation();
            <?= $nespresso_reset_password_hook_script ?>
        </script>
    <?php
    } // nespresso_my_info_hook()
    add_action('wp_footer', 'nespresso_lost_password_hook');
endif;

?>

<?php if ($user) : // viewable only with user toke valid?>

    <main id="content" class="fullPage fullPage__plp" id="container-reset-password">

        <div class="container">

            <div class="mr-bottom-20 row login-page__row-container">

                <div class="col-xs-12 col-md-12 login-page__col">

                    <div class="account-detail mr-bottom-20 mr-top-20">

                    	<div class="row mr-top-20">
    	                    <header class="col-md-12">
                            <?php if (strpos($_SERVER['REQUEST_URI'], '/vi/reset-password') !== false):?>
                                <h2>Đặt lại mật khẩu</h2>
                            <?php else: ?>
    	                       <h2><?= wp_title() ?></h2>
                            <?php endif?>
    	                    </header>
    					</div>

    					<div class="row">
    						<div class="col-md-6 col-sm-6 col-xs-12">

    		                   	<form method="post" 
                                    <?php if (strpos($_SERVER['REQUEST_URI'], '/vi/reset-password') !== false):?>
                                        action="<?= home_url().substr($_SERVER['REQUEST_URI'], 4) ?>"
                                    <?php else: ?>
                                        action="<?= home_url().$_SERVER['REQUEST_URI'] ?>"
                                    <?php endif?>
                                    class="form needs-validation" role="form" id="form-reset-password"
                                >
    			                   <div class="input-group input-group-generic">
                                        <label class="desktop-label col-sm-3 col-md-4" for="password">Password<span class="required">*</span></label>
                                        <div class="col-md-6">
                                            <input type="password"
                                                title="Password"
                                                id="password"
                                                name="password"
												required
												data-regix="^(?=(.*[a-z]){1,})(?=(.*[A-Z]){1,})(?=(.*[\d]){1,})(?=(.*[\!\#\$\%\&\'\(\)\*\+\,\-\.\/\:\;\<\=\>\?\@\[\]\^\_\`\{\|\}\~]){1,})(?!.*\s).{6,30}$"
                                                placeholder="Password"
                                                class="form-control col-md-6"
                                            >
                                            <small id="helppassword" data-msgvi="Mật khẩu phải có: <br>- Ký tự hoa<br>- Ký tự thường<br>- Chữ số<br>- Ký tự đặc biệt: !#$%&'()*+,-./:;<=>?@[]^_`{|}~<br>- Ít nhất 6 ký tự"  data-msg="Please enter a valid password. Must contain a combination of:<br>- Uppercase<br>- Lowercase<br>- Number<br>- Special characters: !#$%&'()*+,-./:;<=>?@[]^_`{|}~<br>- Min length 6 characters" class="text-danger"></small>
											<span class="mobile-label">Password<span class="required">*</span></span>
                                            <p style="margin-top: 0px; margin-bottom: 20px; display: block; line-height: 1em; clear: both;"><small> Choose a secure alphanumeric password with at least one uppercase letter and one special character</small></p>
                                        </div>
                                    </div>
                                    <div class="input-group input-group-generic">
                                        <label class="desktop-label col-sm-3 col-md-4" for="password_confirmation">Confirm password<span class="required">*</span></label>
                                        <div class="col-md-6">
                                            <input type="password" title="Confirm password"  required data-match="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm password" class="form-control col-md-6">
                                            <small id="helppassword_confirmation" data-msgvi="Mật khẩu xác nhận không khớp" data-msg="Passwords are not matching" class="text-danger"></small>
											<span class="mobile-label">Confirm password<span class="required">*</span></span>
                                        </div>
                                    </div>

                                    <div class="input-group">
                                    	<div class="">
                                    		<button type="submit" class="btn btn-default btn-icon-right mr-top-20" title="Submit" name="submit">Submit<i class="fa fa-send"></i></button>
                                		</div>
                                    </div>
    		                   	</form>

    	                   	</div>

                       	</div><!-- ,row -->
                   	</div>

               	</div><!-- .account-detail -->

           	</div>

       	</div>
    </main>

<?php endif; ?>

<?php

global $scripts;
$scripts[] = 'js/components/validation.js';
$scripts[] = 'js/components/reset-password.js';
?>
