<?php

/**
 * Provide a admin slider area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));

?>

<div class="wrap">
    <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
</div>

<!-- include slider config -->
<?php include_once ('nespresso-slider-display.php'); ?>

<hr>
<!-- include mobile slider config -->
<?php include_once ('nespresso-mobile-slider-display.php'); ?>

<hr>
<!-- include Slider pop up -->
<?php include_once ('nespresso-slider-pop-up.php'); ?>

<hr>
<!-- include dynamic banner config -->
<?php include_once ('nespresso-dynamic-banners-below-homepage-slider.php'); ?>

<hr>

<!-- include positive cup config -->
<?php include_once ('nespresso-positive-cup-display.php'); ?>

<hr>

<!-- include positive cup config -->
<?php include_once ('nespresso-recipies-display.php'); ?>

<hr>

<!-- include best seller config -->
<?php include_once ('nespresso-best-seller-display.php'); ?>

<hr>

<!-- include product order config -->
<?php include_once ('nespresso-product-order-display.php'); ?>

<!-- include product order config -->
<?php include_once ('nespresso-product-category-display.php'); ?>


