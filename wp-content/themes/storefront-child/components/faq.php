<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$faq_informations = [
    [
        'title' => 'PRODUCT INFORMATION',
        'icon_class' => 'icon-Information',
        'content' => [
            [
                'question' => 'Why are the capsules sold in quantities of 10?',
                'answer' => 'Nespresso coffee is packaged in sleeves which contains 10 capsules of the same blend. Each sleeve is priced between 150 300 VND to 180 000 VND, or the total price of 10 capsules.<br><br>
                    To learn more about our Grand Crus <a href="/coffee-list">here</a>.'
            ],
            [
                'question' => 'Why do you use aluminium for Nespresso capsules?',
                'answer' => 'Nespresso stands for highest coffee quality, as guaranteed by the freshness and intensity of the coffee aroma. Aluminium offers an impermeable barrier to oxygen, light, ultra-violet rays or water vapour. This protection is essential for preserving the high quality of the coffee.'
            ],
            [
                'question' => 'Is coffee bad for my health?',
                'answer' => 'No. The consumption of moderate amounts of coffee, which is 4 to 5 cups a day, is considered safe by all food authorities and does not pose any risk to human health.
                    <br><br>
                    Coffee delivers a broad range of physical and emotional benefits, and has a role to play in a balanced diet. If you are concerned about potential sensitivity to caffeine, we advise you to consult your doctor.'
            ],
			[
			'question'=>'Do Nespresso capsules have an expiration date?',
			'answer' => 'Nespresso Grand Cru coffees have a limited shelf life of 12 months since the moment they are produced. This means the freshness of coffee is well-preserved in each perfectly portioned coffee. The quality, the aromas and the taste remain optimal for no less than 12 months. As all Nespresso Grands Crus are produced in Switzerland, the remaining shelf life of a Nespresso capsule after arriving in Vietnam, is reduced by a few months, due to shipping, customs clearance and distribution.
			<br><br>
Our Grand Cru coffees do not have an expiry date, but a best before date. This means the quality, the aromas and the taste remain optimal for no less than twelve months. Past these 12 months, there might be a small reduction in the aroma intensity in the blends, but they are still good for consumption.'
			],
			[
			'question'=>'What happens if I use one of your capsules after the 12-month shelf life?',
			'answer' => 'There is no health or safety risk in consuming one of our Grands Crus past its best before date. The aluminium used in our capsules provides an impermeable barrier to light, oxygen and humidity, which perfectly protects the freshness of our coffees.'
			]
            // [
            //     'question' => 'What is the Welcome Offer and how can I order this?',
            //     'answer' => 'We offer new Nespresso Club Members the chance to benefit from a Welcome Offer.
            //         <br><br>
            //         We have three different variations designed to introduce you to the world of Nespresso Grand Crus: All Time Favorites, Moments of the Day, and Travel Around the World. Each set contains a pre-selected assortment of 150 capsules, and comes with a choice of gift.
            //         <br><br>
            //         Please note, the Welcome Offer is available on the Club Member’s first to third coffee order only. Club Members can only redeem the Welcome Offer once per Club Member account number. Welcome Offer coffee selections and gifts are subject to stock availability. Selections of complimentary accessories will be determined by Nespresso, and will not include the full accessories range. '
            // ],
        ]
    ],

    [
        'title' => 'ORDERS',
        'icon_class' => 'icon-bag',
        'content' => [
            [
                'question' => 'What are your delivery terms?',
                'answer' => 'Nespresso makes every possible effort to ensure deliveries in the Ho Chi Minh City area within 1- 2 working days, to Provincial areas within 3 – 5 working days, and to Out of Delivery Zones within 3 – 5 working days via courier hub pick-up, after order has been confirmed. Working days for Nespresso Vietnam are defined as Mondays to Saturdays.
                    <br><br>
                    If no one is available to receive order upon delivery, the courier will inform Nespresso. A Coffee Specialist will get in touch with you to schedule the date of your preferred delivery. You will be charged a delivery fee upon the 3rd attempt.'
                    // <br><br>
                    // Working days for Nespresso Vietnam are defined as Mondays to Saturdays.
                    // <br><br>
                    // You are encouraged to have an alternative representative to receive your order should you be unavailable on the date and time of delivery. This is can be done during the checkout process. If no one is available to receive the order upon delivery, the courier will automatically deliver the next day. If order was still not received, your order will be put on hold* and delivered only until you have confirmed a delivery date with our Coffee Specialists. Customer will be charged P150 to be paid in cash for any 3rd attempt delivery.

                    // *Learn more about our Terms and Conditions <a href="#">here</a>.
            ],
            [
                'question' => 'Where can I buy Nespresso Capsules?',
                'answer' => 'Our 11 Grand Crus can be purchased online at www.nespresso.vn. You can order using your desktop, tablet, or any mobile device.
                    <br><br>
                    If you need assistance in ordering, please call our Coffee Specialists any time between Monday to Saturdays, 10am to 6pm at 1900 633 474.'
            ],
            [
                'question' => 'Where can I buy Nespresso machines?',
                'answer' => 'You can buy Nespresso machines online at www.nespresso.vn. You can order using your desktop, tablet, or any mobile device.
                    <br><br>                   
                    If you need assistance in ordering, please call our Coffee Specialists any time between Monday to Saturdays, 10am to 6pm at 1900 633 474.'
            ],
			[
                'question' => 'What does my order status mean?',
                'answer' => "<table id='sts'>
				<tr><td>Payment Pending</td><td>Your order has been received, waiting for online payment by credit card.</td></tr>
				<tr><td>Failed Payment</td><td>The Online payment by credit card didn't succeed.</td></tr>
				<tr><td>Exit Payment</td><td>The Online payment by credit card didn't succeed.</td></tr>
				<tr><td>Pending</td><td>Your order has been successfully placed.</td></tr>
				<tr><td>On Hold</td><td>Your order is currently On Hold. Please kindly contact Nespresso CS team for further information.</td></tr>
				<tr><td>Processing</td><td>Your order is being prepared by Nespresso team.</td></tr>
				<tr><td>In Transit</td><td>Your order is being delivered to you.</td></tr>
				<tr><td>Cancelled</td><td>Your order has been cancelled. Please kindly contact Nespresso CS team for further information.</td></tr>
				<tr><td>Completed</td><td>Your order has been sucessfully delivered. If you haven't received the order, please kindly contact Nespresso CS team for further information.</td></tr>
				<tr><td>Undelivered</td><td>Your order couldn't be delivered.  If you haven't received the order, please kindly contact Nespresso CS team for further information.</td></tr>
				<tr><td>Ready to Collect</td><td>Your order is now ready to be collected in the selected partner store.</td></tr>
				<tr><td>Refunded</td><td>Your payment has been refunded.  If you haven't received the order, please kindly contact Nespresso CS team for further information.</td></tr>
				</table>"
            ]
        ]
    ],

    [
        'title' => 'ONLINE SUPPORT',
        'icon_class' => 'icon-online_support',
        'content' => [
            [
                'question' => 'I forgot my password. What should I do?',
                'answer' => 'You may access your account again by clicking the “Forgot Password” button at the Login Page. You will be asked to input your email address, and an activation link will be sent to reset your password.
                    <br><br>
                    If you need assistance, please call our Coffee Specialists any time between Monday to Saturdays, 10am to 6pm at 1900 633 474.'
            ],
            // [
            //     'question' => 'How can I register another machine?',
            //     'answer' => 'You can have more than one machine per Club Member ID or account.
            //         <br><br>
            //         Step 1: Log in your account at www.nespresso.vn <br>
            //         Step 2: Under <strong>My Account</strong>, select <strong>My Personal Information</strong><br>
            //         Step 3: Click on <strong>My Machine</strong><br>
            //         Step 4: Select <strong>Add Machine</strong> and enter the required fields
            //         <br><br>
            //         If you have trouble locating your machine’s serial number, or if you cannot recall your purchase date, please call our Coffee Specialists any time between Monday to Saturdays, 10am to 6pm at 1900 633 474.'
            // ],
            // [
            //     'question' => 'What if my machine is already registered abroad?',
            //     'answer' => 'Nespresso Club Membership is specific to the country where you are registered. We advise you to register in our website at www.nespresso.vn to activate your <strong>Nespresso Vietnam Club Member ID</strong>.
            //         <br><br>
            //         This will help you to monitor your machine’s warranty, and get exclusive news and invites to local promotions and events.'
            // ],
        ]
    ],

    [
        'title' => 'PRODUCT ISSUE',
        'icon_class' => 'icon-Cup_size_espresso-lungo',
        'content' => [
            [
                'question' => 'Why is there is a change in the coffee taste of my capsules?',
                'answer' => 'To maintain the optimum temperature, flow, and quality of your coffee, you are advised to descale your machine after every 400-600 capsules, or at least once a year. Nespresso offers this cleaning kit that has been designed for the Nespresso systems and its components.
                    <br><br>
                    Another reason could be is that you are using the large cup (Lungo) button for our Espresso blends, which will over extract coffee from the capsule. This means that the coffee inside will stop producing flavor after around 40ml. The coffee may also start to burn, making it taste bitter. Over extracting Espresso capsules will put unnecessary strain on a Nespresso machine’s pump. If this is the case, you will need to descale your machine more frequently.'
            ],
            [
                'question' => 'Why is Nespresso coffee more expensive than coffee found in stores? ',
                'answer' => 'Nespresso capsules are competitively priced based on the quality of our coffee. We select the highest quality beans from the world’s finest coffee-producing regions. Only the top 1-2% of the world’s coffee crop meets our specific taste and aroma profile.'
                    // <br><br>
                    // We also offer best-in-class personalized services that match the lifestyles and preferences of our Nespresso Club Members.'
            ],
        ]
    ],

    [
        'title' => 'MACHINE SUPPORT',
        'icon_class' => 'icon-machine',
        'content' => [
            [
                'question' => 'How long is the Nespresso Warranty in the Vietnam?',
                'answer' => 'Nespresso Vietnam honors a 1 year warranty for machines purchased in the Vietnam.
                    <br><br>
                    If your machine is purchased and is registered abroad, you may or may not be charged for the technical services. This will depend on your current warranty status, machine model, and condition of the repair.
                    <br><br>
                    To learn more about your machine’s warranty, please call our Coffee Specialists any time between Monday to Saturdays, 10am to 6pm at 1900 633 474.'
            ],
            [
                'question' => 'What is a Descaling Kit and how often should I descale my machine?',
                'answer' => 'In order to keep your Nespresso machine in best condition, you are advised to descale your machine after every 400 to 600 capsules, or at least once a year. This step ensures the optimal temperature and quality of your espresso.'
            ],
            [
                'question' => 'Where can I find my machine’s serial number?',
                'answer' => 'Your Nespresso machine’s serial number consists of 19 alphanumeric characters located in one of the following areas on the machine: <br>
                    <ul>
                        <li>On the drip tray, printed on a removable sticker</li>
                        <li>At the base of the machine, printed on a removable sticker</li>
                        <li>On the original box of the machine when it was delivered</li>
                    </ul>
                    <br><br>
                    If you have trouble finding your machine’s serial number, please call our Coffee Specialists any time between Monday to Saturdays, 10am to 6pm at 1900 633 474.'
            ],
            [
                'question' => 'If non-Nespresso capsules are used in Nespresso machines, does this void the warranty?',
                'answer' => 'The use of non-Nespresso capsules on Nespresso machines does not void the warranty; however, there may be defects and dysfunctions resulting from the use of these products which, if proven, are not covered by the guarantee.
                    <br><br>
                    Nespresso capsules and machines were designed to work together as a single system to deliver the perfect cup of coffee time after time. Remember, only 1-2% of the world coffee crop meets our strict quality and aroma profile requirements.'
            ],
        ]
    ],

    [
        'title' => 'BRAND & PROMOTIONS',
        'icon_class' => 'icon-Stick_a_label_on',
        'content' => [
            [
                'question' => 'What is ÂN NAM FINE FOOD CO., LTD. ?',
                'answer' => 'ÂN NAM FINE FOOD CO., LTD. is the official distributor of Nespresso in the Vietnam.
                    <br><br>
                    322 Dien Bien Phu street<br>
                    Ward 22, Binh Thanh District,<br>
                    Ho Chi Minh City, Vietnam'
                    // <br><br>
                    // You may call us at +632 727-4370, or email us at corporate@novateur.ph.'
            ],
            [
                'question' => 'How can I submit a proposal or a sponsorship request?',
                'answer' => 'If you would like to collaborate with Nespresso Vietnam, you can send an email detailing your request to contact@nespresso.vn .
                    <br><br>
                    We will contact you if we are interested.'
            ],
            [
                'question' => 'Does Nespresso offer Fair Trade coffee?',
                'answer' => 'It is estimated that approximately 6% of the coffee that we purchase is certified Fairtrade, as a number of farmers in our AAA Program are also Fairtrade certified. The core of Nespresso’s commitment to sustainability and farmer welfare is built around the Nespresso AAA Sustainable Quality Program, which incorporates over 63,000 farmers across 11 countries. We understand and respect the responsibility that Nespresso has in creating a sustainable coffee economy, and in embedding our best practice principles throughout our value chain, from coffee cherry to the cup.
                    <br><br>
                    Unlike the Fairtrade label, which sets a minimum price, Nespresso pays a premium price of 30% to 40% above standard market price, and 10% to 15% above coffees of similar quality.
                    <br><br>
                    The Positive Cup Program also has goals in the areas of coffee sourcing and social welfare.
                    <br><br>
                    To learn more about our AAA Program and other sustainability efforts, please call our Coffee Specialists any time between Monday to Saturdays, 7am to 7pm at 1900 633 474.'
            ],
        ]
    ],

]; // $faq_informations

?>

<!-- content -->
<div class="container" id="faq">
    <div class="row">
        <div class="customer-services">
            <div class="customer-services-title">
                <h2><?= get_the_title() ?></h2>
            </div>
            <div class="customer-services-main clearfix">

                <div class="col-xs-12">
<style>
.faq-answer{
	
}
span.q {
    border: 1px solid;
    padding: 0px 3px;
    border-radius: 10px;
}
.a-click{
	cursor:pointer;
}
#sts td{
	background-color:#1B1B1B;
}
</style>
                    <?php foreach ( $faq_informations as $i=>$faq_information ) : ?>

                        <div class="customer-services-bloc col-xs-12 col-sm-6 col-md-4">
                            <div class="col-xs-12">
                                <div class="col-xs-12 customer-services-bloc-content">
                                    <!-- <i class="icon icon-FAQ_on icon-title"></i> -->
                                    <i class="icon <?= $faq_information['icon_class'] ?> icon-title"></i>
                                    <h3><?= $faq_information['title'] ?></h3>

                                    <div class="faq-content-item mr-top-5 mr-bottom-5 hide">
                                        <?php foreach ( $faq_information['content'] as $ii=>$info ) : ?>

                                            <div class="pd-top-10 pd-bottom-10 border-bottom-white">
                                                <p class="customer-services-bloc-description a-click" data-id="<?=$i.$ii?>">
                                                    <span class="q"><i class="fa fa-plus"></i></span> <?= $info['question'] ?>
                                                </p>
                                                <div class="faq-answer faq-answer<?=$i.$ii?> hidden ">
                                                    A: <?= $info['answer'] ?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div><!-- .mr-top-5 -->

                                    <a class="link-btn btn-show-modal uppercase clearfix"
                                        href="javascript:void(0)"
                                    >
                                        <span class="fa fa-long-arrow-right"></span> Learn more
                                    </a>

                                </div><!-- .ustomer-services-bloc-content -->
                            </div><!-- .col-xs-12 -->
                        </div><!-- .customer-services-bloc -->

                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>
</div><!-- .container -->
<!-- /content -->
<script>
jQuery(function(){
	jQuery(document).on('click','.a-click',function(){
		var _that = $(this),_id = _that.data('id');
		//alert(_id);
		jQuery('.a-click>span>i').addClass('fa-plus').removeClass('fa-minus');
		jQuery('.faq-answer').addClass('hidden') ;
		jQuery('.faq-answer'+_id).removeClass('hidden');
		jQuery('.a-click[data-id="'+_id+'"]>span>i').removeClass('fa-plus').addClass('fa-minus');
	});
})
</script>
<?php
global $scripts;
$scripts[] = 'js/components/nespressoModal.js';
$scripts[] = 'js/components/faq.js';
?>
