/**
 * discover-nespress.js
 */

$(document).ready(function() {

	if($(window).width() > '740') {
		$("html").addClass("desktop");	
	} else {
		$("html").addClass("mobile");
	}
	
	$('a[href^="#"]').on('click', function(event) {

	    var target = $(this.getAttribute('href'));
	    target.slideDown();
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top - 79
	        }, 500);
	    }

	});

	//initialize data
	var intensity = [];
	var cupsize = [];
	var aromatic_profile = [];
	var ranges = [];

	var filter_intensity = [];
	var filter_cupsize = [];
	var filter_aromaticprofile = [];
	var filter_range = [];

	$('.range-filter ').on('click', function () {
		$.each($('.range-filter '), function(index, value) {
			if(value.checked) {
				$('.v_filters').addClass('v_hasFilters');
				return false;
			}else{
				$('.v_filters').removeClass('v_hasFilters');
			}
		});

		if($('#ranges').value != 'default') {
			$('.v_filters').addClass('v_hasFilters');
		}

	});

	//reset filter

	$('.v_reset').on('click', function() {

		$('.v_filters').removeClass('v_hasFilters');

		reset_all();
	});

	//Main tab filter
	$('.v_tabLink').on('click', function () {

		reset_all();
		
		var filter_by = $(this).data('filter');
		$('.v-link').removeClass('v-link-active');
		$('.vue_coffeeSelector').addClass('hide');
		$('.v_filterRange').removeClass('hide');

		if(filter_by == 'range') {
			$('.v_filterIntensity').removeClass('hide');
			$('.v_filterCupsizes').removeClass('hide');
			$('.v_filterAromaticProfiles').removeClass('hide');
			$('.v_filterRange').addClass('hide');
			$('.v-link-range').addClass('v-link-active');
			$('#coffee-selector-range').removeClass('hide');
		} else if (filter_by == 'intensity') {
			$('.v_filterIntensity').addClass('hide');
			$('.v_filterCupsizes').removeClass('hide');
			$('.v_filterAromaticProfiles').removeClass('hide');
			$('.v-link-intensity').addClass('v-link-active');
			$('#coffee-selector-intensity').removeClass('hide');
		}
		 else if (filter_by == 'aromatic profile') {
			$('.v_filterIntensity').removeClass('hide');
			$('.v_filterCupsizes').removeClass('hide');
			$('.v_filterAromaticProfiles').addClass('hide');
			$('.v-link-aromatic-profile').addClass('v-link-active');
			$('#coffee-selector-aromatic-profile').removeClass('hide');
		}
	});

	//Add filter button
	$('.v_more').on('click',function () {
		if($(this).attr("aria-expanded") == 'false') {
			$('.v_filterDetails').addClass('v_opened');
			$(this).attr("aria-expanded", true);
			$(this).addClass('v_open');
		} else {
			$('.v_filterDetails').removeClass('v_opened');
			$(this).attr("aria-expanded", false);
			$(this).removeClass('v_open');
		}

	});

	//Filter by range 
	$('.v_range_filter').on('click', function () {
		$('.close-modal').click();
		//get selected category
		var selected_filter_category = $(this).data('filter');

		//reset the filters
		$('.intensity-filter').attr('checked', false);
		$('.cupSize-filter').attr('checked', false);
		$('.aromaticProfile-filter').attr('checked', false);

		if($(this).attr("aria-selected") == 'false') {
			//show filter reset
			$('.v_filters').addClass('v_hasFilters');
			//activate filter
			$(this).attr("aria-selected", true);
			$(this).addClass('v_active v_filter_range_active');

			//de-activate all capsule first
			$('.v-capsules').removeClass('v-capsules-active');
			$('.v-capsules').addClass('v_shadowed');
			$('.v-capsules').addClass('v-capsules-inactive');

			//activate all selected filter
			$.each($('.v_filter_range_active'), function(index, value) {
				//activate capsule base on selected filter
				$('.v-capsule-category-'+value.dataset.filter).addClass('v-capsules-active');
				$('.v-capsule-category-'+value.dataset.filter).removeClass('v_shadowed');
				$('.v-capsule-category-'+value.dataset.filter).removeClass('v-capsules-inactive');

				//get all possible active filter
				$.each($('.v-capsule-category-'+value.dataset.filter), function (index, filter_value) {
					cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
					intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
					aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
				});
			});

			disable_enabled_filter(intensity, cupsize, aromatic_profile);
			
		} else {

			cupsize = [];
			intensity = [];
			aromatic_profile = [];

			//de-activate filter
			$(this).attr("aria-selected", false);
			$(this).removeClass('v_active v_filter_range_active');

			if ($('.v_filter_range_active').length > 0) {

				//remove activated clicked 
				$('.v-capsule-category-'+selected_filter_category).removeClass('v-capsules-active');
				$('.v-capsule-category-'+selected_filter_category).addClass('v_shadowed');
				$('.v-capsule-category-'+selected_filter_category).addClass('v-capsules-inactive');

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(intensity, cupsize, aromatic_profile);

			} else { //unselect all

				//reset the filters
				$('.intensity-filter').attr('checked', false);
				$('.cupSize-filter').attr('checked', false);
				$('.aromaticProfile-filter').attr('checked', false);

				//remove disabled
				$('.intensity-filter').prop('disabled',false); 
				$('.cupSize-filter').prop('disabled',false); 
				$('.aromaticProfile-filter').prop('disabled',false);

				//hide filter reset
				$('.v_filters').removeClass('v_hasFilters');

				//activate all capsule
				$('.v-capsules').addClass('v-capsules-active');
				$('.v-capsules').removeClass('v_shadowed');
				$('.v-capsules').removeClass('v-capsules-inactive');
			}

		}

	});

	$('.intensity-filter').on('click', function () {
		var filter_capsule_value = $(this).val();
		intensity = [];
		cupsize = [];
		aromatic_profile = [];
		if ($(this).prop('checked')) {
			filter_intensity.push(filter_capsule_value);

			//with main filter
			if ($('.v_filter_range_active').length > 0) {

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					if(filter_capsule_value != value.dataset.intensity) {
						value.classList.add('v_shadowed');
						value.classList.add('v-capsules-inactive');
						value.classList.remove('v-capsules-active');
					}
				});

				//disable other intensity filter
				$.each($('.intensity-filter'), function (index, value) {
					if (!value.checked) {
						value.disabled = true;
					}
				});

				//enable other cupsize/aromatic button filter
				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(true, cupsize, aromatic_profile);

			} else { 
				//deactive all capsule first 
				$('.v-capsules').removeClass('v-capsules-active');
				$('.v-capsules').addClass('v_shadowed');
				$('.v-capsules').addClass('v-capsules-inactive');

				//activate only the selected 
				$.each($('.intensity-filter'), function (index, filter_value) {
					if (filter_value.checked) {
						$.each($('.v-capsules.v-capsules-inactive'), function(index, value) {

							if(filter_value.value == value.dataset.intensity) {
								value.classList.remove('v_shadowed');
								value.classList.remove('v-capsules-inactive');
								value.classList.add('v-capsules-active');
							}
						});
					}
				});

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(true, cupsize, aromatic_profile);
			}

		} else {
			remove_item(filter_intensity, filter_capsule_value);

			if ($('.v_filter_range_active').length > 0) {

				filter_intensity = [];

				$.each($('.v_filter_range_active'), function(index, value) {
					//activate capsule base on selected filter
					$('.v-capsule-category-'+value.dataset.filter).addClass('v-capsules-active');
					$('.v-capsule-category-'+value.dataset.filter).removeClass('v_shadowed');
					$('.v-capsule-category-'+value.dataset.filter).removeClass('v-capsules-inactive');

					//get all possible active filter
					$.each($('.v-capsule-category-'+value.dataset.filter), function (index, filter_value) {
						cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
					});

					disable_enabled_filter(intensity, cupsize, aromatic_profile);
				});

			} else {

				if (filter_intensity.length > 0) {
					$.each($('.v-capsules.v-capsules-active'), function(index, value) {

						if(filter_capsule_value == value.dataset.intensity) {
							value.classList.add('v_shadowed');
							value.classList.add('v-capsules-inactive');
							value.classList.remove('v-capsules-active');
						}
					});


					$.each($('.v-capsules.v-capsules-active'), function(index, value) {
						cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
					});

					disable_enabled_filter(true, cupsize, aromatic_profile);

				} else {
					//activate all capsule
					$('.v-capsules').addClass('v-capsules-active');
					$('.v-capsules').removeClass('v_shadowed');
					$('.v-capsules').removeClass('v-capsules-inactive');

					reset_filter();

				}
				
			}
		}
	});

	$('.cupSize-filter').on('click', function () {
		var filter_capsule_value = $(this).val();
		intensity = [];
		cupsize = [];
		aromatic_profile = [];
		if ($(this).prop('checked')) {
			filter_cupsize.push(filter_capsule_value);

			//with main filter
			if ($('.v_filter_range_active').length > 0) {

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					
					cup_size_array = value.dataset.cupsize.split(',')
					cup_size_result = cup_size_array.indexOf(filter_capsule_value);

					if(cup_size_result == -1) {
						value.classList.add('v_shadowed');
						value.classList.add('v-capsules-inactive');
						value.classList.remove('v-capsules-active');
					}
				});

				//enable other cupsize/aromatic button filter
				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(intensity, cupsize, aromatic_profile);

			} else { 
				//deactive all capsule first 
				$('.v-capsules').removeClass('v-capsules-active');
				$('.v-capsules').addClass('v_shadowed');
				$('.v-capsules').addClass('v-capsules-inactive');

				//activate only the selected 
				$.each($('.cupSize-filter'), function (index, filter_value) {
					if (filter_value.checked) {
						$.each($('.v-capsules.v-capsules-inactive'), function(index, value) {
							var cup_size = value.dataset.cupsize.split(',');
							$.each(cup_size, function(index, cup_size_value) {
								if(filter_value.value == cup_size_value) {
									value.classList.remove('v_shadowed');
									value.classList.remove('v-capsules-inactive');
									value.classList.add('v-capsules-active');
								}
							});
						});
					}
				});

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(intensity, cupsize, aromatic_profile);
			}

		} else {
			remove_item(filter_cupsize, filter_capsule_value);

			if ($('.v_filter_range_active').length > 0) {

				filter_cupsize = [];

				$.each($('.v_filter_range_active'), function(index, value) {
					//activate capsule base on selected filter
					$('.v-capsule-category-'+value.dataset.filter).addClass('v-capsules-active');
					$('.v-capsule-category-'+value.dataset.filter).removeClass('v_shadowed');
					$('.v-capsule-category-'+value.dataset.filter).removeClass('v-capsules-inactive');

					//get all possible active filter
					$.each($('.v-capsule-category-'+value.dataset.filter), function (index, filter_value) {
						cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
					});

					disable_enabled_filter(intensity, cupsize, aromatic_profile);
				});

			} else {

				if (filter_cupsize.length > 0) {
					$.each($('.v-capsules.v-capsules-active'), function(index, value) {

						if(filter_capsule_value == value.dataset.intensity) {
							value.classList.add('v_shadowed');
							value.classList.add('v-capsules-inactive');
							value.classList.remove('v-capsules-active');
						}
					});


					$.each($('.v-capsules.v-capsules-active'), function(index, value) {
						cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
					});

					disable_enabled_filter(true, cupsize, aromatic_profile);

				} else {
					//activate all capsule
					$('.v-capsules').addClass('v-capsules-active');
					$('.v-capsules').removeClass('v_shadowed');
					$('.v-capsules').removeClass('v-capsules-inactive');

					reset_filter();

				}
				
			}
		}
	});

	$('.aromaticProfile-filter').on('click', function () {
		var filter_capsule_value = $(this).val();
		intensity = [];
		cupsize = [];
		aromatic_profile = [];
		if ($(this).prop('checked')) {
			filter_aromaticprofile.push(filter_capsule_value);

			//with main filter
			if ($('.v_filter_range_active').length > 0) {

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					if(filter_capsule_value != value.dataset.aromaticprofile) {
						value.classList.add('v_shadowed');
						value.classList.add('v-capsules-inactive');
						value.classList.remove('v-capsules-active');
					}
				});

				//enable other cupsize/aromatic button filter
				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(intensity, cupsize, aromatic_profile);

			} else { 
				//deactive all capsule first 
				$('.v-capsules').removeClass('v-capsules-active');
				$('.v-capsules').addClass('v_shadowed');
				$('.v-capsules').addClass('v-capsules-inactive');

				//activate only the selected 
				$.each($('.aromaticProfile-filter'), function (index, filter_value) {
					
					if (filter_value.checked) {
						$.each($('.v-capsules.v-capsules-inactive'), function(index, value) {

							if(filter_value.value == value.dataset.aromaticprofile) {
								value.classList.remove('v_shadowed');
								value.classList.remove('v-capsules-inactive');
								value.classList.add('v-capsules-active');
							}
						});
					}
				});

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(intensity, cupsize, aromatic_profile);
			}

		} else {
			remove_item(filter_aromaticprofile, filter_capsule_value);

			if ($('.v_filter_range_active').length > 0) {

				filter_aromaticprofile = [];

				$.each($('.v_filter_range_active'), function(index, value) {
					//activate capsule base on selected filter
					$('.v-capsule-category-'+value.dataset.filter).addClass('v-capsules-active');
					$('.v-capsule-category-'+value.dataset.filter).removeClass('v_shadowed');
					$('.v-capsule-category-'+value.dataset.filter).removeClass('v-capsules-inactive');

					//get all possible active filter
					$.each($('.v-capsule-category-'+value.dataset.filter), function (index, filter_value) {
						cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
					});

					disable_enabled_filter(intensity, cupsize, aromatic_profile);
				});

			} else {

				if (filter_aromaticprofile.length > 0) {
					$.each($('.v-capsules.v-capsules-active'), function(index, value) {

						if(filter_capsule_value == value.dataset.filter_aromaticprofile) {
							value.classList.add('v_shadowed');
							value.classList.add('v-capsules-inactive');
							value.classList.remove('v-capsules-active');
						}
					});


					$.each($('.v-capsules.v-capsules-active'), function(index, value) {
						cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
					});

					disable_enabled_filter(intensity, cupsize, aromatic_profile);

				} else {
					//activate all capsule
					$('.v-capsules').addClass('v-capsules-active');
					$('.v-capsules').removeClass('v_shadowed');
					$('.v-capsules').removeClass('v-capsules-inactive');

					reset_filter();

				}
				
			}
		}
	});

	//Filter By Intensity
	$('.v_intentsity_filter').on('click', function () {
		//get selected category
		var selected_filter_intensity = $(this).data('filter');

		//reset the filters
		$('.intensity-filter').attr('checked', false);
		$('.cupSize-filter').attr('checked', false);
		$('.aromaticProfile-filter').attr('checked', false);
		$('#ranges').val('default').removeClass('v_selected');

		if($(this).attr("aria-selected") == 'false') {
			//show filter reset
			$('.v_filters').addClass('v_hasFilters');
			//activate filter
			$(this).attr("aria-selected", true);
			$(this).addClass('v_active v_filter_range_active');

			//de-activate all capsule first
			$('.v-capsules').removeClass('v-capsules-active');
			$('.v-capsules').addClass('v_shadowed');
			$('.v-capsules').addClass('v-capsules-inactive');

			//activate all selected filter
			$.each($('.v_filter_range_active'), function(index, value) {
				//activate capsule base on selected filter
				$('.v-intensity-'+value.dataset.filter).addClass('v-capsules-active');
				$('.v-intensity-'+value.dataset.filter).removeClass('v_shadowed');
				$('.v-intensity-'+value.dataset.filter).removeClass('v-capsules-inactive');

				//get all possible active filter
				$.each($('.v-intensity-'+value.dataset.filter), function (index, filter_value) {
					cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
					intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
					aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
					ranges = merge_array(ranges, filter_value.dataset.category.split(','));
				});
			});

			disable_ranges(ranges);
			disable_enabled_filter(intensity, cupsize, aromatic_profile);
			
		} else {

			cupsize = [];
			intensity = [];
			aromatic_profile = [];

			//de-activate filter
			$(this).attr("aria-selected", false);
			$(this).removeClass('v_active v_filter_range_active');

			if ($('.v_filter_range_active').length > 0) {

				//remove activated clicked 
				$('.v-intensity-'+selected_filter_intensity).removeClass('v-capsules-active');
				$('.v-intensity-'+selected_filter_intensity).addClass('v_shadowed');
				$('.v-intensity-'+selected_filter_intensity).addClass('v-capsules-inactive');

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(intensity, cupsize, aromatic_profile);

			} else { //unselect all

				//reset the filters
				$('.intensity-filter').attr('checked', false);
				$('.cupSize-filter').attr('checked', false);
				$('.aromaticProfile-filter').attr('checked', false);

				//remove disabled
				$('.intensity-filter').prop('disabled',false); 
				$('.cupSize-filter').prop('disabled',false); 
				$('.aromaticProfile-filter').prop('disabled',false);

				//hide filter reset
				$('.v_filters').removeClass('v_hasFilters');

				//activate all capsule
				$('.v-capsules').addClass('v-capsules-active');
				$('.v-capsules').removeClass('v_shadowed');
				$('.v-capsules').removeClass('v-capsules-inactive');
			}

		}

	});

	$('#ranges').on('change', function () {
		var filter_capsule_value = $(this).val();
		intensity = [];
		cupsize = [];
		aromatic_profile = [];
		if (filter_capsule_value != 'default') {

			$('.v_filters').addClass('v_hasFilters');
			$(this).addClass('v_selected')

			//with main filter
			if ($('.v_filter_range_active').length > 0) {

				// disable_enabled_filter(intensity, cupsize, aromatic_profile);
				$.each($('.v_filter_range_active'), function(index, value) {
					//activate capsule base on selected filter
					$('.v-intensity-'+value.dataset.filter).addClass('v-capsules-active');
					$('.v-intensity-'+value.dataset.filter).removeClass('v_shadowed');
					$('.v-intensity-'+value.dataset.filter).removeClass('v-capsules-inactive');

				});

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					if(filter_capsule_value != value.dataset.category) {
						value.classList.add('v_shadowed');
						value.classList.add('v-capsules-inactive');
						value.classList.remove('v-capsules-active');
					}
				});

				//enable other cupsize/aromatic button filter
				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					
					if(filter_capsule_value == value.dataset.category) {

						intensity = merge_array(intensity, value.dataset.intensity.split(','));
						cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
						aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
					}

				});
				
				disable_enabled_filter(intensity, cupsize, aromatic_profile);

			} else { 
				//deactive all capsule first 
				$('.v-capsules').removeClass('v-capsules-active');
				$('.v-capsules').addClass('v_shadowed');
				$('.v-capsules').addClass('v-capsules-inactive');

				//activate only the selected 
				$.each($('.v-capsules.v-capsules-inactive'), function(index, value) {

					if(filter_capsule_value == value.dataset.category) {
						value.classList.remove('v_shadowed');
						value.classList.remove('v-capsules-inactive');
						value.classList.add('v-capsules-active');
					}
				});

				$.each($('.v-capsules.v-capsules-active'), function(index, value) {
					intensity = merge_array(intensity, value.dataset.intensity.split(','));
					cupsize = merge_array(cupsize, value.dataset.cupsize.split(','));
					aromatic_profile = merge_array(aromatic_profile, value.dataset.aromaticprofile.split(','));
				});

				disable_enabled_filter(intensity, cupsize, aromatic_profile);
			}

		} else {
			$(this).removeClass('v_selected')
			remove_item(filter_range, filter_capsule_value);

			if ($('.v_filter_range_active').length > 0) {

				$.each($('.v_filter_range_active'), function(index, value) {
					//activate capsule base on selected filter
					$('.v-intensity-'+value.dataset.filter).addClass('v-capsules-active');
					$('.v-intensity-'+value.dataset.filter).removeClass('v_shadowed');
					$('.v-intensity-'+value.dataset.filter).removeClass('v-capsules-inactive');

					//get all possible active filter
					$.each($('.v-intensity-'+value.dataset.filter), function (index, filter_value) {
						cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
					});

					disable_enabled_filter(intensity, cupsize, aromatic_profile);
				});

			} else {
				//activate all capsule
				$('.v-capsules').addClass('v-capsules-active');
				$('.v-capsules').removeClass('v_shadowed');
				$('.v-capsules').removeClass('v-capsules-inactive');
				reset_filter();
			}
		}
	});

	$('.v_aromatic_profile_filter').on('click', function () {
		//get selected category
		var selected_filter_category = $(this).data('filter');

		//reset the filters
		$('.intensity-filter').attr('checked', false);
		$('.cupSize-filter').attr('checked', false);
		$('.aromaticProfile-filter').attr('checked', false);

		if($(this).attr("aria-selected") == 'false') {

			$('.v_filters').addClass('v_hasFilters');
			
			//first 
			if($('.v_aromatic_profile_filter.v_filter_range_active').length == 0) {
				
				$('.v_aromatic_profile_filter').removeClass('v_active v_filter_range_active');
				$('.v_aromatic_profile_filter').attr("aria-selected", false);

				$('.v_aromatic_notes_filter').removeClass('v_active');
			}

			//activate
			$(this).attr("aria-selected", true);
			$(this).addClass('v_active v_filter_range_active');

			$('.v-aromatic-profile-sub-filter-'+selected_filter_category).addClass('v_active');

			//capsules
			//de-activate all capsule first
			$('.v-capsules').removeClass('v-capsules-active');
			$('.v-capsules').addClass('v_shadowed');
			$('.v-capsules').addClass('v-capsules-inactive');

			//activate all selected filter
			
			$.each($('.v_aromatic_profile_filter.v_filter_range_active'), function(index, value) {
				
				//activate capsule base on selected filter
				$('.v-aromatic-profile-'+value.dataset.filter).addClass('v-capsules-active');
				$('.v-aromatic-profile-'+value.dataset.filter).removeClass('v_shadowed');
				$('.v-aromatic-profile-'+value.dataset.filter).removeClass('v-capsules-inactive');

				//get all possible active filter
				
				$.each($('.v-aromatic-profile-'+value.dataset.filter), function (index, filter_value) {
					
					cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
					intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
					aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
					ranges = merge_array(ranges, filter_value.dataset.category.split(','));
				});
				
				disable_ranges(ranges);
				disable_enabled_filter(intensity, cupsize, aromatic_profile);
			});
			
		} else {
			cupsize = [];
			intensity = [];
			aromatic_profile = [];
			ranges = [];
			//remove active
			$(this).removeClass('v_active v_filter_range_active');
			$(this).attr("aria-selected", false);

			$('.v-aromatic-profile-sub-filter-'+selected_filter_category).removeClass('v_active');
			$('.v-aromatic-profile-sub-filter-'+selected_filter_category).attr("aria-selected", false);

			//remove reset
			if($('.v_aromatic_profile_filter.v_filter_range_active').length == 0) {

				//reset the filters
				$('.intensity-filter').attr('checked', false);
				$('.cupSize-filter').attr('checked', false);
				$('#ranges').val('default');
				$('#ranges').removeClass('v_selected');
				$("#ranges option").each(function(index, option_value) {
					option_value.disabled = false;
				});

				//remove disabled
				$('.intensity-filter').prop('disabled',false); 
				$('.cupSize-filter').prop('disabled',false); 

				//hide filter reset
				$('.v_filters').removeClass('v_hasFilters');

				$('.v_aromatic_profile_filter').addClass('v_active');
				$('.v_aromatic_profile_filter').attr("aria-selected", false);

				$('.v_aromatic_notes_filter').addClass('v_active');

				$('.v-capsules').addClass('v-capsules-active');
				$('.v-capsules').removeClass('v_shadowed');
				$('.v-capsules').removeClass('v-capsules-inactive');

				reset_filter();

			} else {

				$('.v-aromatic-profile-'+selected_filter_category).removeClass('v-capsules-active');
				$('.v-aromatic-profile-'+selected_filter_category).addClass('v_shadowed');
				$('.v-aromatic-profile-'+selected_filter_category).addClass('v-capsules-inactive');

				$.each($('.v_aromatic_profile_filter.v_filter_range_active'), function(index, value) {

					//get all possible active filter
					
					$.each($('.v-aromatic-profile-'+value.dataset.filter), function (index, filter_value) {
						
						cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
						intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
						aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
						ranges = merge_array(ranges, filter_value.dataset.category.split(','));
					});
					
					disable_ranges(ranges);
					disable_enabled_filter(intensity, cupsize, aromatic_profile);
				});

			}
		}
	});

	// Filter by Aromatic notes
	$('.v_aromatic_notes_filter').on('click', function () {
		var selected_filter_category = $(this).data('filter');
		
		if($(this).attr("aria-selected") == 'false') {

			if($('.v_aromatic_profile_filter.v_filter_range_active').length == 0) {
				
				
				if($('.v_aromatic_notes_filter.v_filter_range_active').length == 0) {
					
					//de-activate all capsule first
					$('.v-capsules').removeClass('v-capsules-active');
					$('.v-capsules').addClass('v_shadowed');
					$('.v-capsules').addClass('v-capsules-inactive');

					$('.v_aromatic_notes_filter').removeClass('v_active v_filter_range_active');
					$('.v_aromatic_notes_filter').attr("aria-selected", false);

					$(this).addClass('v_active v_filter_range_active');
					$(this).attr("aria-selected", true);

					$('.v-aromatic-notes-'+selected_filter_category).addClass('v-capsules-active');
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v_shadowed');
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v-capsules-inactive');

				} else {
					
					$(this).addClass('v_active v_filter_range_active');
					$(this).attr("aria-selected", true);

					$('.v-aromatic-notes-'+selected_filter_category).addClass('v-capsules-active');
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v_shadowed');
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v-capsules-inactive');
				}

				$.each($('.v-aromatic-notes-'+selected_filter_category), function (index, filter_value) {
					cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
					intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
					aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
					ranges = merge_array(ranges, filter_value.dataset.category.split(','));
				});

				disable_ranges(ranges);
				disable_enabled_filter(intensity, cupsize, aromatic_profile);

			} else {
				
				if($('.v_aromatic_notes_filter.v_filter_range_active').length == 0) {
					
					$('.v_aromatic_notes_filter').removeClass('v_active v_filter_range_active');
					$('.v_aromatic_notes_filter').attr("aria-selected", false);
					$(this).addClass('v_active v_filter_range_active');
					$(this).attr("aria-selected", true);

					$('.v-capsules').removeClass('v-capsules-active');
					$('.v-capsules').addClass('v_shadowed');
					$('.v-capsules').addClass('v-capsules-inactive');

					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v_shadowed');
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v-capsules-inactive');
					$('.v-aromatic-notes-'+selected_filter_category).addClass('v-capsules-active');

				} else {
					
					$(this).addClass('v_active v_filter_range_active');
					$(this).attr("aria-selected", false);
					
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v_shadowed');
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v-capsules-inactive');
					$('.v-aromatic-notes-'+selected_filter_category).addClass('v-capsules-active');
				}
			}

		} else {

			cupsize = [];
			intensity = [];
			aromatic_profile = [];
			ranges = [];
			
			//remove reset
			$(this).removeClass('v_active v_filter_range_active');
			$(this).attr("aria-selected", false);

			if($('.v_aromatic_profile_filter.v_filter_range_active').length == 0) {
				
				if($('.v_aromatic_notes_filter.v_filter_range_active').length == 0) {
					
					$('.v_aromatic_notes_filter').addClass('v_active');
					$('.v_aromatic_notes_filter').attr("aria-selected", false);

					$('.v-capsules').addClass('v-capsules-active');
					$('.v-capsules').removeClass('v_shadowed');
					$('.v-capsules').removeClass('v-capsules-inactive');

					reset_filter();

				} else {
					
					$(this).removeClass('v_active v_filter_range_active');
					$(this).attr("aria-selected", false);

					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v-capsules-active');
					$('.v-aromatic-notes-'+selected_filter_category).addClass('v_shadowed');
					$('.v-aromatic-notes-'+selected_filter_category).addClass('v-capsules-inactive');

					$.each($('.v_aromatic_notes_filter.v_filter_range_active'),function (index, value) {
						$.each($('.v-aromatic-notes-'+value.dataset.filter), function (index, filter_value) {
							
							cupsize = merge_array(cupsize, filter_value.dataset.cupsize.split(','));
							intensity = merge_array(intensity, filter_value.dataset.intensity.split(','));
							aromatic_profile = merge_array(aromatic_profile, filter_value.dataset.aromaticprofile.split(','));
							ranges = merge_array(ranges, filter_value.dataset.category.split(','));
						});
						
						disable_ranges(ranges);
						disable_enabled_filter(intensity, cupsize, aromatic_profile);
					});
				}
			} else {
				
				if($('.v_aromatic_notes_filter.v_filter_range_active').length == 0) {
					
					$('.v-capsules').removeClass('v-capsules-active');
					$('.v-capsules').addClass('v_shadowed');
					$('.v-capsules').addClass('v-capsules-inactive');

					$.each($('.v_aromatic_profile_filter.v_filter_range_active'), function(index, value) {

						$('.v-aromatic-profile-sub-filter-'+value.dataset.filter).addClass('v_active');

						$('.v-aromatic-profile-'+value.dataset.filter).addClass('v-capsules-active');
						$('.v-aromatic-profile-'+value.dataset.filter).removeClass('v_shadowed');
						$('.v-aromatic-profile-'+value.dataset.filter).removeClass('v-capsules-inactive');
					});


				} else {
					
					$('.v-aromatic-notes-'+selected_filter_category).removeClass('v-capsules-active');
					$('.v-aromatic-notes-'+selected_filter_category).addClass('v_shadowed');
					$('.v-aromatic-notes-'+selected_filter_category).addClass('v-capsules-inactive');
				}
			}
		}
	});

	$('.capsules_description').on('click', function(event) {
		var capsule_active = $(this).parent().hasClass('v-capsules-active');
		if (!capsule_active)
			return;

		var capsules_description_value = $(this).data('capsule');
		if($(window).width() > '740') {
			
			if($(this).attr("aria-expanded") == 'false') {
				$('.capsules_description').attr("aria-expanded", false);
				$(this).attr("aria-expanded", true);
				$('.capsule-description').addClass('hide');
				$('.capsule-description-'+capsules_description_value).removeClass('hide');

				$(this).parent().css('margin-bottom', '600px');

			} else {
				$(this).parent().css('margin-bottom', '0px');
			}

			var target = $(this.getAttribute('href'));
		    target.slideDown();
		    if( target.length ) {
		        event.preventDefault();
		        $('html, body').stop().animate({
		            scrollTop: target.offset().top - 300
		        }, 500);
		    }
		}

		$('.v_coffeeDescription_'+capsules_description_value).css('opacity', '1');
		$('.v_coffeeDescription_'+capsules_description_value).css('visibility', 'visible');
		$('.v_coffeeDescription_'+capsules_description_value).css('transform', 'none');
		$('.v_coffeeDescription_'+capsules_description_value).css('transition', '.5s');

		$('.capsule-description-'+capsules_description_value).removeClass('hide');
		$('.capsule-description-'+capsules_description_value).css('pointer-events', 'auto');
		$('.capsule-description-'+capsules_description_value).css('opacity', '1');
		$('.capsule-description-'+capsules_description_value).css('visibility', 'visible');
		$('.capsule-description-'+capsules_description_value).css('transform', 'scale(1)');
		$('.capsule-description-'+capsules_description_value).css('transition', '.25s .5s');

		$('.m_circle_'+capsules_description_value).css('transform', 'scale(2)');
		$('.m_circle_'+capsules_description_value).css('transition', 'transform .5s .25s');

	});

	$('.close-modal').on('click', function () {
		var capsules_description_value = $(this).data('capsule');
		$('.v_coffeeDescription_'+capsules_description_value).css('opacity', '0');
		$('.v_coffeeDescription_'+capsules_description_value).css('visibility', 'collapse');
		$('.v_coffeeDescription_'+capsules_description_value).css('transform', 'unset');
		$('.v_coffeeDescription_'+capsules_description_value).css('transition', 'unset');

		$('.capsule-description-'+capsules_description_value).addClass('hide');
		$('.capsule-description-'+capsules_description_value).css('pointer-events', 'unset');
		$('.capsule-description-'+capsules_description_value).css('opacity', 'unset');
		$('.capsule-description-'+capsules_description_value).css('visibility', 'unset');
		$('.capsule-description-'+capsules_description_value).css('transform', 'unset');
		$('.capsule-description-'+capsules_description_value).css('transition', 'unset');

		$('.m_circle_'+capsules_description_value).css('transform', 'unset');
		$('.m_circle_'+capsules_description_value).css('transition', 'unset');
	});

	$('.v_btnCloseFixed').on('click', function () {
		$('.capsule-description').addClass('hide');
		$('.capsules_description').attr("aria-expanded", false);
		$('.v-capsules').css('margin-bottom', '0px');
	});
	
    
});

function merge_array(array1, array2) {
    var result_array = [];
    var arr = array1.concat(array2);
    var len = arr.length;
    var assoc = {};

    while(len--) {
        var item = arr[len];

        if(!assoc[item]) 
        { 
            result_array.unshift(item);
            assoc[item] = true;
        }
    }

    return result_array;
}

function remove_item(array1, item1) {
	var index = array1.indexOf(item1);
	if (index > -1) {
	    return array1.splice(index, 1);
	}
}

function disable_enabled_filter(intensity, cupsize, aromatic_profile) {
	//intensity
	if ($.isArray(intensity)) {
		if(intensity.length > 0) {
			$('.intensity-filter').prop('disabled',true); 
			$.each(intensity, function( index, value ) {
			  $('#intensity_'+value).prop('disabled',false); 
			  $('#intensity_'+value).removeClass('intensity_filter');
			});
		} else {
			$('.intensity-filter').prop('disabled',true); 
		}

	}
	
	//aromatic profile
	if ($.isArray(aromatic_profile)) {
		if(aromatic_profile.length > 0) {
			$('.aromaticProfile-filter').prop('disabled',true); 
			$.each(aromatic_profile, function( index, value ) {
			  $('#aromaticProfile_'+value).prop('disabled',false); 
			  $('#aromaticProfile_'+value).removeClass('aromaticProfile_filter');
			});
		} else {
			$('.aromaticProfile-filter').prop('disabled',true); 
		}
	}

	//cupsize
	if ($.isArray(cupsize)) {
		if(cupsize.length > 0) {
			$('.cupSize-filter').prop('disabled',true); 
			$.each(cupsize, function( index, value ) {
			  $('.cupSize_'+value).prop('disabled',false); 
			  $('.cupSize_'+value).removeClass('cupSize_filter');
			});
		} else {
			$('.cupSize_-filter').prop('disabled',true); 
		}
	}
}

function disable_ranges(ranges) {
	//disable ranges options
	$("#ranges option").each(function(index, option_value) {
		range_value = ranges.indexOf(option_value.value);
		if(range_value == -1) {
			if(option_value.value != 'default') {
				option_value.disabled = true;
			}
		} else {
			option_value.disabled = false;
		}
	});
}

function reset_filter() {
	$('.aromaticProfile-filter').prop('disabled',false); 
	$('.aromaticProfile-filter').removeClass('intensity_filter');

	$('.intensity-filter').prop('disabled',false); 
	$('.intensity-filter').removeClass('aromaticProfile_filter');

	$('.cupSize-filter').prop('disabled',false); 
	$('.cupSize-filter').removeClass('cupSize_filter');

	$('#ranges').val('default');
	$('#ranges').removeClass('v_selected');
	$("#ranges option").each(function(index, option_value) {
		option_value.disabled = false;
	});
}

function reset_all() {
	aromatic_profile = [];
	intensity = [];
	cupsize = [];
	ranges = []

	filter_intensity = [];
	filter_cupsize = [];
	filter_aromaticprofile = [];

	$('.v-capsules').addClass('v-capsules-active');
	$('.v-capsules').removeClass('v_shadowed');
	$('.v-capsules').removeClass('v-capsules-inactive');

	$('.v_range_filter').attr("aria-selected", false);
	$('.v_range_filter').removeClass('v_active v_filter_range_active');

	$('.aromaticProfile-filter').prop('disabled',false).attr('checked', false).addClass('aromaticProfile_filter');
	$('.intensity-filter').prop('disabled',false).attr('checked', false).addClass('intensity_filter');
	$('.cupSize-filter').prop('disabled',false).attr('checked', false).addClass('cupSize_filter');

	$('.v_intentsity_filter').attr("aria-selected", false);
	$('.v_intentsity_filter').removeClass('v_active v_filter_range_active');

	$('#ranges').val('default');
	$('#ranges').removeClass('v_selected');
	$("#ranges option").each(function(index, option_value) {
		option_value.disabled = false;
	});
}