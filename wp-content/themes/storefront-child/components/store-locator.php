<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

// Category Banner
$category_banner = get_nespresso_category_banner();

$storelocator_desktop_img = isset($category_banner['storelocator_category_banner_image_url']) && $category_banner['storelocator_category_banner_image_url'] ? $category_banner['storelocator_category_banner_image_url'] : get_stylesheet_directory_uri() . "/images/banner/September 25/Store Locator_Desktop.png";
$storelocator_mobile_img = isset($category_banner['storelocator_category_banner_image_url_mobile']) && $category_banner['storelocator_category_banner_image_url_mobile'] ? $category_banner['storelocator_category_banner_image_url_mobile'] : get_stylesheet_directory_uri() . "/images/banner/September 25/Store Locator_Mobile.png";
$storelocator_link = isset($category_banner['storelocator_category_banner_link']) && $category_banner['storelocator_category_banner_link'] ? $category_banner['storelocator_category_banner_link'] : "/store-locator/";
?>

<div class="slocator">
    <!-- banner -->
    <div class="new-banner">
        <!-- web -->
        <a class="web-banner-img" href="<?php echo $storelocator_link; ?>"><img src="<?php echo $storelocator_desktop_img; ?>" draggable="false"/></a>
        <!-- mobile -->
        <a class="mobile-banner-img" href="<?php echo $storelocator_link; ?>"><img src="<?php echo $storelocator_mobile_img; ?>" draggable="false"/> </a>
    </div>
    <!-- end banner -->
    <div class="slocator-main clearfix">
        <div class="container">
        <div class="slocator-menu">
            <div class="row no-padding no-margin">
                <!-- <?php include 'store-locator/search-bar.php';?> -->
                <?php include 'store-locator/nav.php';?>
            </div>
        </div>

        <div class="slocator-results">
            <div class="row no-margin no-padding">
                <div class="slocator-list col-md-4 no-margin no-padding">
                    <?php include 'store-locator/filter.php';?>
                    <?php include 'store-locator/nav-mobile.php';?>
                    <ul class="slocator-locations" id="slocator-locations">
                        <?php include 'store-locator/list-of-stores.php';?>
                    </ul>
                </div>
                <div class="slocator-map col-md-8 no-margin no-padding" id="slocator-container-map">
                    <?php include 'store-locator/map.php';?>
                    <?php //include('store-locator/footer-details.php'); ;?>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var $ = jQuery;
    $(document).on('click', '#tabs-nav > ul > li > a', function() {
        var href = $(this).attr('class');
        $('.tab').hide();
        $('#tabs-nav a').removeClass('active');
        $(this).addClass('active');
        $('#' + href).show();
    });
    $('.mobile-store-locator-tab').on('click', function() {
        var tab = $(this).data('tab');
        $('.tab').hide();
        $('#tabs-nav a').removeClass('active');
        $(this).addClass('active');
        $('#' + tab).show();
    });
</script>
<?php include 'store-locator/retailers_map.php';?>
<?php
global $scripts;
// $scripts[] = 'js/libs/google-map-api.js';
$scripts[] = 'js/libs/jquery.nicescroll.min.js';
// $scripts[] = 'js/components/store-locator.js';
$scripts[] = 'js/components/ajax-store-locator.js';
$scripts[] = 'js/components/store-locator-map-multiple.js';

?>
