<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>
<!-- <div data-step="4" class="checkister step4 hide"> -->
<div class="hide">
    <header>
        <h2>WELCOME</h2>
    </header>
    <section>
        <div class="main-content clearfix">
            <form method="POST" action="#">
                <section class="form">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/register-step4.jpg" alt="" class="img-freeHtml">
                    <div class="checkister-wrapper clearfix">
                        <button type="button" class="btn btn-primary btn-icon-right pull-left btn-previous-step"
                            href="javascript:void(0)"
                            title="Previous"
                            data-step="4"
                        ><i class="icon icon-arrow_right"></i>Previous</button>
                        <button type="button" class="btn btn-primary btn-icon-right pull-right btn-next-step"
                            href="javascript:void(0)"
                            title="Continue"
                            data-step="4"
                        ><i class="icon icon-arrow_left"></i>Continue</button>
                    </div>
                </section>
            </form>
        </div>
    </section>
</div>
