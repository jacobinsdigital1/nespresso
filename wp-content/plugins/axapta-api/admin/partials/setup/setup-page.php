<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com
 * @since      1.0.0
 *
 * @package    Axapta_Api
 * @subpackage Axapta_Api/admin/partials
 */

global $wp;
$current_url = home_url(add_query_arg(null, null));
$axapta_settings = get_axapta_settings();

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
    <h1><?php echo esc_html(get_admin_page_title()); ?></h1>
    <?php if ( isset($_SESSION['axapta_alert']) && isset($_SESSION['axapta_alert']['message'])) : ?>
        <div class="alert alert-<?= $_SESSION['axapta_alert']['type']; ?> alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?= $_SESSION['axapta_alert']['header']; ?></strong> <?= $_SESSION['axapta_alert']['message']; ?>
        </div>
    <?php unset($_SESSION['axapta_alert']); endif; ?>
</div>
<div class="wrap pd-top-20 pd-bottom-20" id="axapta-setting">

    <h3>Axapta Setting</h3>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        role="form"
        id="form-axapta-setting"
    >

        <input type="hidden" name="action" value="axapta_settings">

        <input type="hidden" name="type" value="update-or-create" id="type">

        <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

        <div class="form-group">
            <label for="uri">API URI <span class="required">*</span></label>
            <input type="text" name="uri" value="<?php echo @$axapta_settings['uri'] ?>" placeholder="" class="form-control" id="uri" required>
        </div>
<div class="form-group">
            <label for="uri">Port<span class="required">*</span></label>
            <input type="text" name="port" value="<?php echo @$axapta_settings['port'] ?>" placeholder="" class="form-control" id="port" required>
        </div>
        <div class="form-group">
            <label for="consumer_key">Consumer Key<span class="required">*</span></label>
            <input type="text" name="consumer_key" value="<?php echo @$axapta_settings['consumer_key'] ?>" placeholder="" class="form-control" id="consumer_key" required>
        </div>

        <div class="form-group">
            <label for="password">Consumer Secret<span class="required">*</span></label>
            <input type="consumer_secret" name="consumer_secret" value="<?php echo @$axapta_settings['consumer_secret'] ?>" placeholder="" class="form-control" id="consumer_secret" required>
        </div>

        <!-- <div class="form-group">
            <label for="username">User ID <span class="required">*</span></label>
            <input type="text" name="userid" value="<?php echo @$axapta_settings['userid'] ?>" placeholder="duy.dt@annam-group.com" class="form-control" id="userid" required>
        </div>

        <div class="form-group">
            <label for="password">Store ID <span class="required">*</span></label>
            <input type="storeid" name="storeid" value="<?php echo @$axapta_settings['storeid'] ?>" placeholder="WMAYW" class="form-control" id="storeid" required>
        </div>

        <div class="form-group">
            <label for="pos_username">Signature <span class="required">*</span></label>
            <input type="text" name="signature" value="<?php echo @$axapta_settings['signature'] ?>" placeholder="ap6s3kl8bnl90nc61s6el1d1141qf688nmh39r7d6lj8b" class="form-control" id="signature" required>
        </div> -->

        <br>

        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-flat pull-right">Submit</button>
        </div>

    </form>

</div>
