
/**
 * Product list
 */
$(document).ready(function() {

    $noUiSlider = null;

    // swtichers
    var switchers = $('.productlist-switcher a, .tab-switcher a'),
        $productcategories = $(".productlist-panel .product-category"),
        $productline = $(".productlist-panel .product");

    $(".filters-reset").on("click", function(e) {
        $('.filters').find('form').trigger('reset');
        $productline.show();
        $('.productlist-content').find('.product').css('display', 'list-item');
        $('.productlist-content').find('.product-category').show();

        // the count of products after filter is applied

        $('.products-count').find('strong').html( $('li.product:visible').length );

        if ( $noUiSlider ) {
            $noUiSlider.set([0, 10000000]);
        }
    });

    switchers.on('click', function(e) {
        switchers
            .removeClass('active')
            .parent().removeClass('active');

        $(this)
            .addClass('active')
            .parent().addClass('active');

        var target = $(this).attr('href');

        if(target == '#mp-description') {
            $('#r2').parent().addClass('hover-panel');
        } else {
            if( $(window).width() > 768) {
                $('#r2').parent().removeClass('hover-panel');
            }
        }

        $('.productlist-content, .productdetail-panel')
            .removeClass('active')
            .filter(target).addClass('active');

        // Uncheck filters
        // $(".filters-reset").trigger("click");

        e.preventDefault();
        return false;
    });

    $('.product__tabs__item a').on('click', function (e) {
        e.preventDefault();
	    $('.product__tabs__item a').removeClass('active');
	    $('.product__tabs__content > div').removeClass('active');
	    $(this).addClass('active');
	    $($(this).attr('href')).addClass('active');
    });

    $('.product__slider__dots a').on('click', function (e) {
        e.preventDefault();
	    $('.product__slider__dots a').removeClass('active');
	    $(this).addClass('active');
	    if($(this).hasClass('product__slider--dark')) {
            $('.product__header').removeClass('product__header--light');
        } else {
		    $('.product__header').addClass('product__header--light');
        }
	    $('.product__slider__list > li').removeClass('active').eq($(this).parent().index()).addClass('active');
    });

	$(".product__slider__list li").swipe( {
		swipe:function(event, direction) {

		    if ( Modernizr.touch ){
                if (direction === "left" || direction === "right") {

                    var nbItem = $(".product__slider__list li").length;
                    var currentIndex = $(this).index();

                    switch (direction) {
                        case "left":
                            currentIndex = currentIndex < nbItem - 1 ? currentIndex + 1 : 0;
                            break;
                        case "right":
                            currentIndex = currentIndex > 0 ? currentIndex - 1 : nbItem - 1;
                            break;
                    }

                    $('.product__slider__dots a').removeClass('active').eq(currentIndex).addClass('active');
                    $(".product__slider__list li").removeClass('active').eq(currentIndex).addClass('active');

                    if($(".product__slider__list li").eq(currentIndex).hasClass('product__slider--dark')) {
	                    $('.product__header').removeClass('product__header--light');
                    } else {
	                    $('.product__header').addClass('product__header--light');
                    }

                }
			}
		}
	});

    /**
     * Filter form object
     */
    $('.filter input:checkbox').removeAttr('checked');
    $('.filter input:radio').removeAttr('checked');

    $('.filters').each(function() {
        var $this = $(this);

        // filter object
        var filter = {

            /** Props */
            container: $this,
            toggler: $this.siblings('.filters-toggler'),
            form: $this.find('form'),
            ranges: $this.find('form .range'),

            /** Init object + fields  */
            init: function() {
                filter.toggler.on('click', filter.toggle);
                filter.form.on('click', '.reset', filter.reset);
                filter.form.on('click', 'input', filter.refresh);

                if($('.price-range').length){

                    var rangeFormat = wNumb({
                        thousand: ',',
                    });

                    $noUiSlider = noUiSlider.create(filter.ranges[0], {
                        start: [0, 10000000],
                        step: 50000,
                        margin: 0,
                        connect: true,
                        range: {
                            'min': 0,
                            'max': 10000000,
                        },
	                    format: wNumb({
		                    decimals: 0,
                            thousand: ',',
	                    }),
                        pips: {
                            mode: 'steps',
                            density: 100
                        },
                        tooltips: true
                    });

                    /**
                     * when novislider changes range
                     */
                    $noUiSlider.on('update', function(values, handle) {
                        filter.refresh;

                        $.each($('[data-price]'), function(i,obj) {

                            var self = $(obj);

                            var productDataPrice = self.attr('data-price');

                            self.css('display', 'list-item');

                            var value_0 = parseInt(values[0].replace(/,/g, ''));
                            var value_1 = parseInt(values[1].replace(/,/g, ''));
                            var productDataPrice = parseInt(productDataPrice.replace('.00', ''));
                            if ( productDataPrice < value_0 || productDataPrice > value_1 ) {
                                self.hide();
                            }
                        })
                    });
                }
            },

            /** Toggle filter block */
            toggle: function(){
                filter.toggler.toggleClass('open');
                setTimeout(function () {
	                filter.container.slideToggle(400);
                }, 200)
            },

            /** Reset form + custom fields  */
            reset: function(e){
                filter.form.trigger('reset');
                // filter.ranges.rangeslider('update', true);
                $productline.show();
                e.preventDefault();
                return false;
            },


            /** Refresh list */
            refresh: function(){

                var $results = $productline,
                    $filter_inputs = filter.form.find("input");

                if($(this).attr('type') == 'checkbox') {
                    $(this).parent().toggleClass('active');
                } else {
                    $('.filter label').removeClass('active');
                    $(this).parent().addClass('active');
                }

                $productline.show();
                $productcategories.show();

                $filtered_results = [];

                $.each($filter_inputs, function(i,e){

                    var $tar = $(e),
                        value = $tar.val(),
                        name = $tar.attr('name');

                    if( $tar.is(":checked") ){

                        $.each($productline, function(i, v){

                            if ( $(v).data(name) == value ) {
                                $filtered_results.push($(v));
                                return true;
                            }
                            var v_name = $(v).data(name).toString();
                            var split_data = v_name.split(',');
                            $.each( split_data, function(i1, v1) {
                                var self = $(v);
                                self.hide();
                                self.parents('.product-category').hide();
                                if ( v1 == value )
                                    $filtered_results.push($(v));
                            });

                        });
                    }
                }); // each()


                if( $filtered_results.length > 0 ){
                    $productcategories.hide();
                    $productline.hide();

                    $.each( $filtered_results, function(i,v) {
                        var self = $(v);
                        self.show();
                        self.parents('.product-category').show();
                    });

                } // endif

                // the count of products after filter is applied
                $('.products-count').find('strong').html( $filtered_results.length );

            } // refresh()
        };

        filter.init();
    });

});
