<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * Template Name: Nespresso Services
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
?>

<?php get_header(); ?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/services.css">

<section id="services-intro" class="main">
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <h1>
                What else can Nespresso do <span class="mark">for you?</span>
            </h1>
            <!-- <h2>
                <span class="mark">Join the Club Nespresso</span> and benefit from our range of personalised services, designed just for you.
            </h2> -->
        </div>
        <div class="row">
            <div class="col-sm-4">
                <article>
                    <a href="#order" class="on-scroll">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/ico-order.png" alt="">
                        <strong>Order</strong>
                        <span class="more">More</span>
                    </a>
                </article>
            </div>
            <div class="col-sm-4">
                <article>
                    <a href="#delivery" class="on-scroll">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/ico-delivery.png" alt="">
                        <strong>Delivery</strong>
                        <span class="more">More</span>
                    </a>
                </article>
            </div>
            <div class="col-sm-4">
                <article>
                    <a href="#customer-care" class="on-scroll">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/ico-customer-care.png" alt="">
                        <strong>Customer Care</strong>
                        <span class="more">More</span>
                    </a>
                </article>
            </div>
        </div>
        <a href="#order" class="scroll-down on-scroll">Scroll Down</a>
    </div>
</section>
<section id="order" class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/img-order.png" alt="" class="img-responsive">
            </div>
            <div class="col-md-5 col-sm-6">
                <h1>Orders</h1>
                <p>
                    Make your choice from our Grand Crus and our extensive range of coffee machines. We also offer different accessories to make your Nespresso moments truly unforgettable.
                </p>
                <p>
                    Coffee Capsules and machines are available on the website. You can order using your desktop, tablet, or any mobile device.
                </p>
                <p>
                    If you need assistance in ordering, please call our Coffee Specialists any time between
                </p>
                <p>
                    Monday to Saturdays, from 10am to 6pm.
                    <br>
                    Ho Chi Minh City : 1900 633 474
                    <br>
                    <!-- Provincial Toll-Free: 1-800-10-4777870 -->
                </p>
                <p>
                    Learn more about our Conditions of Sale <a href="/conditions-of-sale" class="link">here</a>.
                </p>
            </div>
        </div>
    </div>
</section>
<section id="delivery" class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-push-6">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/img-delivery.png" alt="">
            </div>
            <div class="col-md-5 col-md-pull-5 col-sm-6 col-sm-pull-6">
                <h1>Delivery</h1>
                <p>
                    Nespresso makes every possible effort to ensure that orders made via www.nespresso.vn are delivered in 1 – 2 working days for Ho Chi Minh City, 2 – 5 working days for other area.
                </p>
                <p>
                     <em>Free delivery </em> is available for a minimum <b><?= get_woocommerce_currency_symbol() ?> 1,500,000</b> spent.
                </p>
                <p>
                    Learn more about our Conditions of Sale <a href="/conditions-of-sale" class="link">here</a>.
                </p>
            </div>
        </div>
    </div>
</section>
<section id="customer-care" class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/img-customer-care.png" alt="">
            </div>
            <div class="col-md-5 col-sm-6">
                <h1>Customer Care</h1>
                <p>
                    Do you need any assistance for your machine?
                </p>
                <p>
                    Our Call Center is open to customers who need answers from selecting your Grand Crus, to immediate technical diagnosis, and other solutions that meet your needs.
                </p>
                <p>
                    Speak to our Coffee Specialists any time between Monday to Saturday, from 10am to 6pm.
                    <br>
                    Ho Chi Minh City: 1900 633 474
                    <br>
                </p>
                <p>
                    Not a Nespresso member in the Vietnam? <a href="<?php echo site_url(); ?>/register" class="link">Register now!</a>
                </p>
            </div>
        </div>
        <!--div class="row">
            <div class="row">
                <div class="col-sm-4">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/ico-machine-care.png" alt="">
                            <div class="divider"></div>
                            <strong>Machine Care</strong>
                            <p>Get immediate help at any time.</p>
                            <span class="more">Read More</span>
                        </a>
                    </article>
                </div>
                <div class="col-sm-4">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/ico-assistance.png" alt="">
                            <div class="divider"></div>
                            <strong>Assistance everyday from 9.00 a.m. to 7.00 p.m.</strong>
                            <p>
                                Enjoy privileged relationships with <em>Nespresso</em>
                            </p>
                            <span class="more">Read More</span>
                        </a>
                    </article>
                </div>
                <div class="col-sm-4">
                    <article>
                        <a href="#">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/services/services/ico-expertise.png" alt="">
                            <div class="divider"></div>
                            <strong>Expertise</strong>
                            <p>
                                Discover the Art of Degustation.
                            </p>
                            <span class="more">Read More</span>
                        </a>
                    </article>
                </div>
            </div>
        </div-->
    </div>
</section>
<section id="join-club" class="main">
    <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <h2>
                <span class="mark">Join Nespresso</span> now and benefit from our range of services.
            </h2>
            <a href="<?php bloginfo('url'); ?>/login" class="btn btn-primary">Sign In</a>
        </div>
    </div>
</section>

<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/libs/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/components/services.js"></script>

<?php get_footer(); ?>
