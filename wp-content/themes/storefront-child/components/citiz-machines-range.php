<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$products = get_machine_products();
$arrays = get_nespresso_product_list();
$machines = [];
$machine_product_2 = 10603;
$machine_product = 8913;

foreach ($products as $value) {
	if($machine_product == $value->post_id) {
		$machine_product = $value;
	}
	if($machine_product_2 == $value->post_id) {
		$machine_product_2 = $value;
	}
    $machines[] = $value->name;
}

if ($arrays) {
	$machines = $arrays['machine'];
}

global $scripts;
$scripts[] = 'js/components/machine-discover-more.js';

?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/machine-discover-more.css">

<main id="main" class="fullPage fullPage__plp coffee-list desktop">
	<div id="block-8809565345269" class="free-html" data-label="">
		<div class="vue vue_machines v_citiz">
			<section class="vue_introduction v_introduction v_sectionnull v_backgroundVideoPlaying" id="introduction" data-label="Introduction">
			    <div class="v_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/placeholder_XL.jpg');" lazy="loaded"> <iframe tabindex="-1" aria-hidden="true" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" src="https://www.youtube.com/embed/h2JXfaJvRE8?rel=0&fs=0&autoplay=1&playlist=amp3EhtxTs4&controls=0&loop=1&enablejsapi=1&origin=https%3A%2F%2Fwww.nespresso.com&widgetid=1" id="widget2" class="play gtm-video-start gtm-video-progress25 gtm-video-progress50 gtm-video-progress75 gtm-video-complete" data-gtm-yt-inspected-2212929_367="true" height="360" frameborder="0" width="640"></iframe> </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <header>
			                <div class="v_cell">
			                    <div class="v_menu">
			                        <p class="v_visually_hidden"></p>
			                        <ul>
			                            <li> <a href="/machine-list">All machines</a> </li>
			                            <li> <a href="#">Assistance</a> </li>
			                            <li> <a href="#">Specifications</a> </li>
			                        </ul>
			                    </div>
			                </div>
			            </header>
			            <article>
			                <div class="v_articleContent">
			                    <h2> <span><strong class="v_brand" term="citiz">New Citiz</strong></span> </h2>
			                    <div class="v_wysiwyg">
			                        <p>The <strong class="v_brand" term="nespresso">Nespresso</strong> coffee machine for coffee drinkers and design lovers.</p>
			                    </div>
			                    <div class="v_buttonContainer"> <a class="v_btnRoundM" tabindex="-1" aria-hidden="true" href="#visualVariations"> <i class="fn_angleDownCircle"></i> </a>  </div>
			                </div>
			            </article>
			        </div>
			    </div>
			    <div class="v_video">
			        <button type="button" name="button" class="v_btnRoundSM v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			        <div></div>
			    </div>
			</section>
			<section class="vue_visualVariations vue_productConfigurator v_sectionLight v_toggleVariation0 v_toggle_lattissimaTouch v_key_visualVariations" id="visualVariations" data-label="Visual Variations">
			    <h2 class="v_visually_hidden">Customize and Buy your <strong class="v_brand" term="pixie">pixie</strong></h2>
			    <p class="v_visually_hidden">Use the buttons below to expand front or side view</p>
			    <div class="v_visualVariationsSlider citiz-main-product citiz-variation-1" style="touch-action: pan-y; -moz-user-select: none;">
	                <article class="v_product">
	                    <div class="v_restrict">
			                <button style="box-shadow: none !important;" class="v_activeView"><span class="v_visually_hidden">Press ENTER or SPACE to zoom on front view</span><span class="v_visually_hidden">Front</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/silver_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/black_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/cherryred_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-3">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/white_front_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button><button style="box-shadow: none !important;"> <span class="v_visually_hidden">Press ENTER or SPACE to zoom on side view</span> <span class="v_visually_hidden">Side</span>
			                    <ul>
			                        <li class="variation-color-1 variation-1-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/silver_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/black_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-2">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/cherryred_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-1 variation-1-color-3">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/black_side_XL.png');" lazy="loaded"></div>
			                        </li>
			                    </ul>
			                </button>
			            </div>
	                </article>
	            </div>
			    <div class="v_visualVariationsSlider hide citiz-main-product citiz-variation-2" style="touch-action: pan-y; -moz-user-select: none;">
	                <article class="v_product">
	                    <div class="v_restrict">
	                        <button style="box-shadow: none !important;" class="v_activeView"><span class="v_visually_hidden">Press ENTER or SPACE to zoom on front view</span><span class="v_visually_hidden">Front</span>
	                            <ul>
								    <li class="variation-color-2 variation-2-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/silver_front_XL2.png');" lazy="loaded"></div>
			                        </li>
			                        <li class="variation-color-2 variation-2-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/cherryred_front_XL2.png');" lazy="loaded"></div>
			                        </li>
								</ul>
	                        </button><button style="box-shadow: none !important;"> <span class="v_visually_hidden">Press ENTER or SPACE to zoom on side view</span> <span class="v_visually_hidden">Side</span>
	                            <ul>
								    <li  class="variation-color-2 variation-2-color-0 v_activeColor">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/silver_side_XL2.png');" lazy="loaded"></div>
			                        </li>
			                        <li  class="variation-color-2 variation-2-color-1">
			                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/cherryred_side_XL2.png');" lazy="loaded"></div>
			                        </li>
								</ul>
	                        </button>
	                    </div>
	                </article>
	            </div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_productConfiguratorPosition">
			                <div class="v_productConfigurator">
			                    <p class="v_visually_hidden">Use the form below to choose your machine model, change color, and add to bag</p>
			                    <form>
			                        <div class="v_cell v_productToggle">
			                            <fieldset>
			                                <legend>Choose between CitiZ or CitiZ&amp;milk</legend>
			                                <div class="v_rail" style="touch-action: pan-y; -moz-user-select: none;"> <input name="productToggle" id="productToggle0" value="1" type="radio" checked=""><label class="v_label_productToggle0" for="productToggle0" style="background-image: none;">CitiZ</label><input name="productToggle" id="productToggle1" value="2" type="radio">
			                                <!-- <label class="v_label_productToggle1" for="productToggle1" style="background-image: none;">CitiZ&amp;milk</label>  -->
			                                </div>
			                            </fieldset>
			                        </div>
			                        <div class="v_cell v_toggleColor">
			                            <fieldset>
			                                <legend>Choose your color</legend>
			                                <div class="citiz-main-product-color citiz-main-product-color-1 v_bullets0">
			                                    <p aria-hidden="true">Colors</p>
			                                    <div class="v_inputs">
			                                        <input name="colorToggle0" id="radio_colorToggle0_0" value="0" type="radio"><label style="background-color: rgb(148, 148, 148);" for="radio_colorToggle0_0"> <b>Silver <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle0" id="radio_colorToggle0_1" value="1" type="radio"><label style="background-color: rgb(0, 0, 0);" for="radio_colorToggle0_1"> <b>Black <span class="v_visually_hidden">color</span></b> </label>
			                                        <!-- <input name="colorToggle0" id="radio_colorToggle0_2" value="2" type="radio"><label style="background-color: rgb(226, 0, 26);" for="radio_colorToggle0_2"> <b>Cherry Red <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle0" id="radio_colorToggle0_3" value="3" type="radio"><label style="background-color: rgb(255, 255, 255);" for="radio_colorToggle0_3"> <b>White <span class="v_visually_hidden">color</span></b> </label> -->
			                                        <div class="v_bullet_selected"><span></span></div>
			                                    </div>
			                                </div>
			                                <div class="citiz-main-product-color citiz-main-product-color-2 hide">
			                                    <p aria-hidden="true">Colors</p>
			                                    <div class="v_inputs">
			                                        <input name="colorToggle1" id="radio_colorToggle1_0" value="0" type="radio"><label style="background-color: rgb(148, 148, 148);" for="radio_colorToggle1_0"> <b>Silver <span class="v_visually_hidden">color</span></b> </label><input name="colorToggle1" id="radio_colorToggle1_1" value="1" type="radio"><label style="background-color: rgb(226, 0, 26);" for="radio_colorToggle1_1"> <b>Cherry Red <span class="v_visually_hidden">color</span></b> </label>
			                                        <div class="v_bullet_selected"><span></span></div>
			                                    </div>
			                                </div>
			                            </fieldset>
			                        </div>
			                        <div class="v_cell v_buyProduct">
				                        <fieldset>
				                            <legend>Review your product: D Model Lime Green color</legend>
				                            <div class="v_addToCart citiz-price citiz-price-1">
				                                <div class="v_priceAndButton">
				                                    <p class="v_productPrice"><?= wc_price(@$machine_product->price) ?></p>
				                                    <div class="v_addToCartCustom">
				                                        <button type="button" class="view_buy" data-link="<?= @$machine_product->url ?>" style="padding: 0.7em 1em .7em 1em;"><span class="v_label" aria-hidden="true">View Details & Buy</span> </button>
				                                    </div>
				                                </div>
				                            </div>
				                            <div class="v_addToCart citiz-price citiz-price-2 hide">
				                                <div class="v_priceAndButton">
				                                    <p class="v_productPrice"><?= wc_price(@$machine_product_2->price) ?></p>
				                                    <div class="v_addToCartCustom">
				                                        <button type="button" class="view_buy" data-link="<?= @$machine_product_2->url ?>" style="padding: 0.7em 1em .7em 1em;"><span class="v_label" aria-hidden="true">View Details & Buy</span> </button>
				                                    </div>
				                                </div>
				                            </div>
				                        </fieldset>
				                    </div>
			                    </form>
			                </div>
			            </div>
			        </div>
			</section>
			<section class="vue_benefits v_key_benefits showBenefits_citiz" id="benefits" data-label="benefits">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover the key benefits of the <strong class="v_brand" term="citiz">Citiz</strong> range</h2>
			            <div class="v_wysiwyg wow" data-wow="">
			                <p>Designed to appeal to both <strong class="v_brand" term="nespresso">Nespresso</strong> lovers and fans of modern design. <strong class="v_brand" term="citiz">Citiz</strong> is a fusion of the latest technology and inspiration from retro-modern design. With a complete range for preparing your favorite coffees, <strong class="v_brand" term="citiz">Citiz</strong> provides <strong class="v_brand" term="nespresso">Nespresso</strong> expertise in a fun and intuitive way.</p>
			            </div>
			            <ul class="v_row6">
			                <li data-wow="" class="v_icon_autoPoweroff wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/autoPoweroff.svg"></div>
			                    <div>
			                        <p>Automatic off mode</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_cupSize2 wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/cupSize2.svg"></div>
			                    <div>
			                        <p>Coffee size Espresso &amp; Lungo</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_25Seconds wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/25Seconds.svg"></div>
			                    <div>
			                        <p>Fast heat-up 25 seconds</p>
			                    </div>
			                </li><li data-wow="" class="v_icon_programmable2Buttons wow">
			                    <div><img alt="" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/programmable2Buttons.svg"></div>
			                    <div>
			                        <p>2 programmable buttons</p>
			                    </div>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>

			<section class="vue_video v_key_video" id="video" data-label="Video">
			    <h2 class="v_visually_hidden">Watch the <strong class="v_brand" term="citiz&amp;milk">Citiz&amp;Milk</strong> video</h2>
			    <div class="v_backgroundImage" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/video_XL.jpg');" lazy="loaded"></div>
			    <button type="button" name="button" class="v_btnRoundLG v_btnOpen video-modal" data-source="video" title="Press ENTER or SPACE to start the Maestria presentation"> <i class="fn_videoCircle"></i> <span tabindex="-1" aria-hidden="true">Watch the <strong class="v_brand" term="citiz&amp;milk">Citiz&amp;Milk</strong> <span class="v_visually_hidden">Play video</span> </button>

				<div id="video-modal-container" class="video-modal">
	  				<iframe class="video-modal-content" id="modal_video video_iframe"  allowfullscreen="1" allow="encrypted-media" title="YouTube video player" src="http://www.youtube.com/embed/h2JXfaJvRE8?enablejsapi=1&autoplay=0&rel=0"  data-gtm-yt-inspected-2212929_367="true" class="gtm-video-start gtm-video-complete gtm-video-progress25 gtm-video-progress50 gtm-video-progress75" height="360" frameborder="0" width="640"></iframe>
				</div>

			    <div class="v_videoContainer">
			        <button type="button" name="button" class="v_btnRoundMD v_btnClose" title="Press ESCAPE to exit the Maestria video"> <i class="fn_close"></i> <span>Close video</span> </button>
			    </div>
			</section>
			<section class="vue_carousel v_key_carousel v_slide4" id="carousel" data-label="Carousel">
			    <h2 class="v_visually_hidden">Gallery</h2>
			    <p class="v_visually_hidden">Use ENTER or SPACE key on thumbnails buttons to enter slider mode</p>
			    <div class="vue_imageGrid">
			        <div>
			        	<span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/1_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/1_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/1_XL.jpg');" lazy="loaded">
			        	<button tabindex="-1" aria-hidden="true"></button></span>
			        </div><div>
			            <div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/2_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/2_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/2_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> </span> </div><div> <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/3_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/3_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/3_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button> <img class="v_visually_hidden" alt="Image 3" src="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/3_XL.jpg" lazy="loaded"> </span> </div><div>
			            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/4_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/4_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/4_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div><div>
			            <span class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/5_XL.jpg');"></span> <span class="bg_full v_buttonContainerCover image-modal" data-img="/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/5_XL.jpg"  style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/5_XL.jpg');" lazy="loaded"> <button tabindex="-1" aria-hidden="true"></button></span> </div>
			        </div>
			    </div>
			    <div id="modal-container" class="image-modal">
	  				<img class="image-modal-content" id="modal_img" src="">
				</div>
			</section>
			<section class="vue_faq v_sectionLeft v_key_faq" id="faq" data-label="Faq">
			    <div class="bg_normal bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/faq_XL.jpg');" lazy="loaded"></div>
			    <div class="bg_normal bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/machine-range/citiz/faq_S.jpg');" lazy="loaded"></div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Frequently asked questions</h2>
			                <div class="vue_accordion">
			                    <ul role="accordion" class="v_isCollapsable">
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 1 of 5</span> How to take care of my machine? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <ol>
			                                    <li>Descale your machine at least once a year if you use soft water<br>Note: for hard water, descale twice a year. (If you don't know what type of water you have, check the water tank - hard water tends to leave white marks) <br><a href="/product/descaling-kit/" target="_blank">Order a descaling kit now</a></li>
			                                    <li>Eject your capsules after each use</li>
			                                    <li>Change the water tank regularly, and refill with fresh drinking water</li>
			                                    <li>Empty and clean the capsule container and drip tray on a regular basis</li>
			                                    <li>For integrated milk devices, activate the cleaning procedure after each use</li>
			                                    <li>Before your first coffee, brew water without a capsule by pressing any coffee button.</li>
			                                </ol>
			                                <p>When you do this, you pre-heat your coffee cup while rinsing the extraction system for a better coffee experience.</p>
			                            </div>
			                        </li>
			                        <!-- <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 2 of 5</span> How high is the percentage of recyclable material used in <strong class="v_brand" term="nespresso">Nespresso</strong> machines? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>On average, our machines have a 60% recyclability potential. And today, <strong class="v_brand" term="nespresso">Nespresso</strong> Members can bring their machines back to wherever they bought them or in any electrical appliance store. In Europe, the European Union’s Waste Electrical and Electronic Equipment (WEEE) has been in force since 2003. It provides a framework in which consumers can return their used electrical and electronic appliances for recycling.</p>
			                            </div>
			                        </li> -->
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 3 of 5</span> What does 19 bars mean? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>All the&nbsp;<strong class="v_brand" term="nespresso">Nespresso</strong>&nbsp;machines are equipped with a 19-bar pressure pump, which provides the power needed to pierce the film of the capsule and release the coffee's 900 or so different aromas.</p>
			                                <p>The shape of the&nbsp;<strong class="v_brand" term="nespresso">Nespresso</strong>&nbsp;capsule has been specially designed to ensure that the pressurized water flows evenly through the ground coffee during extraction. The temperature and flow time are also set to ensure that each precious aroma is expressed.</p>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 4 of 5</span> My machine has little or no coffee flow? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <p>If there is no coffee flow, please check that:</p>
			                                <ul>
			                                    <li>The main supply cord is not trapped between the water tank and the machine</li>
			                                    <li>The water tank is sufficiently full and correctly positioned</li>
			                                    <li>Your machine is turned ON (lights ON)</li>
			                                    <li>If there is low coffee flow, it means you may need to descale your machine.</li>
			                                </ul>
			                            </div>
			                        </li>
			                        <li class="v_accordionItem">
			                            <button class="faq-item v_title" data-translate="Accordion item" data-of="of" tabindex="0" aria-expanded="false"> <i class="fn_moreCircle"></i> <span class="v_visually_hidden">Accordion item 5 of 5</span> What does the blinking (or no light) on my <strong class="v_brand" term="citiz">Citiz</strong> machine mean? </button>
			                            <div class="v_wysiwyg" aria-hidden="true">
			                                <ol>
			                                    <li>For irregular blinking, please check that the machine is not in descaling mode (fast and regular flashing). With the machine ON, try to exit descaling mode by pressing both buttons for 3 seconds. The machine should stop blinking.</li>
			                                    <li>If there is no light, it means that this machine switches itself OFF automatically after 9 mins (factory settings). This time can be changed to 30 mins.<br>Procedure:<br>- Switch the machine OFF.<br>- Press the Espresso button for 3 seconds<br>- The Espresso button will blink to indicate the current setting (once for 9 mins, twice for 30 mins)<br>- Press the Espresso button once for 9 mins or twice for 30 mins<br>- To exit, press the Lungo button for 3 seconds</li>
			                                </ol>
			                            </div>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_relativeProducts v_sectionLight v_key_relativeProducts" id="relativeProducts" data-label="Relative Products">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 data-wow="" class="wow">Discover all the <strong class="v_brand" term="nespresso">Nespresso</strong> machines</h2>
			            <div class="v_slider v_sliderFlex">
			                <button class="v_sliderArrow v_sliderPrev" tabindex="-1" aria-hidden="true"> <i class="fn_angleLeft"></i> </button>
			                <div class="v_slideContainer" style="touch-action: pan-y; -moz-user-select: none;">
			                    <div class="v_slide" style="transform: translateX(0); transition: all 1.3s ease 0.15s;">
			                        <ul aria-hidden="true">
			                        	<?php foreach ($machines as $key => $machine):?>
	                                        <?php foreach($products as $k => $product): ?>
	                                            <?php if (html_entity_decode($machine) == html_entity_decode($product->name)): ?>
	                                                <li class="nepresso-machine-product" style="width: 199.2px;">
						                                <div data-product-item-id="<?= $product->post_id ?>">
						                                    <div class="v_imageContainer">
						                                        <a class="v_cell" href="<?= $product->url ?>" tabindex="-1" style="background-image: url('<?= $product->image ? $product->image[0] : '' ?>');" lazy="loading"> <span class="v_visually_hidden"><strong class="v_brand" term="<?= $product->name ?>"><?= $product->name ?></strong></span> </a>
						                                    </div>
						                                    <div class="v_sliderItemText">
						                                        <h3 aria-hidden="true"><span><strong class="v_brand" term="prodigio"><?= $product->name ?></strong></span></h3>

						                                    </div>
						                                </div>
						                            </li>
	                                            <?php endif ?>
	                                        <?php endforeach; ?>
                                        <?php endforeach; ?>
                                    </ul>
			                    </div>
			                </div>
			                <button class="v_sliderArrow v_sliderNext" tabindex="-1" aria-hidden="true"> <i class="fn_angleRight"></i> </button>
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_corporate v_sectionLeft v_sectionTop v_key_corporate" id="corporate" data-label="Corporate">
			    <div class="bg_parallax skrollable skrollable-between bg-xl" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_XL.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="bg_parallax skrollable skrollable-between bg-small" data-top-bottom="transform:translate3d(0, -30%, 0)" data-bottom-top="transform:translate3d(0, -70%, 0)" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/discover-more/why_nespresso_S.jpg'); transform: translate3d(0px, -51.4049%, 0px);" lazy="loaded"></div>
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <div class="v_text">
			                <h2 data-wow="" class="wow">Why <strong class="v_brand" term="nespresso">Nespresso</strong>?</h2>
			                <p data-wow="" class="wow">A cup of coffee is much more than a break. It is your small ritual. Make it an unparalleled experience.</p>
			                <div class="v_wysiwyg wow" data-wow="">
			                    <p>Choose <strong class="v_brand" term="nespresso">Nespresso</strong>, do not settle for less: strictly-selected coffee coming in a matchless range of exclusive varieties, coffee machines combining smart design with simple state-of-the-art technology, refined accessories, indulgent sweet treats and services anticipating your every desire. What else?</p>
			                </div>
			                <!-- <a class="v_link v_iconLeft" href="https://www.nespresso.com/es/en/discover-le-club"> <i class="fn_arrowLink"></i> Read more about <strong class="v_brand" term="nespresso">Nespresso</strong> </a> -->
			            </div>
			        </div>
			    </div>
			</section>
			<section class="vue_services v_sectionDark v_key_services" id="services" data-label="Services">
			    <div class="v_sectionRestrict">
			        <div class="v_sectionContent">
			            <h2 class="v_visually_hidden"><strong class="v_brand" term="nespresso">Nespresso</strong> Services</h2>
			            <ul class="v_row5">
			                <li data-wow="" class="wow"> <img alt="" class="freeDelivery" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/freeDelivery.svg">
			                    <h3>Free delivery in the next 24 hours</h3>
			                    <p>Receive your coffee in just 24 hours at an address of your choice.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="pickup" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/pickup.svg">
			                    <h3>Pick up in a boutique today</h3>
			                    <p>Order online and collect your order at your favourite Boutique just 2 hours later.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="assistance247" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/assistance247.svg">
			                    <h3>Assistance for your machine</h3>
			                    <p>Need help with your machine? Our experts are here for you.</p>
			                </li><li data-wow="" class="wow"> <img alt="" class="payment" src="/wp-content/themes/storefront-child/images/coffee-origins/discover-more/payment.svg">
			                    <h3>Secured payment transactions</h3>
			                    <p>SSL encryption means sensitive information is always transmitted securely.</p>
			                </li>
			            </ul>
			        </div>
			    </div>
			</section>
		</div>
	</div>
</main>
