<?php
$activeMenu = 1;
$activeStep = 3;
?>
<div class="container">
    <div class="row">

        <!-- Sidebar account -->
        <div class="col-xs-12 col-md-3 account-sidebar">
            <?php include('components/my-account/menu.php'); ?>
        </div>
        <!-- endof: Sidebar account -->

        <div class="col-xs-12 col-md-8-5 account-detail my-easyorders  my-easyorders__step3 checkister">
            <header>
                <h2>CREATION</h2>
            </header>


            <?php include('components/my-easyordering/steps.php'); ?>

            <div class="my-easyordering-header clearfix">
                <h3 class="pull-left my-easyordering-header__title">Delivery</h3>
                <div class="pull-right clearfix">
                    <a href="easyorder.php?create=2" title="Back" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Back</a>
                    <a href="easyorder.php?create=4" title="Validate" class="btn btn-icon-right btn-green"><i class="fa fa-angle-right"></i>Validate</a>
                </div>
            </div>

            <div class="row main-content">
                <h3 class="main-content__title">Delivery address</h3>
                <div class="bloc-delivery bloc-address" style="background: white">
					<div class="row">
		                <dl class="col-sm-6">
			                <dt class="address__preview__label">Address</dt>
			                <dd class="address__preview__value">
				                Mr First name second name <br>
				                Adress line <br>
				                1234 City <br>
				                Switzerland
			                </dd>
			                <a href="adresses.php" class="link link--right" title="Edit address">Edit address</a>
		                </dl>
		                <dl class="col-sm-6">
			                <dt class="address__preview__label">Delivery remark</dt>
			                <dd class="address__preview__value">Door Code 1234 – Leave on the door front if not there.</dd>
		                </dl>
	                    <p class="pull-left col-sm-12">Currently set as your default delivery address</p>
					</div>
                    <div class="delivery-address-buttons">
                        <a href="adresses.php" class="btn btn-primary" title="Choose another delivery address">Choose another delivery</a>
                        <a href="adresses.php" class="btn btn-primary" title="Add another delivery address">Add another address</a>
                    </div>
                </div>
                <h3 class="main-content__title">Delivery method</h3>
                <h4 class="main-content__subtitle">Delivery at home or office</h4>
                <div class="bloc-delivery">
                    <div class="radio" role="group" aria-labelledby="delivery_method2">
                        <label for="delivery_method2">
                            <input name="delivery_method" id="delivery_method2" value="Standard" title="Standard" type="radio">
                            <i class="icon-Delivery_off"></i>
                            <span>Standard</span>
                            <span class="delivery-free">Free</span>
                        </label>
                    </div>
                    <div class="content">
                        <p>We deliver all your orders in 24h free of charge. Orders plan from Monday to Friday. For a delivery on Saturday, please select the free «Saturday Delivery».</p>
                    </div>
                </div>
            </div>

	        <div class="row create-easyorder__header-footer create-easyorder__header-footer--footer">
		        <div>
			        <a href="#" title="FAQ" class="link link--right">FAQ</a>
		        </div>
		        <div>
			        <a href="easyorder.php?create=2" title="Back" class="btn btn-primary btn-icon"><i class="fa fa-angle-left"></i>Back</a>
			        <a href="easyorder.php?create=4" title="Validate" class="btn btn-icon-right btn-green pull-right"><i class="fa fa-angle-right"></i>Validate</a>
		        </div>
	        </div>


        </div>
    </div>
</div>