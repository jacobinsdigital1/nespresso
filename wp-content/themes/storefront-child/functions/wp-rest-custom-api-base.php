<?php

class CustomApiBaseController extends WP_REST_Controller {
    //The namespace and version for the REST SERVER
    var $my_namespace = 'wc/v';
    var $my_version = '2';
    public function register_routes() {
        $namespace = $this->my_namespace.$this->my_version;

        register_rest_route($namespace, '/products/stockupdate', array(
                'args'   => array(
                    'id' => array(
                        'description' => __( 'Unique identifier for the resource.', 'woocommerce' ),
                        'type'        => 'integer',
                    ),
                ),
                array(
                    'methods'  => 'PUT',
                    'callback' => array(new CustomApiDefaultController('product_stockupdate'), 'init'),
                )
            )
        );

        register_rest_route($namespace, '/products/statusupdate', array(
                'args'   => array(
                    'id' => array(
                        'description' => __( 'Unique identifier for the resource.', 'woocommerce' ),
                        'type'        => 'integer',
                    ),
                ),
                array(
                    'methods'  => 'PUT',
                    'callback' => array(new CustomApiDefaultController('update_order_status'), 'init'),
                )
            )
        );

        register_rest_route($namespace, '/custom/orders', array(
                array(
                    'methods'  => 'GET',
                    'callback' => array(new CustomApiDefaultController('get_order_by_modified_date'), 'init'),
                )
            )
        );
    }
    // Register our REST Server
    public function hook_rest_server() {
        add_action('rest_api_init', array($this, 'register_routes'));
        //add_action('rest_api_init', 'my_customize_rest_cors', 15);
    }
    public function my_customize_rest_cors() {
        remove_filter('rest_pre_serve_request', 'rest_send_cors_headers');
        remove_filter('rest_post_dispatch', 'rest_send_allow_header');
    }
}

$ApiBaseController = new CustomApiBaseController();
$ApiBaseController->hook_rest_server();
