<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */
?>

<!-- content -->
<main id="content" class="delivery-address shopping-bag">
	<div class="container">
	    <?php $stepActive = 2;
	        $nameStep = 'Personal Information';
	        $nameStepUrl = 'checkister-personal-information';
	    ?>

	    <?php get_template_part('components/wizard'); ?>

	    <div class="checkister step2" id="JumpContent" tabindex="-1">
	        <header>
	            <h2>Delivery Mode</h2>
	        </header>
	        <section class="row">
	            <div class="hidden-xs hidden-sm col-md-4 pull-right">
	                <aside>
	                    <span class="col-title">Order details</span>
	                    <div class="sbag-small sbag-loading"></div>
	                    <ul class="bloc-total hidden">
	                        <li class="bloc-total-stotal">
	                            <span class="stotal">Subtotal</span>
	                            <span class="price">$<span class="price-int">0</span></span>
	                        </li>
	                        <li class="bloc-total-vat">
	                            <span class="vat">Vat (inCL.)</span>
	                            <span class="price">$<span class="price-int">0</span></span>
	                        </li>
	                        <li class="bloc-total-total">
	                            <span class="total">Total</span>
	                            <span class="tprice">$<span class="price-int">0</span></span>
	                        </li>
	                    </ul>
		                <a href="<?php bloginfo('url'); ?>/shopping-bag" title="Modify" class="btn btn-primary btn-block">Edit</a>
	                </aside>
	            </div>

	            <div class="col-xs-12 col-md-8">
	                <div class="main-content clearfix">
	                    <h3 class="main-content__title">DELIVERY Address</h3>
	                    <form method="POST" action="<?php bloginfo('url'); ?>delivery-mode" style="background: #F9F9F9;">
	                        <section class="form private">
	                            <div class="checkister-wrapper clearfix">
	                                <p>Please fill the form below, Fields marked with * are required.</p>

		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="delivery-type-private">Your delivery address is<span class="required">*</span></label>
			                            <div class="col-sm-6 radio-group">
				                            <div class="radio" role="group" aria-labelledby="delivery-type-private">
					                            <label for="delivery-type-private">
						                            <input value="private" id="delivery-type-private" name="delivery-type" checked="checked" type="radio">
						                            Private
					                            </label>
				                            </div>
				                            <div class="radio" role="group" aria-labelledby="delivery-type-company">
					                            <label for="delivery-type-company">
						                            <input value="company" id="delivery-type-company" name="delivery-type" type="radio">
						                            Company
					                            </label>
				                            </div>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic company-hidden">
			                            <label class="desktop-label col-sm-3 col-md-4" for="title">Title<span class="required">*</span></label>
			                            <div class="col-sm-4">
				                            <div class="dropdown dropdown--input dropdown--init">
					                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						                            <span class="mobile-label">Title<span class="required">*</span></span>
						                            <span class="current">Please select</span>
					                            </button>
					                            <select tabindex="-1" name="Title" id="title">
						                            <option value="0">Please select</option>
						                            <option value="1">Mr/Ms</option>
						                            <option value="2">Fran&ccedil;ais</option>
						                            <option value="3">Deutsch</option>
					                            </select>
				                            </div>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="country">Country<span class="required">*</span></label>
			                            <div class="col-sm-4">
				                            <div class="dropdown dropdown--input dropdown--init">
					                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						                            <span class="mobile-label">Country<span class="required">*</span></span>
						                            <span class="current">Please select</span>
					                            </button>
					                            <select tabindex="-1" name="Country" id="country">
						                            <option value="0">Please select</option>
						                            <option value="1">United States</option>
						                            <option value="2">Fran&ccedil;ais</option>
						                            <option value="3">Deutsch</option>
					                            </select>
				                            </div>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic  company-hidden">
			                            <label class="desktop-label col-sm-3 col-md-4" for="name">First Name<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="First name" id="name" name="name" placeholder="First name" class="form-control col-sm-12 col-md-9">
				                            <span class="mobile-label">First Name<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic  company-hidden">
			                            <label class="desktop-label col-sm-3 col-md-4" for="lastname">Last Name<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="Last name" id="lastname" name="lastname" placeholder="Last name" class="form-control col-sm-12 col-md-9">
				                            <span class="mobile-label">Last Name<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic  private-hidden">
			                            <label class="desktop-label col-sm-3 col-md-4" for="contactname">Contact Name<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="Contact name" id="contactname" name="contactname" placeholder="Contact name" class="form-control col-sm-12 col-md-9">
				                            <span class="mobile-label">Contact Name<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic private-hidden">
			                            <label class="desktop-label col-sm-3 col-md-4" for="companyname">Company Name<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="Company name" id="companyname" name="companyname" placeholder="Company name" class="form-control col-sm-12 col-md-9">
				                            <span class="mobile-label">Company Name<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <hr>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="name">Address line 1<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="Address line 1" id="name" name="name" placeholder="Address line 1" class="form-control col-sm-12 col-md-9">
				                            <span class="mobile-label">Address line 1<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="name">Address line 2<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="Address line 2" id="name" name="name" placeholder="Address line 2" class="form-control col-sm-12 col-md-9">
				                            <span class="mobile-label">Address line 2<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="name">Post code<span class="required">*</span></label>
			                            <div class="col-sm-3">
				                            <input type="text" title="Post code" id="name" name="name" placeholder="Ex: 1221" class="form-control col-sm-12 col-md-9 form-control--withPlaceholder">
				                            <span class="mobile-label">Post code<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="city">City<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="City" id="city" name="city" placeholder="City" class="form-control col-sm-12 col-md-9">
				                            <span class="mobile-label">City<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <hr>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="name">Phone number 1<span class="required">*</span></label>
			                            <div class="col-sm-6">
				                            <input type="text" title="Phone number 1" id="name" name="name" placeholder="Ex: 0098 765 43 21" class="form-control col-sm-12 col-md-9  form-control--withPlaceholder">
				                            <span class="mobile-label">Phone number 1<span class="required">*</span></span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="language1">Home/Office<span class="required">*</span></label>
			                            <div class="col-sm-4">
				                            <div class="dropdown dropdown--input dropdown--init">
					                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						                            <span class="mobile-label">Home/Office<span class="required">*</span></span>
						                            <span class="current">Please select</span>
					                            </button>
					                            <select tabindex="-1" name="language" id="language1">
						                            <option value="0">Please select</option>
						                            <option value="1">Home</option>
						                            <option value="2">Fran&ccedil;ais</option>
						                            <option value="3">Deutsch</option>
					                            </select>
				                            </div>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="language2">Country<span class="required">*</span></label>
			                            <div class="col-sm-4">
				                            <div class="dropdown dropdown--input dropdown--init">
					                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						                            <span class="mobile-label">Country<span class="required">*</span></span>
						                            <span class="current">Please select</span>
					                            </button>
					                            <select tabindex="-1" name="language" id="language2">
						                            <option value="0">Please select</option>
						                            <option value="1">United States</option>
						                            <option value="2">Fran&ccedil;ais</option>
						                            <option value="3">Deutsch</option>
					                            </select>
				                            </div>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="name">Phone number 2 </label>
			                            <div class="col-sm-6">
				                            <input type="text" title="Phone number 2" id="name" name="name" placeholder="Ex: 0098 765 43 21" class="form-control col-sm-12 col-md-9 form-control--withPlaceholder">
				                            <span class="mobile-label">Phone number 2</span>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="language3">Home/Office</label>
			                            <div class="col-sm-4">
				                            <div class="dropdown dropdown--input dropdown--init">
					                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						                            <span class="mobile-label">Home/Office</span>
						                            <span class="current">Please select</span>
					                            </button>
					                            <select tabindex="-1" name="language" id="language3">
						                            <option value="0">Please select</option>
						                            <option value="1">Home</option>
						                            <option value="2">Fran&ccedil;ais</option>
						                            <option value="3">Deutsch</option>
					                            </select>
				                            </div>
			                            </div>
		                            </div>
		                            <div class="input-group input-group-generic">
			                            <label class="desktop-label col-sm-3 col-md-4" for="language4">Country</label>
			                            <div class="col-sm-4">
				                            <div class="dropdown dropdown--input dropdown--init">
					                            <button class="btn btn-primary btn-white btn-icon-right"><i class="fa fa-angle-down"></i>
						                            <span class="mobile-label">Country</span>
						                            <span class="current">Please select</span>
					                            </button>
					                            <select tabindex="-1" name="language" id="language4">
						                            <option value="0">Please select</option>
						                            <option value="1">United States</option>
						                            <option value="2">Fran&ccedil;ais</option>
						                            <option value="3">Deutsch</option>
					                            </select>
				                            </div>
			                            </div>
		                            </div>
		                            <hr>
	                                <div class="input-group input-group-generic">
	                                    <label class="desktop-label col-sm-3 col-md-4" for="name">Delivery remark</label>
	                                    <div class="col-sm-6">
	                                        <input type="text" title="Delivery remark" id="name" name="name" placeholder="Delivery remark" class="form-control col-sm-12 col-md-9">
	                                        <span class="mobile-label">Delivery remark</span>
	                                    </div>
	                                </div>
	                                <hr>
	                                <span class="title">Newsletter</span>
	                                <div class="input-group input-group-generic">
	                                    <div class="checkbox">
	                                        <label for="keep-me-informated">
	                                            <input value="none" id="keep-me-informated" name="keep-me-informated" type="checkbox">
	                                            Yes, I would like to receive special offers and information from the <i>Nespresso</i> Club by email.
	                                        </label>
	                                    </div>
	                                </div>
	                            </div>
		                        <div class="link-wrapper">
			                        <a class="btn btn-primary btn-icon-right pull-right" href="<?php bloginfo('url'); ?>/delivery-mode" title="Save my address"><span>Save my address</span> <i class="icon icon-arrow_left"></i></a>
		                        </div>
	                        </section>
	                    </form>
	                </div>
	            </div>

	        </section>
	    </div>
    </div>
</main>

<?php
global $scripts;
$scripts[]='js/components/shopping-bag-small.js';
$scripts[]='js/components/form.js';
?>

<!-- /content -->
