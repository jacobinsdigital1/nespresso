<?php

/**
 * Provide a admin positive cup area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://minionsolutions.com/
 * @since      1.0.0
 *
 * @package    Nespresso
 * @subpackage Nespresso/admin/partials
 */


// get the current url, to be used to redirect later
global $wp;
$current_url = home_url(add_query_arg(null, null));
$category_banner = get_nespresso_category_banner();

?>
<div class="wrap pd-top-20 pd-bottom-20" id="promotional-banner-pages">

    <h3>Promotional Banners in Pages</h3>

    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        enctype="multipart/form-data"
        role="form"
        id="form-promotional-banner-pages"
    >

        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_category_banner">

                <input type="hidden" name="type" value="update-or-create">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <h4></h4>
                 <!-- Advisory -->
                <!-- Desktop image -->
                <div class="form-group">
                    <label for="image">Advisory Banner Image (Desktop):</label>
                    <br>
                    <img class="<?= @$category_banner['advisory_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" style="height: auto;" src="<?= @$category_banner['advisory_banner_image_url'] ;?>" id="image-thumbnail-advisory-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="advisory-banner">Select Image</a>
                    <input type="hidden" name="advisory_banner_image_url" value="<?= @$category_banner['advisory_banner_image_url'] ;?>" id="image-url-advisory-banner">
                </div>

                <!-- Mobile image -->
                <div class="form-group">
                    <label for="image">Advisory Banner Image (Mobile):</label>
                    <br>
                    <img class="<?= @$category_banner['advisory_banner_image_url_mobile'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" style="height: auto;" src="<?= @$category_banner['advisory_banner_image_url_mobile'] ;?>" id="image-thumbnail-advisory-banner-mobile">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="advisory-banner-mobile">Select Image</a>
                    <input type="hidden" name="advisory_banner_image_url_mobile" value="<?= @$category_banner['advisory_banner_image_url_mobile'] ;?>" id="image-url-advisory-banner-mobile">
                </div>

                <div class="form-group">
                    <label for="advisory_banner_callback">Advisory Status Callback (Optional) <small>e.g how-to-order</small>:</label>
                    <br>
                    <input type="text" id="advisory_banner_callback" placeholder="how-to-order" name="advisory_banner_callback" class="form-control" value="<?= @$category_banner['advisory_banner_callback'] ;?>">
                </div>

                <div class="form-group">
                    <label for="image">Advisory Status:</label>
                    <br>
                    <select name="advisory_banner_status">
                        <option value="Inactive" <?php echo (isset($category_banner['advisory_banner_status']) && $category_banner['advisory_banner_status'] == 'Inactive') ? 'selected' : '' ?> >Inactive</option>
                        <option value="Active" <?php echo (isset($category_banner['advisory_banner_status']) && $category_banner['advisory_banner_status'] == 'Active') ? 'selected' : '' ?> >Active</option>
                    </select>
                </div>

                <!-- Coffee -->
                <!-- Desktop image -->
                <div class="form-group">
                    <label for="image">Coffee Banner Image (Desktop):</label>
                    <br>
                    <img class="<?= @$category_banner['coffee_category_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['coffee_category_banner_image_url'] ;?>" id="image-thumbnail-coffee-category-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="coffee-category-banner">Select Image</a>
                    <input type="hidden" name="coffee_category_banner_image_url" value="<?= @$category_banner['coffee_category_banner_image_url'] ;?>" id="image-url-coffee-category-banner">
                </div>



                <!-- Mobile image -->
                <div class="form-group">
                    <label for="image">Coffee Banner Image (Mobile):</label>
                    <br>
                    <img class="<?= @$category_banner['coffee_category_banner_image_url_mobile'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['coffee_category_banner_image_url_mobile'] ;?>" id="image-thumbnail-coffee-category-banner-mobile">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="coffee-category-banner-mobile">Select Image</a>
                    <input type="hidden" name="coffee_category_banner_image_url_mobile" value="<?= @$category_banner['coffee_category_banner_image_url_mobile'] ;?>" id="image-url-coffee-category-banner-mobile">
                </div>

                 <!--Coffee link -->
                <div class="form-group">
                    <label for="link">Coffee Banner Link:</label>
                    <input type="text" name="coffee_category_banner_link" value="<?= @$category_banner['coffee_category_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>
                <div class="form-group">
                    <label for="link">Coffee Banner Promo name:</label>
                    <input type="text" name="coffee_category_banner_promo_name" value="<?= @$category_banner['coffee_category_banner_promo_name'] ?>" class="form-control" placeholder="e.g. Discovery offer">
                </div>

                <!-- Machine -->
                <!-- Desktop image -->
                <div class="form-group">
                    <label for="image">Machine Banner Image (Desktop):</label>
                    <br>
                    <img class="<?= @$category_banner['machine_category_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['machine_category_banner_image_url'] ;?>" id="image-thumbnail-machine-category-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="machine-category-banner">Select Image</a>
                    <input type="hidden" name="machine_category_banner_image_url" value="<?= @$category_banner['machine_category_banner_image_url'] ;?>" id="image-url-machine-category-banner">
                </div>


                <!-- Mobile image -->
                <div class="form-group">
                    <label for="image">Machine Banner Image (Mobile):</label>
                    <br>
                    <img class="<?= @$category_banner['machine_category_banner_image_url_mobile'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['machine_category_banner_image_url_mobile'] ;?>" id="image-thumbnail-machine-category-banner-mobile">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="machine-category-banner-mobile">Select Image</a>
                    <input type="hidden" name="machine_category_banner_image_url_mobile" value="<?= @$category_banner['machine_category_banner_image_url_mobile'] ;?>" id="image-url-machine-category-banner-mobile">
                </div>

                <!--Machine link -->
                <div class="form-group">
                    <label for="link">Machine Banner Link:</label>
                    <input type="text" name="machine_category_banner_link" value="<?= @$category_banner['machine_category_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>
                <div class="form-group">
                    <label for="link">Machine Banner Promo name:</label>
                    <input type="text" name="machine_category_banner_promo_name" value="<?= @$category_banner['machine_category_banner_promo_name'] ?>" class="form-control" placeholder="e.g. Pixie & Aeroccino 3 bundle offer">
                </div>

                <!-- Accessory -->
                <!-- Desktop image -->
                <div class="form-group">
                    <label for="image">Accessory Banner Image (Desktop):</label>
                    <br>
                    <img class="<?= @$category_banner['accessory_category_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['accessory_category_banner_image_url'] ;?>" id="image-thumbnail-accessory-category-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="accessory-category-banner">Select Image</a>
                    <input type="hidden" name="accessory_category_banner_image_url" value="<?= @$category_banner['accessory_category_banner_image_url'] ;?>" id="image-url-accessory-category-banner">
                </div>

                <!-- Mobile image -->
                <div class="form-group">
                    <label for="image">Accessory Banner Image (Mobile):</label>
                    <br>
                    <img class="<?= @$category_banner['accessory_category_banner_image_url_mobile'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['accessory_category_banner_image_url_mobile'] ;?>" id="image-thumbnail-accessory-category-banner-mobile">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="accessory-category-banner-mobile">Select Image</a>
                    <input type="hidden" name="accessory_category_banner_image_url_mobile" value="<?= @$category_banner['accessory_category_banner_image_url_mobile'] ;?>" id="image-url-accessory-category-banner-mobile">
                </div>

                <!--Accessory link -->
                <div class="form-group">
                    <label for="link">Accessory Banner Link:</label>
                    <input type="text" name="accessory_category_banner_link" value="<?= @$category_banner['accessory_category_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>
                <div class="form-group">
                    <label for="link">Accessory Banner Promo name:</label>
                    <input type="text" name="accessory_category_banner_promo_name" value="<?= @$category_banner['accessory_category_banner_promo_name'] ?>" class="form-control" placeholder="e.g. BONBONNIERE CAPSULE DISPENSER">
                </div>

                <!-- Store locator -->
                <!-- Desktop image -->
                <div class="form-group">
                    <label for="image">Store Locator Banner Image (Desktop):</label>
                    <br>
                    <img class="<?= @$category_banner['storelocator_category_banner_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['storelocator_category_banner_image_url'] ;?>" id="image-thumbnail-storelocator-category-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="storelocator-category-banner">Select Image</a>
                    <input type="hidden" name="storelocator_category_banner_image_url" value="<?= @$category_banner['storelocator_category_banner_image_url'] ;?>" id="image-url-storelocator-category-banner">
                </div>

                <!-- Mobile image -->
                <div class="form-group">
                    <label for="image">Store Locator Banner Image (Mobile):</label>
                    <br>
                    <img class="<?= @$category_banner['storelocator_category_banner_image_url_mobile'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['storelocator_category_banner_image_url_mobile'] ;?>" id="image-thumbnail-storelocator-category-banner-mobile">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="storelocator-category-banner-mobile">Select Image</a>
                    <input type="hidden" name="storelocator_category_banner_image_url_mobile" value="<?= @$category_banner['storelocator_category_banner_image_url_mobile'] ;?>" id="image-url-storelocator-category-banner-mobile">
                </div>

                <!--Storelocator link -->
                <div class="form-group">
                    <label for="link">Store Locator Banner Link:</label>
                    <input type="text" name="storelocator_category_banner_link" value="<?= @$category_banner['storelocator_category_banner_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>
                <div class="form-group">
                    <label for="link">Store Locator Banner Promo name:</label>
                    <input type="text" name="storelocator_category_banner_promo_name" value="<?= @$category_banner['storelocator_category_banner_promo_name'] ?>" class="form-control" placeholder="e.g. The ultimate coffee experience">
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
        <hr>
        <h3>Dynamic Banner in Product list</h3>


        <div class="panel panel-default">
            <div class="panel-body">
                <!-- Coffee Sidebar Image -->
                <div class="form-group">
                    <label for="image">Coffee Sidebar Image:</label>
                    <br>
                    <img class="<?= @$category_banner['coffee_sidebar_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['coffee_sidebar_image_url'] ;?>" id="image-thumbnail-coffee-sidebar-image">
					<span style="cursor:pointer" class="btn" id="coffee_sidebar_image_url_remove">Remove</span>
					<script>
						jQuery('#coffee_sidebar_image_url_remove').on('click',function(){
							jQuery('#image-url-coffee-sidebar-image').val('');
							jQuery('#image-thumbnail-coffee-sidebar-image').attr('src','');
						});
					</script>
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="coffee-sidebar-image">Select Image</a>
                    <input type="hidden" name="coffee_sidebar_image_url" value="<?= @$category_banner['coffee_sidebar_image_url'] ;?>" id="image-url-coffee-sidebar-image">
                </div>
                <div class="form-group">
                    <label for="link">Coffee Sidebar Image Link:</label>
                    <input type="text" name="coffee_sidebar_image_link" value="<?= @$category_banner['coffee_sidebar_image_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>
                <div class="form-group">
                    <label for="link">Coffee Sidebar Image Promo name:</label>
                    <input type="text" name="coffee_sidebar_image_promo_name" value="<?= @$category_banner['coffee_sidebar_image_promo_name'] ?>" class="form-control" placeholder="e.g. Pixie & Aeroccino 3 bundle offer">
                </div>
                <!-- Accessory Sidebar Image -->
                <div class="form-group">
                    <label for="image">Accessory Sidebar Image:</label>
                    <br>
                    <img class="<?= @$category_banner['accessory_sidebar_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['accessory_sidebar_image_url'] ;?>" id="image-thumbnail-accessory-sidebar-image">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="accessory-sidebar-image">Select Image</a>
                    <input type="hidden" name="accessory_sidebar_image_url" value="<?= @$category_banner['accessory_sidebar_image_url'] ;?>" id="image-url-accessory-sidebar-image">
                </div>
                <div class="form-group">
                    <label for="link">Accessory Sidebar Image Link:</label>
                    <input type="text" name="accessory_sidebar_image_link" value="<?= @$category_banner['accessory_sidebar_image_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>
                <div class="form-group">
                    <label for="link">Accessory Sidebar Image Promo name:</label>
                    <input type="text" name="accessory_sidebar_image_promo_name" value="<?= @$category_banner['accessory_sidebar_image_promo_name'] ?>" class="form-control" placeholder="e.g. Pixie & Aeroccino 3 bundle offer">
                </div>

                <!-- Machine Sidebar Image -->
                <div class="form-group">
                    <label for="image">Machine Sidebar Image:</label>
                    <br>
                    <img class="<?= @$category_banner['machine_sidebar_image_url'] ? '' : 'hide' ;?> img-thumbnail btn-open-wp-modal" src="<?= @$category_banner['machine_sidebar_image_url'] ;?>" id="image-thumbnail-machine-sidebar-image">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal" data-section="machine-sidebar-image">Select Image</a>
                    <input type="hidden" name="machine_sidebar_image_url" value="<?= @$category_banner['machine_sidebar_image_url'] ;?>" id="image-url-machine-sidebar-image">
                </div>
                <div class="form-group">
                    <label for="link">Machine Sidebar Image Link:</label>
                    <input type="text" name="machine_sidebar_image_link" value="<?= @$category_banner['machine_sidebar_image_link'] ?>" class="form-control" placeholder="e.g. <?= home_url() ?>/product/arpeggio">
                </div>
                <div class="form-group">
                    <label for="link">Machine Sidebar Image Promo name:</label>
                    <input type="text" name="machine_sidebar_image_promo_name" value="<?= @$category_banner['machine_sidebar_image_promo_name'] ?>" class="form-control" placeholder="e.g. Pixie & Aeroccino 3 bundle offer">
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </form>
</div><!-- #positive-cup -->
