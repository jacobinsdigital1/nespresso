
<?php
global $wp;
$current_url = home_url(add_query_arg(null, null));

$left_image = get_nespresso_left_dynamic_banners();
$right_image = get_nespresso_right_dynamic_banners();
 ?>

<div class="wrap pd-top-20 pd-bottom-20" id="dynamic-banner">

    <h3>Dynamic Banners</h3>

    <script type="text/javascript">
        $left_image = <?= $left_image ? json_encode($left_image) : 'null'; ?>;
        $right_image = <?= $right_image ? json_encode($right_image) : 'null'; ?>;
    </script>

    <button class="btn btn-primary pull-right <?= $left_image ? 'hide' : ''; ?>" id="btn-add-dynamic-banner" data-id="left">Add Left Slide</button>
    <h3>Left</h3>
    <table class="table table-striped" id="left-dynamic-banner-list-table">
        <thead>
            <tr>
                <th>Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($left_image ) : ?>
                <?php foreach ( $left_image as $slide ) : ?>
                    <tr data-id="<?= $slide->id ?>"  class="slide-row">
                        <td class="text-center">
                            <?php if ( $slide->image_url ) : ?>
                                <image src="<?= $slide->image_url ?>" class="thumbnail" height="100" style="margin: auto;">
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-success btn-edit-left-dynamic-banner" data-id="<?= $slide->id  ?>">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-danger btn-delete-left-dynamic-banner" data-id="<?= $slide->id  ?>">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        <tbody>
    </table>
    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        enctype="multipart/form-data"
        role="form"
        id="form-left-dynamic-banner"
        class="hide"
    >
        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_left_dynamic_banner">

                <input type="hidden" name="type" value="update-or-create" id="type-left-dynamic-banner">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <input type="hidden" name="id" value="" id="id-left-dynamic-banner">
                <!-- image -->
                <div class="form-group">
                    <label for="image">Image:</label>
                    <br>
                    <img class="hide img-thumbnail btn-open-wp-modal" src="" id="image-thumbnail-left-dynamic-banner" data-section="left-dynamic-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal-left-dynamic-banner" data-section="left-dynamic-banner">Select Image</a>
                    <input type="hidden" name="image_url" id="image-url-left-dynamic-banner" value="">
                </div>

                <!-- link -->
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" name="link" id="link-left-dynamic-banner" value="" class="form-control">
                </div>

                 <!-- content -->
                <div class="form-group">
                    <label for="content">Promo Name:</label>
                    <input type="text" name="content" id="left-dynamic-banner-content" class="form-control">
                </div>

                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-default btn-flat pull-left"
                        id="btn-close-left-dynamic-banner"
                    >
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>
    <button class="btn btn-primary pull-right <?= $right_image ? 'hide' : ''; ?>" id="btn-add-dynamic-banner" data-id="right">Add Right Slide</button>
    <h3>Right</h3>
    <table class="table table-striped" id="right-dynamic-banner-list-table">
        <thead>
            <tr>
                <th>Image</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            <?php if ($right_image ) : ?>
                <?php foreach ( $right_image as $slide ) : ?>
                    <tr data-id="<?= $slide->id ?>"  class="slide-row">
                        <td class="text-center">
                            <?php if ( $slide->image_url ) : ?>
                                <image src="<?= $slide->image_url ?>" class="thumbnail" height="100" style="margin: auto;">
                            <?php endif; ?>
                        </td>
                        <td class="text-center">
                            <button class="btn btn-success btn-edit-right-dynamic-banner" data-id="<?= $slide->id  ?>">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </button>
                            <button class="btn btn-danger btn-delete-right-dynamic-banner" data-id="<?= $slide->id  ?>">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        <tbody>
    </table>
    <form action="<?= esc_url( admin_url('admin-post.php') );  ?>"
        method="post"
        enctype="multipart/form-data"
        role="form"
        id="form-right-dynamic-banner"
        class="hide"
    >
        <div class="panel panel-default">

            <div class="panel-body">

                <input type="hidden" name="action" value="nespresso_right_dynamic_banner">

                <input type="hidden" name="type" value="update-or-create" id="type-right-dynamic-banner">

                <input type="hidden" name="redirect_url" value="<?= $current_url ?>">

                <input type="hidden" name="id" value="" id="id-right-dynamic-banner">
                <!-- image -->
                <div class="form-group">
                    <label for="image">Image:</label>
                    <br>
                    <img class="hide img-thumbnail btn-open-wp-modal" src="" id="image-thumbnail-right-dynamic-banner" data-section="right-dynamic-banner">
                    <br>
                    <a href="javascript:void(0)" class="btn-open-wp-modal-right-dynamic-banner" data-section="right-dynamic-banner">Select Image</a>
                    <input type="hidden" name="image_url" id="image-url-right-dynamic-banner" value="">
                </div>

                <!-- link -->
                <div class="form-group">
                    <label for="link">Link:</label>
                    <input type="text" name="link" id="link-right-dynamic-banner" value="" class="form-control">
                </div>

                 <!-- content -->
                <div class="form-group">
                    <label for="content">Promo Name:</label>
                    <input type="text" name="content" id="right-dynamic-banner-content" class="form-control">
                </div>

                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-default btn-flat pull-left"
                        id="btn-close-right-dynamic-banner"
                    >
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-primary btn-flat">
                        Submit
                    </button>
                </div>

            </div><!-- .panel-body -->

        </div><!-- .panel -->
    </form>
</div><!-- #slider -->
