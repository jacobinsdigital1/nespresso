<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post;

$product = include_accessory_product_meta_from_results($post, $get_recommended_products = true);

$hq_data  = get_hq_data($product->sku);

global $scripts;

?>

<?php get_header();?>

<?php get_template_part('components/generics/qty-selector');?>

<!-- content -->
<main id="content">

    <div class="product-page product-page--accessories" id="JumpContent" tabindex="-1">

	    <div id="product__header" class="product__header product__header--light">
		    <div class="back-container">
			    <a href="/accessory-list" title="All accessories" class="btn-back link link--left">Back to all accessories</a>
		    </div>
			<?=get_nespresso_show_off($product->label_off,'pro-detail-off');?>
		    <div class="product__slider">
			    <a href="#" class="product__slider__close"></a>
			    <ul class="product__slider__list">
				    <li class="active">
					    <img src="<?=$product->image ? $product->image[0] : ''?>" alt="<?php $product->name?>" draggable="false"/>
				    </li>
			    </ul>
		    </div>
		    <div class="product__info">
			    <div>
				    <div class="product__info__type">Original</div>
					
				    <h2 class="product__info__title"><?=strtoupper($product->name)?></h2>
				    <span class="product__info__price uppercase"><?= wc_price($product->price) ?>
						<?php if($product->regular_price>$product->price){ ?>
							<span class="product-price--old" style="color:gray;text-decoration: line-through;display:block"><?=wc_price($product->regular_price) ?></span>
						<?php  } ?>
					</span>
				    <div class="container-btn-custom">
				    	<div id="qty-selector-content" class=""></div>
					    <a href="#" class="btn-custom-add-to-cart btn btn-icon btn-green btn-block btn-icon-right btn-large"
					      	data-id="<?=$product->product_id?>"
					      	data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->product_id ?>"
						    data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
						    data-sku="<?= $product->sku ?>"
							data-type="<?=$product->product_type?>"
							data-name="<?=$product->name?>"
							data-price="<?=$product->price?>"
							data-image-url="<?=$product->image ? $product->image[0] : ''?>"
							data-picture="<?=$product->image ? $product->image[0] : ''?>"
							data-qty-step="1"
							data-vat="1"
				      	>
						    <i class="icon-basket"></i><span class="btn-add-qty"></span><span>Add to basket</span><i class="icon-plus"></i>
					    </a>
					    <div  class="mobi-select-cont" >
			                <select class="mobi-sec" style="width: 100%;height: 100%;"
			                data-id="<?=$product->product_id?>"
			                data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->product_id ?>"
						    data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
						    data-sku="<?= $product->sku ?>"
							data-cart="true"
							data-type="<?=$product->product_type?>"
							data-name="<?=$product->name?>"
							data-price="<?=$product->price?>"
							data-image-url="<?=$product->image ? $product->image[0] : ''?>"
							data-picture="<?=$product->image ? $product->image[0] : ''?>"
							data-qty-step="1"
							data-vat="1"
				                >

			                    <?php
			                    $c=0;
			                        while ( $c<= 100) {
			                           echo "<option value='$c'>$c</option>";
			                           $c+=1;
			                        }
			                     ?>
			                </select>
			            </div>
				    </div>
					<p style="margin:20px 0px 0px 0px">
						<i class="icon-Delivery_off"></i>
						<span>Get Free delivery on all orders above ₫1,500,000</span>
					</p>
					
			    </div>
		    </div>
	    </div>

        <div class="product-content">
	        <div class="container">
	        	<?php if (isset($product->description)): ?>
		        	<!-- product description -->
		            <div class="product-content__text"><?php echo $product->description; ?></div>
		        <?php endif;?>

	            <!--h3>EXCEPTIONALLY INTENSE AND SYRUPY</h3>

	            <p>Set of 2 double-wall Espresso cups (80 ml) in stainless steel matching the Arpeggio Grand Cru color, with 2 stirrers (12.5 cm) in stainless steel. Hand wash recommended.</p>
	            <p>
	                <a href="#" class="link link--right" title="Discover the range">Discover the range</a>
	            </p> -->

	            <?php if ($product->video_url): ?>
	            	<p class="product-video"><iframe width="100%" height="100%" src="<?=$product->video_url?>" frameborder="0" allowfullscreen></iframe></p>
            	<?php endif;?>

	            <?php get_template_part('components/generics/social-links');?>
            </div>
        </div>

        <?php if (isset($product->recommended_products) && $product->recommended_products): ?>
		    <div class="recommended-grey">
			    <div class="container">
				    <h4>RECOMMENDED PRODUCTS</h4>
				     <?php include 'wp-content/themes/storefront-child/components/generics/recommended-products.php';?>
			    </div>
		    </div>
	    <?php endif;?>

    </div>

</main>


<!-- /content -->

<!-- get_footer -->
<?php get_footer();?>
