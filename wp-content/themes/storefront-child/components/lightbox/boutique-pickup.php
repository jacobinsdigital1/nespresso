<div class="pickup" id="JumpContent" tabindex="-1">
    <header>
    	<div class="row">
        	<div class="col-xs-12">
        		<h2>Select a <i>Nespresso</i> boutique</h2>
        	</div>
            <div class="col-xs-2 pickup__header">
                 <div class="pickup__header__target">
                    <img alt="Geolocate me!" src="images/target.png">
                </div>
            </div>
            <div class="col-xs-10">
                <label for="city" class="label-masked">City</label>
                <input type="text" id="city" name="city" placeholder="Enter your city" class="form-control col-xs-9 col-sm-10">
				<button class="btn btn-primary btn-grey" title="search"><i class="fa fa-search"></i></button>
            </div>
         </div>
    </header>
   
   <div class="row">
        <div class="col-xs-12 pickup__entete">
              20 shops nearby
        </div>
   </div>
   
   <div class="row pickup__shoplist">
   		<?php for($i=0; $i<6;$i++){ ?>
        <div class="col-xs-12 pickup__shop">
              <div class="pickup__shop__info">
                  <strong>Boutique <i>Nespresso</i> Paudex</strong><br />
                  Your order will be ready on Thursday 11/19 At 11h55 AM
              </div>
              <a href="#" class="btn-link pickup__shop__seeaddress">See address and opening hours</a>
              <div class="pickup__shop__address">
              	<address>
                	Route du Lac 3<br />
					1094 Paudex
                 </address>
                 <p>Monday - Friday 08:00 - 19:00<br />Saturday 08:00 - 17:00</p>
              </div>
              
              <div class="pickup__shop__select clearfix">
                  <a href="#" class="btn btn-green btn-icon" title="Select this point"><i class="icon icon-check"></i> Select</a>
              </div>
        </div>
        <?php } ?>
   </div>
                        
</div>