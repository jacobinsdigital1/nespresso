<div class="tool-box bg-white p-20p">
<div id="message" class="woocommerce-message wc-connect rTable">
    <div class="rTableRow">
        <div class="rTableCell">
            <h4><?php _e('<strong>WooCommerce Order CSV</strong>', 'order-import-export-for-woocommerce'); ?></h4>
            <p class="submit">
                <a target="_blank" href="https://www.webtoffee.com/setting-up-order-import-export-plugin-for-woocommerce/" class="button-primary"><?php _e('Documentation', 'order-import-export-for-woocommerce'); ?></a>
                <a class="docs button-primary" href="<?php echo WF_OrderImpExpCsv_FILE_Url.'Sample_Order.csv'; ?>"><?php _e('Sample Order CSV', 'order-import-export-for-woocommerce'); ?></a>
            </p>
        </div>
        <div class="rTableCell">
            <h4><?php _e('<strong>WooCommerce Coupon CSV</strong>', 'order-import-export-for-woocommerce'); ?></h4>
            <p class="submit">
                <a target="_blank" href="https://www.webtoffee.com/how-to-import-and-export-woocommerce-coupons-using-order-coupon-subscription-export-import-plugin/" class="button-primary"><?php _e( 'Documentation', 'order-import-export-for-woocommerce' ); ?></a>
                <a class="docs button-primary" href="<?php echo WF_OrderImpExpCsv_FILE_Url.'Sample_Coupon.csv'; ?>"><?php _e('Sample Coupon CSV', 'order-import-export-for-woocommerce'); ?></a></p>
		<p>
        </div>
    </div>
</div>
</div>