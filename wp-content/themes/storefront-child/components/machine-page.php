<?php

/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $post;

$product = include_machine_product_meta_from_results($post, $single_product_page = true);

$product->reviews = get_product_reviews($product->ID);
$product->embedded_machine_video = get_embedded_machine_video($product->ID);
$product->machine_downloadable = get_downloadable($product->ID);
$hq_data  = get_hq_data($product->sku);

// $product->reviews = get_product_reviews($product->ID);

/**
 * review stats
 */
// $total_reviews = 0;
// $review_counts = [5=>0,4=>0,3=>0,2=>0,1=>0];
// foreach ( $product->reviews as $review ) :
//     if ( isset($review->rating) && isset($review->rating[0])) :
//         $rating = (int)$review->rating[0];
//         $review_counts[$rating] += 1;
//         $total_reviews++;
//     endif;
// endforeach;

$comments = get_comments(['post_id' => $product->ID]);
$star5_review_percent = 0;
$star5_review_percent = 0;

$star4_review_percent = 0;
$star4_review_percent = 0;

$star3_review_percent = 0;
$star3_review_percent = 0;

$star2_review_percent = 0;
$star2_review_percent = 0;

$star1_review_percent = 0;
$star1_review_percent = 0;

$average = 0;

$total_comments = $star_1 = $star_2 = $star_3 = $star_4 = $star_5 = 0;

if ($comments) {
    $review_reply = [];
    foreach ($comments as $key => $comment) {
        //review reply
        if ($comment->comment_parent != 0) {
            $review_reply[$comment->comment_parent][$key]['comment_content'] = $comment->comment_content;
            $review_reply[$comment->comment_parent][$key]['comment_author'] = $comment->comment_author;
            $review_reply[$comment->comment_parent][$key]['comment_date'] = $comment->comment_date;
        }

        if ($comment->comment_approved == 1 && $comment->comment_parent == 0) {
            $ratings = get_comment_meta($comment->comment_ID);
            $rating = $ratings['rating'][0];
            switch ($rating) {
                case '1':
                    $star_1++;
                    break;
                case '2':
                    $star_2++;
                    break;
                case '3':
                    $star_3++;
                    break;
                case '4':
                    $star_4++;
                    break;
                case '5':
                    $star_5++;
                    break;
            }

            $total_comments++;
        }
    }
    if ($total_comments > 0) {
        $star5_review_percent = $star_5 / $total_comments * 100;
        $star5_review_percent = floor($star5_review_percent);

        $star4_review_percent = $star_4 / $total_comments * 100;
        $star4_review_percent = floor($star4_review_percent);

        $star3_review_percent = $star_3 / $total_comments * 100;
        $star3_review_percent = floor($star3_review_percent);

        $star2_review_percent = $star_2 / $total_comments * 100;
        $star2_review_percent = floor($star2_review_percent);

        $star1_review_percent = $star_1 / $total_comments * 100;
        $star1_review_percent = floor($star1_review_percent);

        $average = $star5_review_percent + $star4_review_percent;
    }
}

/**
 * get the product review average
 */
// $review_average = 0;
// foreach ( $review_counts as $rating => $count ) :
//     $review_average += ($rating * $count);
// endforeach;
// $review_average = $review_average && $total_reviews ? $review_average / $total_reviews : 0;

if (is_user_logged_in()) {
    //get user my machines
    $user_id = get_current_user_id();
    $key = 'my_machines';
    $single = true;
    $my_machines = get_user_meta($user_id, $key, $single);
    $is_owned = 'no';
    if ($my_machines) {
        $is_owned = (array_search($product->ID, array_column($my_machines, 'product_id')) === false) ? 'no' : 'yes';
    }

}

global $scripts;
$scripts[] = 'js/libs/rangeslider.min.js';
$scripts[] = 'js/components/rates.js';
$scripts[] = 'js/components/productlist.js';
$scripts[] = 'js/components/machine-page.js';

?>

<?php get_header();?>

<!-- content -->

<?php get_template_part('components/generics/qty-selector');?>

<main id="content">

    <div class="product-page product-page--machine" id="JumpContent" tabindex="-1">

        <div id="product__header" class="product__header product__header--light">
	        <div class="back-container">
		        <a href="/machine-list" title="All Machines" class="btn-back link link--left">Back to all machines</a>
	        </div>
            <?=get_nespresso_show_off($product->label_off,'pro-detail-off');?>
	        <div class="product__slider">
		        <a href="#" class="product__slider__close"></a>
		        <ul class="product__slider__list">
                    <?php if (isset($product->variations) && $product->variations): ?>
                        <?php

                            $active = false;
                            foreach ($product->variations as $variant):
                                $image = wp_get_attachment_image_src($variant['image_id'], 'single-post-thumbnail');
                                ?>
						                                <li class="<?=!$active ? 'active' : '';?>" data-image-id="<?=$variant['image_id'];?>">
						                                    <img src="<?=$image[0];?>" alt="<?php $product->name;?>" draggable="false"/>
						                                </li>
						                        <?php

                                $active = true;
                            endforeach;
                            ?>
                    <?php else: ?>
                        <li class="active">
                            <img src="<?=$product->image[0];?>" alt="<?php $product->name;?>" draggable="false"/>
                        </li>
                    <?php endif;?>
		        </ul>
	        </div>
	        <ol class="product__slider__dots">
                <?php if ($product->variations): ?>
                    <?php

$active = false;
foreach ($product->variations as $variant):
    $image = wp_get_attachment_image_src($variant['image_id'], 'single-post-thumbnail');
    ?>
						                            <li>
						                                <a href="javascript:void(0)" class="<?=!$active ? 'active' : '';?>"></a>
						                                    <div class="product__slider__dots__tooltip" style="background-image: url('<?=$image[0];?>')">
						                                </div>
						                            </li>
						                    <?php

    $active = true;
endforeach;
?>
                <?php else: ?>
                    <li>
                        <a href="javascript:void(0)" class="<?=$active ? 'active' : '';?>"></a>
                            <div class="product__slider__dots__tooltip" style="background-image: url('<?=$product->image[0];?>')">
                        </div>
                    </li>
                <?php endif;?>
	        </ol>

	        <div class="product__info">
		        <div>
			        <div class="product__info__type">Original</div>
			        <h2 class="product__info__title"><?=strtoupper($product->name);?></h2>
			      <!--   <div class="rate-widget">
                        <?php $i = 1;foreach ($review_counts as $rating => $count): ?>
    				        <div class="star-<?=$i;?> ratings-stars fa <?=$rating < $i && !$count ? '' : 'ratings-vote';?>" data-test="<?=round($count);?>"></div>
                        <?php $i++;endforeach;?>
				        <div class="total-votes"><?=$total_reviews;?></div>
			        </div> -->

                    <?php if ($product->variations): ?>
    			        <div class="product__info__colors">
    				        <div class="product__info__colours__title">Colours</div>
    				        <ul class="product__info__colours__list">
    				        	<?php

$active = false;
foreach ($product->variations as $variant):
?>
        						        <li>
        							        <a href="javascript:void(0)"
                                                title="<?=$variant['attributes']['attribute_pa_color'];?>"
                                                class="<?=!$active ? 'active' : '';?>"
                                                style="border: 1px solid #DDD; background: <?=strtolower($variant['attributes']['attribute_pa_color']);?>"
                                                data-image-id="<?=$variant['image_id'];?>"
                                                data-variation-id="<?=$variant['variation_id'];?>"
                                                data-variation-price-html='<?=wc_price($variant['display_price']);?>'
                                            ></a>
        						        </li>
    					        <?php

$active = true;
endforeach;
?>
    				        </ul>
    			        </div>
                    <?php endif;?>

                    <?php if ($product->price): ?>
                        <span class="product__info__price uppercase"><?= wc_price($product->price) ?>
                        <?php if($product->regular_price>$product->price){ ?>
							<span class="product-price--old" style="color:gray;text-decoration: line-through;display:block"><?=wc_price($product->regular_price) ?></span>
						<?php  } ?>
						</span>
                    <?php endif; ?>
                    
			        <div class="container-btn-custom">
                        <div id="qty-selector-content" class=""></div>
                        <?php if ($product->price):
                            $class = "btn-green";
                            $label = "Add to basket";
                        else:
                            $class = "btn-disabled";
                            $label = "Out of stock";
                        endif; ?>
				        <a href="#" class="btn-custom-add-to-cart btn btn-icon <?= $class ?> btn-block btn-icon-right btn-large"
                            data-id="<?= $product->product_id ?>"
                            data-hqid="<?= isset($hq_data['id']) ? $hq_data['id'] : $product->product_id ?>"
                            data-range="<?= isset($hq_data['product_range']) ? $hq_data['product_range'] : '' ?>"
                            data-sku="<?= $product->sku ?>"
                            data-type="<?=$product->product_type;?>"
                            data-name="<?=$product->name;?>"
                            data-price="<?=$product->price;?>"
                            data-image-url="<?=$product->image ? $product->image[0] : '';?>"
                            data-picture="<?=$product->image ? $product->image[0] : '';?>"
                            data-qty-step="1"
                            data-vat="1"
                            data-page="machine"
                        >
					        <i class="icon-basket"></i><span class="btn-add-qty"></span><span><?= $label ?></span><i class="icon-plus"></i>
				        </a>
                        <!-- <div  class="mobi-select-cont" >
                                <select class="mobi-sec" style="width: 100%;height: 100%;"
                                data-id="<?= $product->product_id ?>"
                            data-cart="true"
                            data-type="<?= $product->product_type ?>"
                            data-name="<?= $product->name ?>"
                            data-price="<?= $product->price ?>"
                            data-image-url="<?= $product->image ? $product->image[0] : '' ?>"
                            data-picture="<?= $product->image ? $product->image[0] : '' ?>"
                            data-qty-step="1"
                            data-vat="1"
                                >

                                    <?php
                                    $c=0;
                                        while ( $c<= 100) {
                                           echo "<option value='$c'>$c</option>";
                                           $c+=1;
                                        }
                                     ?>
                                </select>
                            </div> -->
			        </div>
                    <p style="margin:20px 0px 0px 0px">
						<i class="icon-Delivery_off"></i>
						<span>Get Free delivery on all orders above ₫1,500,000</span>
					</p>
		        </div>
	        </div>
        </div>

        <!-- Tab switchers -->
        <div class="tab-switcher tab-switcher--grey clearfix">
            <div class="container">
                <div class="col-xs-6 col-sm-4 no-padding">
                    <a href="#mp-description" title="Description" class="tab-switcher__item active">
                        <div class="outer">
                            <div class="inner">
                                Description
                            </div>
                        </div>
                    </a>
                </div>
                <!-- <div class="col-xs-6 col-sm-4 no-padding">
                    <a href="#mp-reviews" title="Reviews" class="tab-switcher__item">
                        <div class="outer">
                            <div class="inner">
                                Reviews(<?=$total_comments;?>)
                            </div>
                        </div>
                    </a>
                </div> -->
            </div>
        </div>

        <div id="mp-description" class="productdetail-panel active">

            <div class="product-content">
				<div class="container">
                    <?php if (isset($product->description)): ?>
                        <div class="product-content__text"><?=$product->description;?>
                            <?php if ($product->machine_downloadable):?>
                                <p><a href="<?= $product->machine_downloadable ?>" download>Click here to download user manual.</a></p>
                                <br><br>
                            <?php endif;?>
                            <?php if ($product->embedded_machine_video) {
    echo $product->embedded_machine_video;
}
?>
                        </div>
                    <?php endif;?>

                    <!-- <div class="pd-top-10 pd-bottom-20 mr-bottom-20">
                        <a href="#" class="link-btn pull-left" title="Discover the range">Discover the range <span class="fa icon-Fleche"></span></a>
                    </div> -->

                    <?php if (isset($product->specification)): ?>
                        <div>
                            <h3>Specifications</h3>
                            <?=$product->specification;?>
                        </div>
                    <?php endif;?>

                    <?php /*<div class="product-features">
<h3>KEY FEATURES</h3>
<div class="product-feature">
<i class="icon icon-Fast"></i> Fast
</div><!--
--><div class="product-feature">
<i class="icon icon-Eco_friendly"></i> Eco-friendly
</div><!--
--><div class="product-feature">
<i class="icon icon-Fast_heat_up_25s"></i> Fast heat-up
</div><!--
--><div class="product-feature">
<i class="icon icon-cup_size_espresso-lungo"></i> Coffee size<br> Espresso & Lungo
</div><!--
--><div class="product-feature">
<i class="icon icon-Automatic_off_mode"></i> Auto OFF
</div><!--
--><div class="product-feature">
<i class="icon icon-Coffee_capsule_off"></i> Original
</div>
</div> */;?>
					<?php get_template_part('components/generics/social-links');?>
				</div>
            </div><!-- ."product-content -->

            <?php if (isset($product->recommended_products) && $product->recommended_products): ?>
                <div class="recommended-grey">
    	            <div class="container">
    		            <h4>RECOMMENDED PRODUCTS</h4>
    		            <?php include 'wp-content/themes/storefront-child/components/generics/recommended-products.php';?>
    	            </div>
                </div>
            <?php endif;?>

        </div>
        <div id="mp-reviews" class="productdetail-panel">

            <div class="review-header">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-8 review-header__l">
                            <h3>Share your experience with us</h3>
                            <p>
                                Read our Club Member ratings or write your own review. Your feedback is
                                important in helping us improve and innovate. If you have any
                                questions please contact <i>Nespresso Club Vietnam</i> at (01) 234-5678.
                            </p>
                            <?php if (is_user_logged_in()): ?>
                                <button class="btn btn-primary btn-white" data-owned="<?=$is_owned;?>" id="write-review" title="Write a review">Write a review</button>
                            <?php endif;?>
                        </div>
                        <div class="col-xs-12 col-md-4 review-header__r">
                            <div class="review-stats">
                                <div class="review-stats__item review-stats-item">
                                    <span class="review-stats-item__stars">5 <i class="fa fa-star"></i></span>
                                    <span class="review-stats-item__bar review-stats-item__bar--<?=$star5_review_percent;?>"></span>
                                    <span class="review-stats-item__count"><?=$star_5;?></span>
                                </div>
                                <div class="review-stats__item review-stats-item">
                                    <span class="review-stats-item__stars">4 <i class="fa fa-star"></i></span>
                                    <span class="review-stats-item__bar review-stats-item__bar--<?=$star4_review_percent;?>"></span>
                                    <span class="review-stats-item__count"><?=$star_4;?></span>
                                </div>
                                <div class="review-stats__item review-stats-item">
                                    <span class="review-stats-item__stars">3 <i class="fa fa-star"></i></span>
                                    <span class="review-stats-item__bar review-stats-item__bar--<?=$star3_review_percent;?>"></span>
                                    <span class="review-stats-item__count"><?=$star_3;?></span>
                                </div>
                                <div class="review-stats__item review-stats-item">
                                    <span class="review-stats-item__stars">2 <i class="fa fa-star"></i></span>
                                    <span class="review-stats-item__bar review-stats-item__bar--<?=$star2_review_percent;?>"></span>
                                    <span class="review-stats-item__count"><?=$star_2;?></span>
                                </div>
                                <div class="review-stats__item review-stats-item">
                                    <span class="review-stats-item__stars">1 <i class="fa fa-star"></i></span>
                                    <span class="review-stats-item__bar review-stats-item__bar--<?=$star1_review_percent;?>"></span>
                                    <span class="review-stats-item__count"><?=$star_1;?></span>
                                </div>
                            </div>
                            <?php if ($total_comments > 0): ?>
                            <p class="rates__summary"><strong><?=$average;?> <sup>%</sup></strong> of <i>Nespresso Club Members</i> recommend this product</p>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="review-content">
                <div class="container">
                    <div id="review_form_wrapper" class="hide">
                        <div id="review_form">
                            <div id="respond" class="comment-respond">
                                <form action="/wp-comments-post.php" method="post" id="commentform" class="comment-form" novalidate="">
                                    <div class="comment-form-rating">
                                        <label for="rating" style="color: black">Your rating</label>
                                        <br>
                                        <select name="rating" id="rating" aria-required="true" required="" style="display: none;">
                                            <option value="">Rate…</option>
                                            <option value="5">Perfect</option>
                                            <option value="4">Good</option>
                                            <option value="3">Average</option>
                                            <option value="2">Not that bad</option>
                                            <option value="1">Very poor</option>
                                        </select>
                                    </div>
                                    <p class="comment-form-comment">
                                        <label for="comment" style="color: black">Your review <span class="required">*</span></label>
                                        <textarea style="color: black" id="comment" name="comment" cols="45" rows="8" aria-required="true" required=""></textarea>
                                    </p>
                                    <p class="form-submit">
                                        <input name="submit" id="submit" class="submit" value="Submit" type="submit">
                                        <input name="comment_post_ID" value="<?=$product->ID;?>" id="comment_post_ID" type="hidden">
                                        <input name="comment_parent" id="comment_parent" value="0" type="hidden">
                                        <input id="cancel-comment" class="pull-right" style="background: #f5f5f5 !important; border: 1px solid #ddd; !important; color: #333" id="cancel-comment" value="Cancel" type="button">
                                    </p>
                                    <?=wp_nonce_field('unfiltered-html-comment_' . $product->ID, '_wp_unfiltered_html_comment_disabled', false);?>
                                    <script>
                                        (function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();
                                    </script>
                                </form>
                            </div><!-- #respond -->
                        </div>
                    </div>
                    <?php
if ($comments && $total_comments > 0):
    foreach ($comments as $key => $comment):
        $ratings = get_comment_meta($comment->comment_ID);
        $rating = $ratings['rating'][0];

        if ($comment->comment_approved == 1 && $comment->comment_parent == 0): ?>
												                                    <div class="review-content__item review-content-item">
												                                        <div class="review-content-item__header clearfix">
												                                            <div class="rate-widget pull-left">
												                                                <div class="star-1 ratings-stars fa <?=$rating >= 1 ? 'ratings-vote' : '';?>"></div>
												                                                <div class="star-2 ratings-stars fa <?=$rating >= 2 ? 'ratings-vote' : '';?>"></div>
												                                                <div class="star-3 ratings-stars fa <?=$rating >= 3 ? 'ratings-vote' : '';?>"></div>
												                                                <div class="star-4 ratings-stars fa <?=$rating >= 4 ? 'ratings-vote' : '';?>"></div>
												                                                <div class="star-5 ratings-stars fa <?=$rating >= 5 ? 'ratings-vote' : '';?>"></div>
												                                            </div>
												                                            <div class="pull-left">Posted by <?=$comment->comment_author;?> / <?=date('m/d/Y', strtotime($comment->comment_date));?></div>
												                                        </div>
												                                        <div class="review-content-item__title" style="padding-top: 0px;"></div>
												                                        <div class="review-content-item__content">
												                                            <?=$comment->comment_content;?>
												                                        </div>
												                                        <?php if (isset($review_reply[$comment->comment_ID])):
            foreach ($review_reply[$comment->comment_ID] as $reply): ?>
																		                                            <div class="review-content-item__content review-content-item__content--response">
																		                                                <span class="posted"><i class="icon icon-Nespresso_Monogram"></i> Posted by <i>Nespresso</i> Club <?=date('m/d/Y', strtotime($reply['comment_date']));?></span>
																		                                                <?=$reply['comment_content'];?>
																		                                            </div>
																		                                        <?php

        endforeach;
    endif;?>
						                                    </div>
						                                <?php endif;
endforeach;

else:
    echo '<p style="color: black">There are no reviews yet.</p>';
endif;
?>
                </div>
            </div>
        </div>

    </div>

</main>

<!-- /content -->

<!-- get_footer -->
<?php get_footer();?>
