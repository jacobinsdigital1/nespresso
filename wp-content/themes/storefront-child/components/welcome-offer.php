<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

//redirect to login if user is not logged in
if( !is_user_logged_in() )
wp_redirect('/login');
?>

<!-- content -->
<?php
$discover_products  = get_discovery_products();
$bought             = false;
$already_in_cart    = false;
$user               = wp_get_current_user();

foreach ($discover_products as $key => $value) {

  if (wc_customer_bought_product($user->user_email, $user->ID, $value->post_id)) {
    $bought = true;
    break;
  }

  foreach( WC()->cart->get_cart() as $key => $cart_item ){
      if ($value->post_id == $cart_item['product_id']) {
          $already_in_cart =  true;
          break;
      }
  }

}

if ($bought || $already_in_cart) {
  wp_redirect('/coffee-list');
} else {
  get_template_part('components/welcome-offer-products');
}

?>
