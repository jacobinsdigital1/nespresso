<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://minionsolutions.com
 * @since      1.0.0
 *
 * @package    Axapta_Api
 * @subpackage Axapta_Api/admin
 */

/**
 * option name for the axapta settings
 */
global $axapta_settings_option_name;
$axapta_settings_option_name = 'axapta_settings_option_name';


/**
 * save axapta best sellers as an wp option
 *
 * @param  array  $post - post data
 * @return array
 */
function save_axapta_settings(array $post=[]) {

    if (!$post || empty($post) )
        return [];

    global $axapta_settings_option_name;

    $axapta_settings['uri'] = ( isset($post['uri']) ? $post['uri'] : '');
	$axapta_settings['port'] = ( isset($post['port']) ? $post['port'] : '');
    $axapta_settings['consumer_key'] = ( isset($post['consumer_key']) ? $post['consumer_key'] : '');
    $axapta_settings['consumer_secret'] = ( isset($post['consumer_secret']) ? $post['consumer_secret'] : '');
    // $axapta_settings['userid'] = ( isset($post['userid']) ? $post['userid'] : '');
    // $axapta_settings['storeid'] = ( isset($post['storeid']) ? $post['storeid'] : '');
    // $axapta_settings['signature'] = ( isset($post['signature']) ? $post['signature'] : '');


    try {
        update_option($axapta_settings_option_name, $axapta_settings);
        return $axapta_settings;
    } catch (Exception $e) {
        return [];
    }

} //save_axapta_settings()

/**
 * get the axapta settings
 *
 * @return array|false
 */
function get_axapta_settings() {

    global $axapta_settings_option_name;

    $axapta_settings = get_option($axapta_settings_option_name);

    if ( !$axapta_settings )
        return [];

    return $axapta_settings;

} // get_axapta_settings()







