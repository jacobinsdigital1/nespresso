<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

$products = get_coffee_products();
$arrays = get_nespresso_product_list();

if (!$arrays) {
    $categories = custom_acf_get_data('Product Details', 'category');
}else{
    $categories['choices'] = $arrays['coffee'];

    //remove other category (temporary solution)
	// $remove_category = ['Master Origin', 'Limited Edition', 'Discovery Offer', 'Variations', 'Pure Origin'];
	// foreach ($remove_category as $value) {
	// 	$index = array_search($value,$categories['choices']);
	// 	unset($categories['choices'][$index]);
	// }
	foreach ($categories['choices'] as $key => $value) {
		//check if category has product
		if (!get_coffee_category_with_product('category', $value)) {
			unset($categories['choices'][$key]);
		}
	}
}

$intensities = [3,4,5,6,8,9,10,11,12];
$aromatic_notes = [
	'Fruity' => [
		'Fruity Winy', 'Flowery', 'Citrus'
	],
	'Balanced' => [
		'Biscuits', 'Cereals', 'Roasted', 'Honey'
	],
	'Intense' => [
		'Spicy', 'Malted', 'Woody', 'Cocoa', 'Intensely Roasted'
	]
];

foreach ($aromatic_notes['Fruity'] as $key => $value) {
	if (!get_coffee_category_with_product('aromatic_notes', $value)) {
		unset($aromatic_notes['Fruity'][$key]);
	}
}

foreach ($aromatic_notes['Balanced'] as $key => $value) {
	if (!get_coffee_category_with_product('aromatic_notes', $value)) {
		unset($aromatic_notes['Balanced'][$key]);
	}
}

foreach ($aromatic_notes['Intense'] as $key => $value) {
	if (!get_coffee_category_with_product('aromatic_notes', $value)) {
		unset($aromatic_notes['Intense'][$key]);
	}
}

foreach ($intensities as $key => $value) {
	//check if category has product
	if (!get_coffee_category_with_product('intensity', $value)) {
		unset($intensities[$key]);
	}
}

global $scripts;
$scripts[] = 'js/components/coffee-range.js';

?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/coffee-origins.css">
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/coffee-range.css">
<?php get_template_part('components/generics/qty-selector'); ?>
<main id="main" class="fullPage fullPage__plp coffee-list desktop">
	<div class="vue v_coffeeOrigins">
		<section id="introduction" class="vue_introduction v_parallax" pv-height="auto" id="introduction" data-label="Introduction" style="height: 575px;">
			<div class="bg_container v_parallaxLayer">
				<div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/introduction_placeholder_L.jpg');"></div>
				<div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/introduction_XL.jpg');" lazy="loaded"></div>
			</div>
			<div class="v_sectionRestrict">
				<div class="v_sectionContent">
					<header>
						<div class="v_cell">
							<div class="v_menu v_menuDropdown">
								<p class="v_visually_hidden">Navigation to other resources</p>
								<div class="v_subMenu">
									<div class="v_subMenuCell">
										<ul>
											<li>
												<a class="v_active" href="#"><span>Discover Our Coffee Range</span></a>
											</li>
											<li>
												<a class="" href="/coffee-origins"><span>Coffee Origins</span></a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</header>
					<article>
						<div class="v_articleContent">
							<h2 data-wow="" class="wow">An Authentic Coffee Experience</h2>
							<div class="v_wysiwyg wow" data-wow="" data-wow-delay="1">
								<p>Discover <strong class="v_brand" term="nespresso">Nespresso</strong>, where quality coffee is delivered at the touch of a button. Savor our coffee varieties from three gourmet aromatic profiles: intense, balanced, and fruity / flowery.</p>
							</div>
							<div class="v_buttonContainer wow" data-wow="" data-wow-delay="2">
								<a href="#filters" class="v_btnRoundM" style="opacity: 0.5;">
									<i class="fn_angleDownCircle"></i>
									<span class="v_visually_hidden">Go to the Origins section</span>
								</a>
							</div>
						</div>
					</article>
				</div>
			</div>
		</section>
		<!-- Filter Section -->
		<section class="vue_filters v_sectionLight" id="filters" data-label="Filters">
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent v_sectionTabs"> <span class="v_visually_hidden">You are in a tab selection : choose a filter below</span>
		            <ul role="tablist" class="coff-range-tab">
		                <li class="v-link v-link-range v-link-active">
		                    <h2 class="v_tab"> <a class="v_tabLink" role="tab" data-filter="range" aria-selected="true">Filter by <strong>range</strong></a> </h2> </li>
		                <li  class="v-link v-link-intensity">
		                    <h2 class="v_tab"> <a class="v_tabLink" role="tab" data-filter="intensity" aria-selected="false">Filter by <strong>intensity</strong></a> </h2> </li>
		                <li  class="v-link v-link-aromatic-profile">
		                    <h2 class="v_tab"> <a class="v_tabLink" role="tab" data-filter="aromatic profile" aria-selected="false">Filter by <strong>aromatic profile</strong></a> </h2> </li>
		            </ul>
		            <div class="v_filters">
		                <div class="v_tableCell">
		                    <button class="v_more" aria-expanded="false">Add filters</button>
		                </div>
		                <div class="v_tableCell">
		                    <button class="v_reset">Reset filters</button>
		                </div>
		            </div>
		            <div class="v_filterDetails">
		                <div class="v_filterIntensity">
		                    <div class="v_filterAlign"> <strong>Intensity</strong>
		                        <ul>
		                            <li>
		                                <input id="intensity_1" value="1" disabled="" type="checkbox">
		                                <label for="intensity_1"> <span>1</span> </label>
		                            </li><li>
		                                <input id="intensity_1" value="2" disabled="" type="checkbox">
		                                <label for="intensity_2"> <span>2</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_3" value="3" type="checkbox">
		                                <label for="intensity_3"> <span>3</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_4" value="4" type="checkbox">
		                                <label for="intensity_4"> <span>4</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_5" value="5" type="checkbox">
		                                <label for="intensity_5"> <span>5</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_6" value="6" type="checkbox">
		                                <label for="intensity_6"> <span>6</span> </label>
		                            </li><li>
		                                <input id="intensity_7" value="7" disabled="" type="checkbox">
		                                <label for="intensity_7"> <span>7</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_8" value="8" type="checkbox">
		                                <label for="intensity_8"> <span>8</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_9" value="9" type="checkbox">
		                                <label for="intensity_9"> <span>9</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_10" value="10" type="checkbox">
		                                <label for="intensity_10"> <span>10</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_11" value="11" type="checkbox">
		                                <label for="intensity_11"> <span>11</span> </label>
		                            </li><li>
		                                <input class="range-filter intensity_filter intensity-filter" id="intensity_12" value="12" type="checkbox">
		                                <label for="intensity_12"> <span>12</span> </label>
		                            </li><li>
		                                <input id="intensity_13" value="13" disabled="" type="checkbox">
		                                <label for="intensity_13"> <span>13</span> </label>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		                <div class="v_filterCupsizes">
		                    <div class="v_filterAlign"> <strong>Cup sizes</strong>
		                        <ul>
		                            <li>
		                                <input class="range-filter cupSize_filter cupSize-filter cupSize_1" id="cupSize_ristretto" value="1" type="checkbox">
		                                <label for="cupSize_ristretto" title="25 ml"> <span class="v_visually_hidden">Ristretto</span> </label>
		                            </li><li>
		                                <input class="range-filter cupSize_filter cupSize-filter cupSize_2" id="cupSize_espresso" value="2" type="checkbox">
		                                <label for="cupSize_espresso" title="40 ml"> <span class="v_visually_hidden">Espresso</span> </label>
		                            </li><li>
		                                <input class="range-filter cupSize_filter cupSize-filter cupSize_3" id="cupSize_lungo" value="3" type="checkbox">
		                                <label for="cupSize_lungo" title="110 ml"> <span class="v_visually_hidden">Lungo</span> </label>
		                            </li><li>
		                                <input id="cupSize_americano" value="americano" type="checkbox" disabled="">
		                                <label for="cupSize_americano" title="150 ml"> <span class="v_visually_hidden">Americano</span> </label>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		                <div class="v_filterAromaticProfiles">
		                    <div class="v_filterAlign"> <strong>Aromatic profiles</strong>
		                        <ul>
		                            <li>
		                                <input class="range-filter aromaticProfile_filter aromaticProfile-filter" id="aromaticProfile_fruity" value="fruity" type="checkbox">
		                                <label for="aromaticProfile_fruity">Fruity</label>
		                            </li><li>
		                                <input class="range-filter aromaticProfile_filter aromaticProfile-filter" id="aromaticProfile_balanced" value="balanced" type="checkbox">
		                                <label for="aromaticProfile_balanced">Balanced</label>
		                            </li><li>
		                                <input class="range-filter aromaticProfile_filter aromaticProfile-filter" id="aromaticProfile_intense" value="intense" type="checkbox">
		                                <label for="aromaticProfile_intense">Intense</label>
		                            </li>
		                        </ul>
		                    </div>
		                </div>
		                <div class="v_filterRange hide">
						    <div class="v_filterAlign"> <strong aria-hidden="true">Range</strong>
						        <label for="ranges" class="v_visually_hidden">Choose your range</label>
						        <select class="range_filter select-range-filter" id="ranges" name="select" role="listbox" aria-activedescendant="range_default">
						            <option value="default">Unset</option>
						            <option role="option" value="intenso" id="range_intenso" aria-selected="false">Intenso</option>
						            <option role="option" value="espresso" id="range_espresso" aria-selected="false">Espresso</option>
						            <option role="option" value="pure-origin" id="range_pure-origin" aria-selected="false">Pure Origin</option>
						            <option role="option" value="lungo" id="range_lungo" aria-selected="false">Lungo</option>
						            <option role="option" value="decaffeinato" id="range_decaffeinato" aria-selected="false">Decaffeinato</option>
						            <option role="option" value="variations" id="range_variations" aria-selected="false">Variations</option>
						        </select>
						        <div class="v_iconRanges"></div>
						    </div>
						</div>
		            </div>
		        </div>
		    </div>
		</section>
		<!-- END Filter Section -->

		<!-- Range Section -->
		<section class="vue_coffeeSelector v_sectionLight" id="coffee-selector-range" data-label="Coffee selector">
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_coffeeSelector"> <span class="v_visually_hidden">Choose a range</span>
		                <ul class="v_range">
			                <?php foreach ($categories['choices'] as $key => $categ):?>
			                	<?php if($categ != 'Discovery Offer'): ?>
			                		<li class="v_hidable">
				                        <a class="v_title v_range_filter v-range-filter-<?=  strtolower(str_replace(' ','-',$categ)) ?>" data-filter="<?=  strtolower(str_replace(' ','-',$categ)) ?>" aria-selected="false">
				                            <h3><?= $categ ?></h3> </a> <span class="v_visually_hidden">Choose a coffee</span>
				                        <div class="v_mobileSlider">
				                            <ul class="v_coffees">
				                            	<?php foreach ($products as $category => $product): ?>
				                            		<?php if ($categ === $category): ?>
				                            			<?php foreach ($product as $k => $product): ?>
				                            				<li
				                            					<?php
				                            					$class_size_of_cup = '';
				                            					if(is_array($product->size_of_cup)):
											                        foreach ($product->size_of_cup as $size_of_cup):
											                        	$class_size_of_cup .= " v-cupsize-" . $size_of_cup;
											             			endforeach;
												                else:
												                	$class_size_of_cup .= " v-cupsize-" . $product->size_of_cup;
												                endif; ?>

												                <?php
				                            					if(is_array($product->size_of_cup)):
											                        $cupSize = implode(",", $product->size_of_cup);
											                    else:
											                       $cupSize = $product->size_of_cup;
											                    endif;?>

				                            					class="v-capsules  v-capsules-active <?= $class_size_of_cup ?> v-capsule-category-<?= strtolower(str_replace(' ','-',$categ))?> v-intensity-<?= $product->intensity ?> v-aromatic-profile-<?= strtolower($product->aromatic_profile) ?>"
												                data-cupsize="<?= $cupSize?>"
												                data-intensity="<?= $product->intensity?>"
				                            					data-aromaticprofile="<?= strtolower($product->aromatic_profile)?>"
				                            					data-category="<?=  strtolower(str_replace(' ','-',$categ)) ?>"

				                            				>
							                                    <a href="#range-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>" aria-expanded="false" class="capsules_description" data-capsule="<?= strtolower(str_replace(' ','-',$product->name)) ?>">
							                                        <div data-product-item-id="<?= $product->post_id ?>">
							                                            <h4 class="v_titleCoffee"><strong class="v_brand" term="<?= $product->name?>"><?= ucwords(strtolower($product->aromatic_profile))?></strong></h4>
							                                            <div class="v_caps">
							                                                <div class="v_capsPlaceholder"></div> <img alt="" src="<?= $product->image[0] ?>" lazy="loaded"> </div> <span class="v_intensity"><?= $product->intensity ?></span> </div>
							                                    </a>
							                                    <div class="v_coffeeDescription v_coffeeDescription_<?= strtolower(str_replace(' ','-',$product->name)) ?>">
							                                        <div class="m_circle m_circle_<?= strtolower(str_replace(' ','-',$product->name)) ?> <?= strtolower(str_replace(' ','-',$product->name)) ?>"></div>
							                                        <div id="range-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>" class="v_slider hide capsule-description capsule-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>">
																	    <div class="v_overflow" style="height: 646px;">
																	        <div class="v_padding">
																	            <a class="v_btnRoundSM v_btnCloseFixed close-modal" data-capsule="<?= strtolower(str_replace(' ','-',$product->name)) ?>"> <i class="fn_close"></i> <span>Close panel</span> </a>
																	            <div class="v_row">
																	                <div class="v_col33"> <img alt="" src="<?= $product->product_view_image['url'] ?>" lazy="loaded">  <!-- <img alt="" src="/ecom/medias/sys_master/public/10252256870430.png" lazy="loaded"> --> </div>
																	                <div class="v_col66">
																	                    <h5><strong class="v_brand" term="<?= $product->post_id?>"><?=$product->name?></strong></h5>
																	                    <div class="mobile-other">
																	                    	<p class=""><?= wc_price($product->price)?> </p>
																		                    <div class="mobile-add-to-cart">
																		                    	<button class="btn btn-icon btn-block btn-green btn-icon-right btn-coffee-origin"
																						                data-id="<?=$product->post_id?>"
																						                data-cart="true"
																						                data-name="<?=$product->name?>"
																						                data-price="<?=$product->price?>"
																						                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
																						                data-type="<?=$product->product_type?>"
																						                data-vat="1"
																						                data-qty-step="10"
																						                data-url="<?= $product->url ?>"
																						                data-aromatic-profile="<?= $product->aromatic_profile?>"
																						            >

																						            <i class="icon-Visit_on"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
																						        </button>
																		                    </div>
																	                    </div>
																	                    <p class="v_wysiwyg">
																	                    <?= substr(strip_tags($product->description, '<p>'), 0, 300) . ' . . . ' ?> <br>
																	                    <a class="v_link v_iconLeft" href="<?= $product->url ?>"> <i class="fn_arrowLink"></i> <span>Read more</span> </a>
																	                    </p>
																	                    <div class="v_row">
																						    <ul class="v_inlineTable">
																						        <li class="v_features"> <strong>Intensity</strong>
																						            <div class="v_intensityLevel">
																						                <ul class="v_inline">
																						                    <li class="v_value"><?= $product->intensity ?></li>
																						                    <?php for ($i=0; $i < $product->intensity; $i++): ?>
																						                    	<li class="v_filled"></li>
																						                    <?php endfor; ?>
																						                </ul>
																						            </div>
																						        </li>
																						        <li class="v_features"> <strong>Cup size</strong>
																						            <div class="v_intensityLevel v_level">
																						                <div class="cupsize">
																						                    <?php if(is_array($product->size_of_cup)):
																						                        foreach ($product->size_of_cup as $key => $value): ?>
																						                            <i class="cupsize__<?=$value?>"></i>
																						                        <?php endforeach; ?>
																						                    <?php else: ?>
																						                        <i class="cupsize__<?=$product->size_of_cup?>"></i>
																						                    <?php endif; ?>

																						                </div>
																						            </div>
																						        </li>
																						        <!-- <li class="v_features"> <strong>Bitterness</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Acidity</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">1</li>
																						                    <li class="v_filled"></li>
																						                    <li></li>
																						                    <li></li>
																						                    <li></li>
																						                    <li></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Body</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Roasting</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						    </ul>
																						    <ul class="v_inlineTable">
																						        <!-- <li class="v_features"> <strong>Cup size</strong> <span>Ristretto 25 ml<br>Espresso 40 ml</span> </li> -->
																						        <li class="v_features"> <strong>Aromatic profile</strong> <span><?=$product->aromatic_profile?></span> </li>
																						        <li class="v_features"> <strong>Aromatic notes</strong> <span><?= $product->property ?></span> </li>
																						    </ul>
																						</div>
																	                    <div class="v_row v_more">
																							<div></div>
																						    <div>
																						        <div class="v_addToCart">
																						            <div class="v_priceAndButton">
																						                <p class="v_productPrice"><?= wc_price($product->price)?> </p>
																						                <div class="product-add pull-right" style="position: relative;">

																									        <button class="btn btn-icon btn-block btn-green btn-icon-right btn-coffee-origin"
																									                data-id="<?=$product->post_id?>"
																									                data-cart="true"
																									                data-name="<?=$product->name?>"
																									                data-price="<?=$product->price?>"
																									                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
																									                data-type="<?=$product->product_type?>"
																									                data-vat="1"
																									                data-qty-step="10"
																									                data-url="<?= $product->url ?>"
																									                data-aromatic-profile="<?= $product->aromatic_profile?>"
																									            >
																									            <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
																									        </button>

																									    </div>
																						            </div>
																						        </div>
																						    </div>
																						</div>
																	                </div>
																	            </div>
																	        </div>
																	    </div>
																	</div>
							                                    </div>

																<!-- <div id="modal-container-<?= strtolower(str_replace(' ','-',$product->name)) ?>" data-capsule="<?= strtolower(str_replace(' ','-',$product->name)) ?>" class="image-modal">
													  				<img title="<?= strtolower(str_replace(' ','-',$product->name)) ?>" class="image-modal-content" id="modal_img-<?= strtolower(str_replace(' ','-',$product->name)) ?>">
													  				</div>
																</div> -->
							                                </li>
				                            			<?php endforeach;?>
				                            		<?php endif; ?>
				                            	<?php endforeach;?>
				                            </ul>
				                        </div>
				                    </li>
			                	<?php endif;?>
			                <?php endforeach;?>
		                </ul>
		            </div>
		        </div>
		    </div>
		</section>
		<!-- End Range Section -->

		<!-- Intensity Section -->
		<section class="vue_coffeeSelector v_sectionLight hide" id="coffee-selector-intensity" data-label="Coffee selector">
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_coffeeSelector"> <span class="v_visually_hidden">Choose an intensity</span>
		                <div class="v_intensityArrow">
		                    <div class="v_arrow"></div>
		                    <p class="">Intense</p>
		                </div>
		                <ul class="v_intensity">
		                	<?php foreach ($intensities as $intensity): ?>
		                		<li class="v_hidable">
			                        <a class="v_title v_intentsity_filter v-intensity-filter-<?=  $intensity ?>" data-filter="<?=  $intensity ?>" aria-selected="false">
			                            <h3><?= $intensity ?></h3> </a> <span class="v_visually_hidden">Choose a coffee</span>
			                        <div class="v_mobileSlider">
			                            <ul class="v_coffees">
			                            	<?php foreach ($products as $category => $product): ?>
			                            		<?php foreach ($product as $k => $product): ?>
				                            		<?php if ($category != 'Discovery Offer'): ?>
				                            			<?php if ($intensity == $product->intensity): ?>
						                            		<li
						                            			<?php
				                            					$class_size_of_cup = '';
				                            					if(is_array($product->size_of_cup)):
											                        foreach ($product->size_of_cup as $size_of_cup):
											                        	$class_size_of_cup .= " v-cupsize-" . $size_of_cup;
											             			endforeach;
												                else:
												                	$class_size_of_cup .= " v-cupsize-" . $size_of_cup;
												                endif; ?>

												                <?php
				                            					if(is_array($product->size_of_cup)):
											                        $cupSize = implode(",", $product->size_of_cup);
											                    else:
											                       $cupSize = $value;
											                    endif;?>

				                            					class="v-capsules  v-capsules-active <?= $class_size_of_cup ?> v-capsule-category-<?= strtolower(str_replace(' ','-',$category))?> v-intensity-<?= $product->intensity ?> v-aromatic-profile-<?= strtolower($product->aromatic_profile) ?>"
												                data-cupsize="<?= $cupSize?>"
												                data-intensity="<?= $product->intensity?>"
				                            					data-aromaticprofile="<?= strtolower($product->aromatic_profile)?>"
				                            					data-category="<?=  strtolower(str_replace(' ','-',$category)) ?>"
						                            		>
							                                    <a href="#intensity-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>" aria-expanded="false" class="capsules_description" data-capsule="<?= strtolower(str_replace(' ','-',$product->name)) ?>">
							                                        <div data-product-item-id="<?= $product->post_id ?>">
							                                            <h4 class="v_titleCoffee"><strong class="v_brand" term="<?= $product->name?>"><?= ucwords(strtolower($product->name))?></strong></h4>
							                                            <div class="v_caps">
							                                                <div class="v_capsPlaceholder"></div> <img alt="" src="<?= $product->image[0] ?>" lazy="loaded"> </div>
							                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21 21" style="enable-background:new 0 0 21 21;" xml:space="preserve" class="injected-svg" aria-hidden="true">
							                                            	<?php if ($product->aromatic_notes && $product->aromatic_notes == 'Flowery'): ?>
							                                            		<g class="aromatic-notes aromatic-flowery">
								                                                    <path d="M10.5,0L10.5,0L10.5,0C9.1,0,8,1.1,8,2.5c0,0.5,0.2,1,0.5,1.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0.6-0.2,1.3-0.3,1.9-0.3c0.7,0,1.3,0.1,1.9,0.3c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0C12.8,3.5,13,3,13,2.5   C13,1.1,11.9,0,10.5,0z M12.1,3.1c-0.5-0.1-1.1-0.2-1.6-0.2C9.9,2.9,9.4,3,8.9,3.1C8.8,2.9,8.7,2.7,8.7,2.5c0-1,0.8-1.7,1.7-1.8   l0,0h0l0,0c1,0,1.7,0.8,1.7,1.8C12.3,2.7,12.2,2.9,12.1,3.1z"></path>
								                                                    <path d="M4.1,12.3C4.1,12.3,4.1,12.3,4.1,12.3c-0.2-0.6-0.3-1.3-0.3-1.9c0-0.6,0.1-1.2,0.2-1.8c0,0,0,0,0,0C3.6,8.3,3.1,8,2.5,8   C1.1,8,0,9.1,0,10.5C0,11.9,1.1,13,2.5,13C3.1,13,3.6,12.7,4.1,12.3z M0.7,10.5c0-1,0.8-1.8,1.8-1.8c0.3,0,0.6,0.1,0.8,0.2   c-0.1,0.5-0.2,1-0.2,1.5c0,0.5,0.1,1.1,0.2,1.6c-0.2,0.1-0.5,0.2-0.8,0.2C1.5,12.3,0.7,11.5,0.7,10.5z"></path>
								                                                    <path d="M4.6,7.2L4.6,7.2c0.6-1.1,1.5-2.1,2.7-2.7c0,0,0,0,0,0C7.2,4,6.9,3.5,6.5,3.1C6.1,2.6,5.4,2.4,4.8,2.4S3.6,2.6,3.1,3.1   c-1,1-1,2.5,0,3.5C3.5,7,4,7.2,4.6,7.2z M3.6,3.6c0.3-0.3,0.8-0.5,1.2-0.5c0.5,0,0.9,0.2,1.2,0.5C6.2,3.7,6.4,4,6.4,4.2   C5.6,4.8,4.8,5.5,4.2,6.5C4,6.4,3.8,6.3,3.6,6.1C3.2,5.7,3.1,5.3,3.1,4.8C3.1,4.3,3.2,3.9,3.6,3.6z"></path>
								                                                    <path d="M12.4,17C12.4,17,12.4,17,12.4,17C12.3,17,12.3,16.9,12.4,17C12.3,16.9,12.3,16.9,12.4,17c-0.6,0.1-1.2,0.2-1.9,0.2   c-0.6,0-1.2-0.1-1.8-0.3c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.4,0.4-0.6,1-0.6,1.6c0,1.4,1.1,2.5,2.5,2.5l0,0l0,0   c1.4,0,2.5-1.1,2.5-2.5C13,17.9,12.7,17.4,12.4,17C12.4,17,12.4,17,12.4,17z M10.5,20.3L10.5,20.3L10.5,20.3c-1,0-1.8-0.8-1.8-1.8   c0-0.3,0.1-0.6,0.2-0.8c0.5,0.1,1,0.2,1.6,0.2c0.5,0,1-0.1,1.5-0.2l0,0l0,0c0.1,0.2,0.2,0.5,0.2,0.8C12.3,19.5,11.5,20.3,10.5,20.3   z"></path>
								                                                    <path d="M4.7,13.7L4.7,13.7c-0.6,0-1.2,0.3-1.6,0.7c-1,1-1,2.5,0,3.5c0.5,0.5,1.1,0.7,1.7,0.7s1.3-0.2,1.7-0.7   c0.4-0.4,0.7-1,0.7-1.6c0,0,0,0,0,0C6.2,15.7,5.3,14.8,4.7,13.7z M6.1,17.4c-0.3,0.3-0.8,0.5-1.2,0.5c-0.5,0-0.9-0.2-1.2-0.5   c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2c0.2-0.2,0.5-0.4,0.7-0.4c0.6,0.9,1.3,1.6,2.2,2.2C6.4,16.9,6.3,17.2,6.1,17.4z"></path>
								                                                    <path d="M18.5,8c-0.6,0-1.1,0.2-1.6,0.6c0,0,0,0,0,0c0.2,0.6,0.2,1.2,0.2,1.8c0,0.7-0.1,1.3-0.3,1.9c0,0,0,0,0,0   c0.4,0.4,1,0.6,1.6,0.6c1.4,0,2.5-1.1,2.5-2.5C21,9.1,19.9,8,18.5,8z M18.5,12.3c-0.3,0-0.6-0.1-0.8-0.2c0.1-0.5,0.2-1.1,0.2-1.6   c0-0.5-0.1-1-0.2-1.5c0.2-0.1,0.5-0.2,0.8-0.2c1,0,1.8,0.8,1.8,1.8C20.3,11.5,19.5,12.3,18.5,12.3z"></path>
								                                                    <path d="M16.3,13.8L16.3,13.8c-0.6,1.1-1.5,2-2.6,2.6c0,0,0,0,0,0c0,0.6,0.3,1.1,0.7,1.6c0.5,0.5,1.1,0.7,1.7,0.7   c0.6,0,1.3-0.2,1.7-0.7c1-1,1-2.5,0-3.5C17.5,14,16.9,13.8,16.3,13.8z M17.4,17.4c-0.3,0.3-0.8,0.5-1.2,0.5s-0.9-0.2-1.2-0.5   c-0.2-0.2-0.4-0.5-0.4-0.7c0.9-0.6,1.6-1.3,2.2-2.2c0.2,0.1,0.5,0.2,0.7,0.4c0.3,0.3,0.5,0.8,0.5,1.2   C17.9,16.7,17.7,17.1,17.4,17.4z"></path>
								                                                    <path d="M16.4,7.2L16.4,7.2C17,7.2,17.5,7,17.9,6.5c1-1,1-2.5,0-3.5c-0.5-0.5-1.1-0.7-1.7-0.7c-0.6,0-1.3,0.2-1.7,0.7   C14,3.5,13.8,4,13.8,4.5c0,0,0,0,0,0C14.9,5.2,15.8,6.1,16.4,7.2z M14.9,3.6c0.3-0.3,0.8-0.5,1.2-0.5c0.5,0,0.9,0.2,1.2,0.5   c0.3,0.3,0.5,0.8,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2c-0.2,0.2-0.4,0.3-0.7,0.4c-0.6-0.9-1.3-1.7-2.2-2.3C14.6,4,14.8,3.7,14.9,3.6z"></path>
								                                                </g>
							                                            	<?php elseif ($product->aromatic_notes && $product->aromatic_notes == 'Citrus'): ?>
							                                            		<g class="aromatic-notes aromatic-citrus">
								                                                    <path d="M11.5,0C5.2,0,0,5.2,0,11.5v1h1h21h1v-1C23,5.2,17.8,0,11.5,0z M1,11.5C1,5.7,5.7,1,11.5,1C17.3,1,22,5.7,22,11.5H1z"></path>
								                                                    <path d="M15.1,3.8l0.1-0.1v0c0-0.1,0.1-0.2,0.1-0.3c0-0.3-0.1-0.5-0.4-0.6c0,0-0.1,0-0.1-0.1c-1-0.4-2.1-0.6-3.2-0.6   c-1.1,0-2.2,0.2-3.2,0.6c0,0-0.1,0-0.1,0C7.9,2.9,7.8,3.2,7.8,3.4c0,0.1,0,0.2,0.1,0.3v0l0.1,0.1l3.6,6.2l0,0l0,0l0,0l0,0l0,0l0,0   L15.1,3.8z"></path>
								                                                    <path d="M7.2,4.3L7.1,4.1l0,0C7,4,7,4,6.9,3.9C6.7,3.8,6.4,3.8,6.2,4C6.2,4,6.1,4,6.1,4C5.2,4.7,4.5,5.6,4,6.5   c-0.6,1-0.9,2-1.1,3.1c0,0,0,0.1,0,0.1c0,0.2,0.1,0.5,0.3,0.6c0.1,0,0.2,0.1,0.3,0.1l0,0l0.2,0l7.1,0l0,0l0,0L7.2,4.3z"></path>
								                                                    <path d="M16.9,4c0,0-0.1-0.1-0.1-0.1c-0.2-0.1-0.5-0.1-0.7,0C16,4,16,4,15.9,4.1l0,0l-0.1,0.1l-3.6,6.2l0,0l0,0l7.1,0l0.2,0l0,0   c0.1,0,0.2,0,0.3-0.1c0.2-0.1,0.3-0.4,0.3-0.6c0,0,0-0.1,0-0.1C20,8.5,19.6,7.5,19,6.5C18.5,5.6,17.8,4.7,16.9,4z"></path>
								                                                </g>
							                                            	<?php elseif ($product->aromatic_notes && $product->aromatic_notes == 'Fruity Winy'): ?>
							                                            		<g class="aromatic-notes aromatic-fruity-winy">
								                                                    <path d="M18,15.8c-1.4,0-2.6,1.2-2.6,2.6S16.6,21,18,21s2.6-1.2,2.6-2.6C20.6,16.9,19.5,15.8,18,15.8z M18,20.3   c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9s1.9,0.9,1.9,1.9S19.1,20.3,18,20.3z"></path>
								                                                    <path d="M11.5,15.8c-1.4,0-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6s2.6-1.2,2.6-2.6C14.1,16.9,13,15.8,11.5,15.8z M11.5,20.3   c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9S12.6,20.3,11.5,20.3z"></path>
								                                                    <path d="M5,14.7c-1.4,0-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6s2.6-1.2,2.6-2.6S6.5,14.7,5,14.7z M5,19.2c-1.1,0-1.9-0.9-1.9-1.9   c0-1.1,0.9-1.9,1.9-1.9s1.9,0.9,1.9,1.9C6.9,18.4,6.1,19.2,5,19.2z"></path>
								                                                    <path d="M11,11.6C11,10.2,9.8,9,8.4,9s-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6C9.8,14.2,11,13,11,11.6z M8.4,13.5c-1.1,0-1.9-0.9-1.9-1.9   c0-1.1,0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9C10.3,12.6,9.5,13.5,8.4,13.5z"></path>
								                                                    <path d="M13.2,9.4c1.4,0,2.6-1.2,2.6-2.6s-1.2-2.6-2.6-2.6s-2.6,1.2-2.6,2.6C10.5,8.2,11.7,9.4,13.2,9.4z M13.2,4.8   c1.1,0,1.9,0.9,1.9,1.9c0,1.1-0.9,1.9-1.9,1.9s-1.9-0.9-1.9-1.9C11.2,5.7,12.1,4.8,13.2,4.8z"></path>
								                                                    <path d="M17.1,12.9c0-1.4-1.2-2.6-2.6-2.6s-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6S17.1,14.3,17.1,12.9z M14.5,14.8   c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9S15.5,14.8,14.5,14.8z"></path>
								                                                    <path d="M3.4,9.2C3.6,7.8,5,7.5,8.2,5.7C10.6,4.4,8.9,0,8.9,0C8.8,1.8,4.3,3.2,4.3,3.2S-0.1,4.8,0,7.5c0.1,3.3,4.2,3.6,4.2,3.6   S3.2,10.3,3.4,9.2z M2.7,10c-0.9-0.3-2-1.1-2-2.6c0-1.8,2.8-3.2,3.8-3.6c0.6-0.1,2.8-0.8,4.1-2C8.8,3,8.9,4.6,7.8,5.2   c-0.7,0.4-1.4,0.7-2,1C4,7.1,2.9,7.7,2.7,9C2.6,9.4,2.6,9.7,2.7,10z"></path>
								                                                </g>
							                                            	<?php elseif ($product->aromatic_notes && ($product->aromatic_notes == 'Cocoa' || $product->aromatic_notes == 'Biscuits' )): ?>
							                                            		<g class="aromatic-notes aromatic-biscuits aromatic-cocoa">
								                                                    <ellipse cx="5.3" cy="15.1" rx="0.7" ry="0.5"></ellipse>
								                                                    <ellipse cx="6.7" cy="13.7" rx="0.7" ry="0.5"></ellipse>
								                                                    <ellipse cx="7.9" cy="12" rx="0.7" ry="0.5"></ellipse>
								                                                    <ellipse cx="9.1" cy="10.2" rx="0.7" ry="0.5"></ellipse>
								                                                    <ellipse cx="10.3" cy="8.5" rx="0.7" ry="0.5"></ellipse>
								                                                    <ellipse transform="matrix(0.9553 -0.2956 0.2956 0.9553 -1.7135 2.8891)" cx="8.7" cy="7.1" rx="0.5" ry="0.7"></ellipse>
								                                                    <ellipse transform="matrix(0.9554 -0.2954 0.2954 0.9554 -2.2607 2.6101)" cx="7.5" cy="8.8" rx="0.5" ry="0.7"></ellipse>
								                                                    <ellipse transform="matrix(0.9554 -0.2954 0.2954 0.9554 -2.7949 2.2933)" cx="6.2" cy="10.4" rx="0.5" ry="0.7"></ellipse>
								                                                    <ellipse transform="matrix(0.9554 -0.2953 0.2953 0.9554 -3.3551 1.9886)" cx="4.9" cy="12.1" rx="0.5" ry="0.7"></ellipse>
								                                                    <ellipse cx="11.5" cy="6.8" rx="0.7" ry="0.5"></ellipse>
								                                                    <ellipse cx="12.7" cy="5.1" rx="0.7" ry="0.5"></ellipse>
								                                                    <path d="M17.5,0.8C16.9,0.2,16-0.1,14.9,0c-3.4,0.5-6.1,2-9.4,4.2c-3,2.1-5.2,6.7-5.4,9c-0.1,1.3,0.1,4.4,1.4,5.9   C1.8,19.7,2.3,20,2.9,20C3,20,3,20,3.1,20l0,0c2.1,0,7-2,9.5-5.9c0.5-0.8,1.2-1.8,1.9-2.8c1.9-2.7,3.9-5.6,4-7.2   C18.5,4.1,18.6,2,17.5,0.8z M17.8,4.1C17.7,5.6,15.7,8.4,14,11c-0.7,1-1.4,1.9-1.9,2.8c-2.3,3.7-7.1,5.6-9,5.6c-0.1,0-0.1,0-0.1,0   c-0.4,0-0.7-0.2-1.1-0.6c-1-1.2-1.3-3.9-1.2-5.4c0.2-1.9,2.1-6.5,5.1-8.6C9,2.6,11.7,1.1,15,0.7c0.2,0,0.3,0,0.5,0   c0.6,0,1.2,0.2,1.5,0.6C17.9,2.3,17.8,4.1,17.8,4.1z"></path>
								                                                    <path d="M3,16c0-0.1,0-5.3,2.9-8c2.7-2.5,5-4,7.8-4.8C13.9,3.1,14,2.9,14,2.8c-0.1-0.2-0.2-0.3-0.4-0.2c-2.9,0.9-5.3,2.4-8.1,5   c-3.1,2.9-3.1,8.3-3.1,8.5s0.1,0.3,0.3,0.3S3,16.2,3,16z"></path>
								                                                    <path d="M14.8,3.4c-0.2,0-0.3,0.2-0.3,0.4c0,0,0.2,1.3-0.3,2.1c-0.2,0.3-0.5,0.8-1,1.4c-1.1,1.5-2.8,3.8-4.1,5.9   c-1.8,2.8-6,4.3-6.1,4.3c-0.2,0.1-0.3,0.3-0.2,0.4c0,0.1,0.2,0.2,0.3,0.2h0.1c0.2-0.1,4.5-1.6,6.4-4.6c1.4-2.1,3-4.3,4.1-5.9   c0.4-0.6,0.8-1.1,1-1.4c0.7-1,0.5-2.5,0.5-2.5C15.2,3.5,15,3.4,14.8,3.4z"></path>
								                                                </g>
							                                            	<?php elseif ($product->aromatic_notes && ($product->aromatic_notes == 'Malted' || $product->aromatic_notes == 'Cereals' )): ?>
							                                            		<g class="aromatic-notes aromatic-cereals aromatic-malted">
								                                                    <path d="M6.3,7.9C9.7,2.8,6.1,0,6.1,0C2.9,4.7,6.3,7.9,6.3,7.9z M6.2,0.9c0.7,0.8,1.8,2.8,0,5.9C5.6,5.9,4.6,3.8,6.2,0.9z"></path>
								                                                    <path d="M6.3,10.5c0,0-0.7-4.6-6.3-4.7C0,5.8,0.2,10.3,6.3,10.5z M5.5,9.8C2,9.5,1,7.5,0.7,6.4C3.9,6.7,5.1,8.7,5.5,9.8z"></path>
								                                                    <path d="M0,10.1c0,0,0.2,4.5,6.3,4.7C6.3,14.8,5.7,10.1,0,10.1z M0.7,10.7C3.9,11,5.1,13,5.5,14.1C2,13.8,1,11.8,0.7,10.7z"></path>
								                                                    <path d="M0,14.3c0,0,0.2,4.5,6.3,4.7C6.3,19,5.7,14.4,0,14.3z M0.7,15c3.2,0.3,4.4,2.3,4.8,3.4C2,18,1,16,0.7,15z"></path>
								                                                    <path d="M12.7,5.8c-5.7,0-6.4,4.7-6.4,4.7C12.5,10.3,12.7,5.8,12.7,5.8z M7.1,9.8C7.5,8.7,8.7,6.7,12,6.4   C11.7,7.5,10.7,9.5,7.1,9.8z"></path>
								                                                    <path d="M6.3,14.8c6.1-0.2,6.3-4.7,6.3-4.7C7,10.1,6.3,14.8,6.3,14.8z M12,10.7c-0.3,1.1-1.3,3.1-4.8,3.4C7.5,13,8.7,11,12,10.7z"></path>
								                                                    <path d="M6.3,19c6.1-0.2,6.3-4.7,6.3-4.7C7,14.4,6.3,19,6.3,19z M12,15c-0.3,1.1-1.3,3.1-4.8,3.4C7.5,17.3,8.7,15.3,12,15z"></path>
								                                                </g>
							                                            	<?php elseif ($product->aromatic_notes && $product->aromatic_notes == 'Roasted'): ?>
							                                            		<g class="aromatic-notes aromatic-roasted">
								                                                    <path d="M20.6,16.4L20.6,16.4L20.6,16.4c-1-1.3-2.3-2.3-3.8-2.9c-1.3-0.5-2.8-0.8-4.5-0.8c-1,0-2,0.1-3.1,0.3   c-3.4,0.6-6.8,2.9-8.4,5.7c-0.9,1.6-1.1,3.2-0.6,4.6c0.5,1.4,1.7,2.5,3.3,3.2c1.4,0.6,3.1,0.9,5,0.9c2,0,4.2-0.4,5.6-0.9   c3.3-1.4,5.5-3.2,6.4-5.5C21.5,18.5,20.6,16.5,20.6,16.4z M20,20.7c-0.8,2.1-2.9,3.9-6.1,5.2c-1.3,0.5-3.4,0.9-5.3,0.9   c-1.8,0-3.5-0.3-4.8-0.8c-1.5-0.6-2.5-1.6-3-2.9c-0.4-1.2-0.2-2.6,0.6-4.1c1.5-2.6,4.8-4.8,8-5.4c1-0.2,2-0.3,3-0.3   c1.6,0,3,0.3,4.3,0.8c1.4,0.6,2.5,1.4,3.4,2.6C20.2,16.9,20.8,18.6,20,20.7z"></path>
								                                                    <path d="M9.5,3.6C9.6,3.7,9.7,3.8,9.8,4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.3l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3C10,3,9.9,2.8,9.9,2.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9C10.3,0.1,10.4,0,10.4,0s-0.1,0.1-0.3,0.3C9.9,0.4,9.7,0.7,9.5,1.1   C9.4,1.3,9.3,1.6,9.2,1.8c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C9.3,3,9.4,3.3,9.5,3.6z"></path>
								                                                    <path d="M5.3,7.1c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4C7.2,8.7,7.1,8.4,7,8.1   C6.9,8,6.8,7.8,6.7,7.7L6.4,7.3L6.2,7.1C6.1,6.9,6,6.9,6,6.8C5.8,6.6,5.7,6.3,5.7,6.1c0-0.1,0-0.2-0.1-0.3c0-0.1,0-0.2,0-0.3   c0-0.2,0-0.4,0.1-0.6C5.8,4.5,5.9,4.1,6,3.9c0.1-0.2,0.2-0.3,0.2-0.3S6,3.7,5.9,3.8C5.7,4,5.4,4.3,5.3,4.7C5.2,4.9,5.1,5.2,5,5.4   c0,0.1,0,0.3,0,0.4C5,6,5,6.1,5,6.3C5.1,6.6,5.2,6.9,5.3,7.1z"></path>
								                                                    <path d="M17.6,16.5c-0.5,0.3-1.4,0.7-3,0.9c-0.4,0-0.7,0.1-1.1,0.1c-0.7,0-1.4-0.1-2.1-0.1c-0.7-0.1-1.3-0.1-1.9-0.1   c-1.1,0-2.2,0.2-3.2,0.6c-1.7,0.6-2.8,2.1-3.4,3.2c-0.7,1.2-0.9,2.3-0.9,2.3l-0.1,0.5l0.3-0.4c2.8-4.3,5-4.5,10.1-4.7   c1.5,0,3.5-0.3,5.1-1.5c0.9-0.7,1.2-1.3,1.2-1.4l-0.1-0.1C18.3,15.8,18.1,16.1,17.6,16.5z"></path>
								                                                </g>
							                                            	<?php elseif ($product->aromatic_notes && $product->aromatic_notes == 'Honey'): ?>
							                                            		<g class="aromatic-notes aromatic-honey">
								                                                	<path d="M20.5,17.7l0-6.1l-4.9-2.9l0-5.7L10.2,0L4.9,3.1l0,5.7l-5,2.9l0,6.1l5.3,3.1l4.9-2.9l5,2.9L20.5,17.7z M10.2,0.8l4.6,2.7  l0,5.3l-4.6,2.7L5.6,8.7l0-5.3L10.2,0.8z M5.3,20l-4.6-2.7l0-5.3l4.6-2.6L9.9,12l0,5.3L5.3,20z M10.6,12l4.6-2.7l4.6,2.6l0,5.3  L15.2,20l-4.6-2.7L10.6,12z"></path>
								                                                </g>
							                                                <?php elseif ($product->aromatic_notes && $product->aromatic_notes == 'Spicy'): ?>
								                                                <g class="aromatic-notes aromatic-spicy">
								                                                    <path d="M17.7,13.7c-0.3,0-0.6,0-0.9,0c-0.2,0-0.3,0-0.5,0c-2.3,0-3.2-1.7-3.8-3c-0.7-1.3-1.3-5.2-2.5-7C9.4,3,8.6,2.6,7.9,2.6   C7.2,2.6,6.5,2.9,6,3.4C5.9,3.5,5.7,3.6,5.6,3.7c1.5,0.7,2,2.8,2,2.8c-1-1.7-3-2.6-3-2.6l0,0C4.3,3.9,4,3.8,3.6,3.8   c-0.4,0-0.8,0.1-1.3,0.5C0,6,2.9,7.8,3.6,8.1C5,8.7,6.3,10,8.1,12.8c1.7,2.9,4.2,4,6.3,4c0.7,0,1.4-0.1,2-0.4   c0.3-0.1,0.5-0.1,0.8-0.1c1.6,0,3,1.4,3,1.4c0.1,0.1,0.2,0.1,0.3,0.1c0.4,0,0.5-0.6,0.5-0.9C21,16.2,21.2,13.7,17.7,13.7z    M20.4,16.9c0,0.1,0,0.1,0,0.2c-0.5-0.4-1.8-1.4-3.2-1.4c-0.4,0-0.7,0.1-1,0.2c-0.5,0.2-1.2,0.3-1.8,0.3c-2.3,0-4.4-1.3-5.8-3.7   c-1.7-2.8-3.1-4.3-4.7-5c-0.5-0.2-1.7-1-1.8-1.7c0-0.3,0.2-0.7,0.6-1c0.5-0.3,0.7-0.4,0.9-0.4c0.2,0,0.4,0.1,0.7,0.1l0.1,0   c0.2,0.1,1.9,0.9,2.7,2.3C7.2,7,7.4,7.2,7.6,7.2c0.1,0,0.1,0,0.2,0C8.1,7,8.3,6.7,8.2,6.4c0-0.1-0.4-1.7-1.6-2.7   C7,3.4,7.5,3.2,7.9,3.2c0.6,0,1.2,0.3,1.6,1c0.7,1,1.2,2.8,1.6,4.3c0.3,1.1,0.6,2,0.8,2.6c0.6,1.2,1.7,3.3,4.4,3.3   c0.2,0,0.4,0,0.5,0c0.3,0,0.6,0,0.8,0c1,0,1.8,0.2,2.2,0.7c0.5,0.5,0.5,1.3,0.5,1.7L20.4,16.9z"></path>
								                                                    <path d="M2.8,1.1c1.4,0.3,2,2.1,2,2.1C5.5,2.4,6,2.3,6,2.3S5.3,0.3,3.1,0C0.9-0.2,0,1.1,0,1.1l0.4,0.4C0.4,1.5,1.6,0.8,2.8,1.1z"></path>
								                                                </g>
							                                            	<?php elseif ($product->aromatic_notes && $product->aromatic_notes == 'Woody'): ?>
							                                            		<g class="aromatic-notes aromatic-woody">
								                                                    <path d="M23,4.6h-0.5H19h-1.5L13,0.2C13,0.1,12.9,0.1,12.9,0c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c-0.1,0-0.1,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0.1-0.2,0.1c0,0-0.1,0-0.1,0c-0.1,0.1-0.2,0.1-0.4,0.2c0,0,0,0,0,0   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.3,0.2-0.4,0.3   C9.2,2.4,8.7,3.7,9.1,4.1l0.5,0.5H2.8C1.2,4.6,0,6.8,0,9.4s1.2,4.8,2.8,4.8H19h3.6H23c1.1,0,2-2.2,2-4.8S24.1,4.6,23,4.6z    M2.8,13.5c-0.9,0-1.9-1.7-1.9-4s1-4,1.9-4c0.3,0,0.5,0.2,0.8,0.4c0.6,0.6,1.1,2,1.1,3.6c0,1.6-0.5,3-1.1,3.6   C3.3,13.3,3,13.5,2.8,13.5z M23,13.5h-0.5H19H4.3c0.7-0.9,1.2-2.4,1.2-4c0-1.7-0.5-3.2-1.2-4h5.3h1.9L10.1,4L9.7,3.6   c0-0.3,0.4-1,1.1-1.7c0.7-0.7,1.4-1.1,1.7-1.1l4.3,4.3l0.2,0.2h0.3H19h3.6H23c0.3,0,1.2,1.4,1.2,4C24.2,12.1,23.3,13.5,23,13.5z"></path>
								                                                    <rect x="7.5" y="7.5" width="14.4" height="0.5"></rect>
								                                                    <rect x="13" y="11.3" width="8.9" height="0.5"></rect>
								                                                    <rect x="16" y="9" width="3.8" height="0.5"></rect>
								                                                    <rect x="9.7" y="10" width="11" height="0.5"></rect>
								                                                </g>
							                                                <?php elseif ($product->aromatic_notes && $product->aromatic_notes == 'Intensely Roasted'): ?>
								                                                <g class="aromatic-notes aromatic-intensely-rosted">
								                                                    <path d="M20.6,16.4L20.6,16.4L20.6,16.4c-1-1.3-2.3-2.3-3.8-2.9c-1.3-0.5-2.8-0.8-4.5-0.8c-1,0-2,0.1-3.1,0.3   c-3.4,0.6-6.8,2.9-8.4,5.7c-0.9,1.6-1.1,3.2-0.6,4.6c0.5,1.4,1.7,2.5,3.3,3.2c1.4,0.6,3.1,0.9,5,0.9c2,0,4.2-0.4,5.6-0.9   c3.3-1.4,5.5-3.2,6.4-5.5C21.5,18.5,20.6,16.5,20.6,16.4z M20,20.7c-0.8,2.1-2.9,3.9-6.1,5.2c-1.3,0.5-3.4,0.9-5.3,0.9   c-1.8,0-3.5-0.3-4.8-0.8c-1.5-0.6-2.5-1.6-3-2.9c-0.4-1.2-0.2-2.6,0.6-4.1c1.5-2.6,4.8-4.8,8-5.4c1-0.2,2-0.3,3-0.3   c1.6,0,3,0.3,4.3,0.8c1.4,0.6,2.5,1.4,3.4,2.6C20.2,16.9,20.8,18.6,20,20.7z"></path>
								                                                    <path d="M17.6,16.5c-0.5,0.3-1.4,0.7-3,0.9c-0.4,0-0.7,0-1.1,0c-0.7,0-1.4-0.1-2.1-0.1c-0.7-0.1-1.3-0.1-1.9-0.1   c-1.1,0-2.2,0.2-3.2,0.6c-1.7,0.6-2.8,2.1-3.4,3.2c-0.7,1.2-0.9,2.3-0.9,2.3l-0.1,0.5l0.3-0.4c2.8-4.3,5-4.5,10.1-4.7   c1.5,0,3.5-0.3,5.1-1.5c0.9-0.7,1.2-1.3,1.2-1.4l-0.1-0.1C18.3,15.8,18.1,16.1,17.6,16.5z"></path>
								                                                    <path d="M14,5.9c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3C14.9,7,15,7.1,15,7.2c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6C15.2,9.5,15.1,9.8,15,10c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9C15.8,9,15.9,8.8,16,8.5c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.4l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3c-0.1-0.2-0.2-0.4-0.3-0.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9c0.1-0.2,0.2-0.3,0.2-0.3s-0.1,0.1-0.3,0.3   c-0.2,0.2-0.4,0.5-0.6,0.9c-0.1,0.2-0.2,0.4-0.2,0.7c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C13.8,5.3,13.8,5.6,14,5.9z"></path>
								                                                    <path d="M9.5,3.6C9.6,3.7,9.7,3.8,9.8,4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.3l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3C10,3,9.9,2.8,9.9,2.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9C10.3,0.1,10.4,0,10.4,0s-0.1,0.1-0.3,0.3C9.9,0.4,9.7,0.7,9.5,1.1   C9.4,1.3,9.3,1.6,9.2,1.8c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C9.3,3,9.4,3.3,9.5,3.6z"></path>
								                                                    <path d="M5.3,7.1c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4C7.2,8.7,7.1,8.4,7,8.1   C6.9,8,6.8,7.8,6.7,7.7L6.4,7.3L6.2,7.1C6.1,6.9,6,6.9,6,6.8C5.8,6.6,5.7,6.3,5.7,6.1c0-0.1,0-0.2-0.1-0.3c0-0.1,0-0.2,0-0.3   c0-0.2,0-0.4,0.1-0.6C5.8,4.5,5.9,4.1,6,3.9c0.1-0.2,0.2-0.3,0.2-0.3S6,3.7,5.9,3.8C5.7,4,5.4,4.3,5.3,4.7C5.2,4.9,5.1,5.2,5,5.4   c0,0.1,0,0.3,0,0.4C5,6,5,6.1,5,6.3C5.1,6.6,5.2,6.9,5.3,7.1z"></path>
								                                                </g>
								                                            <?php else: ?>
								                                            	<g class="aromatic-notes aromatic-intensely-rosted">
								                                                    <path d="M20.6,16.4L20.6,16.4L20.6,16.4c-1-1.3-2.3-2.3-3.8-2.9c-1.3-0.5-2.8-0.8-4.5-0.8c-1,0-2,0.1-3.1,0.3   c-3.4,0.6-6.8,2.9-8.4,5.7c-0.9,1.6-1.1,3.2-0.6,4.6c0.5,1.4,1.7,2.5,3.3,3.2c1.4,0.6,3.1,0.9,5,0.9c2,0,4.2-0.4,5.6-0.9   c3.3-1.4,5.5-3.2,6.4-5.5C21.5,18.5,20.6,16.5,20.6,16.4z M20,20.7c-0.8,2.1-2.9,3.9-6.1,5.2c-1.3,0.5-3.4,0.9-5.3,0.9   c-1.8,0-3.5-0.3-4.8-0.8c-1.5-0.6-2.5-1.6-3-2.9c-0.4-1.2-0.2-2.6,0.6-4.1c1.5-2.6,4.8-4.8,8-5.4c1-0.2,2-0.3,3-0.3   c1.6,0,3,0.3,4.3,0.8c1.4,0.6,2.5,1.4,3.4,2.6C20.2,16.9,20.8,18.6,20,20.7z"></path>
								                                                    <path d="M17.6,16.5c-0.5,0.3-1.4,0.7-3,0.9c-0.4,0-0.7,0-1.1,0c-0.7,0-1.4-0.1-2.1-0.1c-0.7-0.1-1.3-0.1-1.9-0.1   c-1.1,0-2.2,0.2-3.2,0.6c-1.7,0.6-2.8,2.1-3.4,3.2c-0.7,1.2-0.9,2.3-0.9,2.3l-0.1,0.5l0.3-0.4c2.8-4.3,5-4.5,10.1-4.7   c1.5,0,3.5-0.3,5.1-1.5c0.9-0.7,1.2-1.3,1.2-1.4l-0.1-0.1C18.3,15.8,18.1,16.1,17.6,16.5z"></path>
								                                                    <path d="M14,5.9c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3C14.9,7,15,7.1,15,7.2c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6C15.2,9.5,15.1,9.8,15,10c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9C15.8,9,15.9,8.8,16,8.5c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.4l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3c-0.1-0.2-0.2-0.4-0.3-0.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9c0.1-0.2,0.2-0.3,0.2-0.3s-0.1,0.1-0.3,0.3   c-0.2,0.2-0.4,0.5-0.6,0.9c-0.1,0.2-0.2,0.4-0.2,0.7c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C13.8,5.3,13.8,5.6,14,5.9z"></path>
								                                                    <path d="M9.5,3.6C9.6,3.7,9.7,3.8,9.8,4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.3l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3C10,3,9.9,2.8,9.9,2.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9C10.3,0.1,10.4,0,10.4,0s-0.1,0.1-0.3,0.3C9.9,0.4,9.7,0.7,9.5,1.1   C9.4,1.3,9.3,1.6,9.2,1.8c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C9.3,3,9.4,3.3,9.5,3.6z"></path>
								                                                    <path d="M5.3,7.1c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4C7.2,8.7,7.1,8.4,7,8.1   C6.9,8,6.8,7.8,6.7,7.7L6.4,7.3L6.2,7.1C6.1,6.9,6,6.9,6,6.8C5.8,6.6,5.7,6.3,5.7,6.1c0-0.1,0-0.2-0.1-0.3c0-0.1,0-0.2,0-0.3   c0-0.2,0-0.4,0.1-0.6C5.8,4.5,5.9,4.1,6,3.9c0.1-0.2,0.2-0.3,0.2-0.3S6,3.7,5.9,3.8C5.7,4,5.4,4.3,5.3,4.7C5.2,4.9,5.1,5.2,5,5.4   c0,0.1,0,0.3,0,0.4C5,6,5,6.1,5,6.3C5.1,6.6,5.2,6.9,5.3,7.1z"></path>
								                                                </g>
								                                            <?php endif; ?>
							                                            </svg>
							                                        </div>
							                                    </a>
							                                    <div class="v_coffeeDescription v_coffeeDescription_<?= strtolower(str_replace(' ','-',$product->name)) ?>">
							                                        <div class="m_circle m_circle_<?= strtolower(str_replace(' ','-',$product->name)) ?> <?= strtolower(str_replace(' ','-',$product->name)) ?>"></div>
							                                        <div id="range-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>" class="v_slider hide capsule-description capsule-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>">
																	    <div class="v_overflow" style="height: 646px;">
																	        <div class="v_padding">
																	            <a class="v_btnRoundSM v_btnCloseFixed close-modal" data-capsule="<?= strtolower(str_replace(' ','-',$product->name)) ?>"> <i class="fn_close"></i> <span>Close panel</span> </a>
																	            <div class="v_row">
																	                <div class="v_col33"> <img alt="" src="<?= $product->product_view_image['url'] ?>" lazy="loaded">  <!-- <img alt="" src="/ecom/medias/sys_master/public/10252256870430.png" lazy="loaded"> --> </div>
																	                <div class="v_col66">
																	                    <h5><strong class="v_brand" term="<?= $product->post_id?>"><?=$product->name?></strong></h5>
																	                    <div class="mobile-other">
																	                    	<p class=""><?= wc_price($product->price)?> </p>
																		                    <div class="mobile-add-to-cart">
																		                    	<button class="btn btn-icon btn-block btn-green btn-icon-right btn-coffee-origin"
																						                data-id="<?=$product->post_id?>"
																						                data-cart="true"
																						                data-name="<?=$product->name?>"
																						                data-price="<?=$product->price?>"
																						                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
																						                data-type="<?=$product->product_type?>"
																						                data-vat="1"
																						                data-qty-step="10"
																						                data-url="<?= $product->url ?>"
																						                data-aromatic-profile="<?= $product->aromatic_profile?>"
																						            >
																						            <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
																						        </button>
																		                    </div>
																	                    </div>
																	                    <p class="v_wysiwyg">
																	                    <?= substr(strip_tags($product->description, '<p>'), 0, 300) . ' . . . ' ?> <br>
																	                    <a class="v_link v_iconLeft" href="<?= $product->url ?>"> <i class="fn_arrowLink"></i> <span>Read more</span> </a>
																	                    </p>
																	                    <div class="v_row">
																						    <ul class="v_inlineTable">
																						        <li class="v_features"> <strong>Intensity</strong>
																						            <div class="v_intensityLevel">
																						                <ul class="v_inline">
																						                    <li class="v_value"><?= $product->intensity ?></li>
																						                    <?php for ($i=0; $i < $product->intensity; $i++): ?>
																						                    	<li class="v_filled"></li>
																						                    <?php endfor; ?>
																						                </ul>
																						            </div>
																						        </li>
																						        <li class="v_features"> <strong>Cup size</strong>
																						            <div class="v_intensityLevel v_level">
																						                <div class="cupsize">
																						                    <?php if(is_array($product->size_of_cup)):
																						                        foreach ($product->size_of_cup as $key => $value): ?>
																						                            <i class="cupsize__<?=$value?>"></i>
																						                        <?php endforeach; ?>
																						                    <?php else: ?>
																						                        <i class="cupsize__<?=$product->size_of_cup?>"></i>
																						                    <?php endif; ?>

																						                </div>
																						            </div>
																						        </li>
																						        <!-- <li class="v_features"> <strong>Bitterness</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Acidity</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">1</li>
																						                    <li class="v_filled"></li>
																						                    <li></li>
																						                    <li></li>
																						                    <li></li>
																						                    <li></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Body</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Roasting</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						    </ul>
																						    <ul class="v_inlineTable">
																						        <!-- <li class="v_features"> <strong>Cup size</strong> <span>Ristretto 25 ml<br>Espresso 40 ml</span> </li> -->
																						        <li class="v_features"> <strong>Aromatic profile</strong> <span><?=$product->aromatic_profile?></span> </li>
																						        <li class="v_features"> <strong>Aromatic notes</strong> <span><?= $product->property ?></span> </li>
																						    </ul>
																						</div>
																	                    <div class="v_row v_more">
																							<div></div>
																						    <div>
																						        <div class="v_addToCart">
																						            <div class="v_priceAndButton">
																						                <p class="v_productPrice"><?= wc_price($product->price)?> </p>
																						                <div class="product-add pull-right" style="position: relative;">

																									        <button class="btn btn-icon btn-block btn-green btn-icon-right btn-coffee-origin"
																									                data-id="<?=$product->post_id?>"
																									                data-cart="true"
																									                data-name="<?=$product->name?>"
																									                data-price="<?=$product->price?>"
																									                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
																									                data-type="<?=$product->product_type?>"
																									                data-vat="1"
																									                data-qty-step="10"
																									                data-url="<?= $product->url ?>"
																									                data-aromatic-profile="<?= $product->aromatic_profile?>"
																									            >
																									            <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
																									        </button>

																									    </div>
																						            </div>
																						        </div>
																						    </div>
																						</div>
																	                </div>
																	            </div>
																	        </div>
																	    </div>
																	</div>
							                                    </div>
							                                </li>
							                            <?php endif;?>
							                        <?php endif;?>
							                	<?php endforeach;?>
			                            	<?php endforeach;?>
			                            </ul>
			                        </div>
			                    </li>
		                	<?php endforeach; ?>
		                </ul>
		            </div>
		        </div>
		    </div>
		</section>
		<!-- End Intensity Section -->

		<!-- Aromatic Profile Section -->
		<section class="vue_coffeeSelector v_sectionLight hide" id="coffee-selector-aromatic-profile" data-label="Coffee selector">
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_coffeeSelector"> <span class="v_visually_hidden">Choose an aromatic profile on this list</span>
		                <ul class="v_aromaticProfile">
		                <?php foreach ($aromatic_notes as  $aromatic_profile => $aromatic_note): ?>
		                	<li>
		                        <a class="v_title v_aromatic_profile_filter v_active v-aromatic-profile-filter-<?=  $aromatic_profile ?>" data-filter="<?=  strtolower($aromatic_profile) ?>" aria-selected="false" >
		                            <h3 class="aromatic-h3"><?= $aromatic_profile ?></h3>
		                        </a>
		                        <span class="v_visually_hidden">Choose aromatic notes</span>
		                        <ul class="v_row v_categories">
		                        	<?php foreach ($aromatic_note as $aromatic_note_value):?>
		                        		<li class="v_hidable">
			                                <a class="v_titleAromaticCategory v_active  v_aromatic_notes_filter v-aromatic-notes-filter-<?=  strtolower(str_replace(' ','-',$aromatic_note_value)) ?> v-aromatic-profile-sub-filter-<?=  strtolower($aromatic_profile) ?>" data-filter="<?=  strtolower(str_replace(' ','-',$aromatic_note_value)) ?>" aria-selected="false">
			                                <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 21 27.3" style="enable-background:new 0 0 21 27.3;" xml:space="preserve" class="injected-svg" data-src="/shared_res/agility/grandCruCoffeeRange/coffeePlp/img/aromaticProfiles/roasted.svg" aria-hidden="true">
			                                        <?php if ($aromatic_note_value == 'Flowery'): ?>
	                                            		<g class="aromatic-notes aromatic-flowery">
		                                                    <path d="M10.5,0L10.5,0L10.5,0C9.1,0,8,1.1,8,2.5c0,0.5,0.2,1,0.5,1.4c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0   c0.6-0.2,1.3-0.3,1.9-0.3c0.7,0,1.3,0.1,1.9,0.3c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0C12.8,3.5,13,3,13,2.5   C13,1.1,11.9,0,10.5,0z M12.1,3.1c-0.5-0.1-1.1-0.2-1.6-0.2C9.9,2.9,9.4,3,8.9,3.1C8.8,2.9,8.7,2.7,8.7,2.5c0-1,0.8-1.7,1.7-1.8   l0,0h0l0,0c1,0,1.7,0.8,1.7,1.8C12.3,2.7,12.2,2.9,12.1,3.1z"></path>
		                                                    <path d="M4.1,12.3C4.1,12.3,4.1,12.3,4.1,12.3c-0.2-0.6-0.3-1.3-0.3-1.9c0-0.6,0.1-1.2,0.2-1.8c0,0,0,0,0,0C3.6,8.3,3.1,8,2.5,8   C1.1,8,0,9.1,0,10.5C0,11.9,1.1,13,2.5,13C3.1,13,3.6,12.7,4.1,12.3z M0.7,10.5c0-1,0.8-1.8,1.8-1.8c0.3,0,0.6,0.1,0.8,0.2   c-0.1,0.5-0.2,1-0.2,1.5c0,0.5,0.1,1.1,0.2,1.6c-0.2,0.1-0.5,0.2-0.8,0.2C1.5,12.3,0.7,11.5,0.7,10.5z"></path>
		                                                    <path d="M4.6,7.2L4.6,7.2c0.6-1.1,1.5-2.1,2.7-2.7c0,0,0,0,0,0C7.2,4,6.9,3.5,6.5,3.1C6.1,2.6,5.4,2.4,4.8,2.4S3.6,2.6,3.1,3.1   c-1,1-1,2.5,0,3.5C3.5,7,4,7.2,4.6,7.2z M3.6,3.6c0.3-0.3,0.8-0.5,1.2-0.5c0.5,0,0.9,0.2,1.2,0.5C6.2,3.7,6.4,4,6.4,4.2   C5.6,4.8,4.8,5.5,4.2,6.5C4,6.4,3.8,6.3,3.6,6.1C3.2,5.7,3.1,5.3,3.1,4.8C3.1,4.3,3.2,3.9,3.6,3.6z"></path>
		                                                    <path d="M12.4,17C12.4,17,12.4,17,12.4,17C12.3,17,12.3,16.9,12.4,17C12.3,16.9,12.3,16.9,12.4,17c-0.6,0.1-1.2,0.2-1.9,0.2   c-0.6,0-1.2-0.1-1.8-0.3c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c0,0,0,0,0,0c-0.4,0.4-0.6,1-0.6,1.6c0,1.4,1.1,2.5,2.5,2.5l0,0l0,0   c1.4,0,2.5-1.1,2.5-2.5C13,17.9,12.7,17.4,12.4,17C12.4,17,12.4,17,12.4,17z M10.5,20.3L10.5,20.3L10.5,20.3c-1,0-1.8-0.8-1.8-1.8   c0-0.3,0.1-0.6,0.2-0.8c0.5,0.1,1,0.2,1.6,0.2c0.5,0,1-0.1,1.5-0.2l0,0l0,0c0.1,0.2,0.2,0.5,0.2,0.8C12.3,19.5,11.5,20.3,10.5,20.3   z"></path>
		                                                    <path d="M4.7,13.7L4.7,13.7c-0.6,0-1.2,0.3-1.6,0.7c-1,1-1,2.5,0,3.5c0.5,0.5,1.1,0.7,1.7,0.7s1.3-0.2,1.7-0.7   c0.4-0.4,0.7-1,0.7-1.6c0,0,0,0,0,0C6.2,15.7,5.3,14.8,4.7,13.7z M6.1,17.4c-0.3,0.3-0.8,0.5-1.2,0.5c-0.5,0-0.9-0.2-1.2-0.5   c-0.3-0.3-0.5-0.8-0.5-1.2c0-0.5,0.2-0.9,0.5-1.2c0.2-0.2,0.5-0.4,0.7-0.4c0.6,0.9,1.3,1.6,2.2,2.2C6.4,16.9,6.3,17.2,6.1,17.4z"></path>
		                                                    <path d="M18.5,8c-0.6,0-1.1,0.2-1.6,0.6c0,0,0,0,0,0c0.2,0.6,0.2,1.2,0.2,1.8c0,0.7-0.1,1.3-0.3,1.9c0,0,0,0,0,0   c0.4,0.4,1,0.6,1.6,0.6c1.4,0,2.5-1.1,2.5-2.5C21,9.1,19.9,8,18.5,8z M18.5,12.3c-0.3,0-0.6-0.1-0.8-0.2c0.1-0.5,0.2-1.1,0.2-1.6   c0-0.5-0.1-1-0.2-1.5c0.2-0.1,0.5-0.2,0.8-0.2c1,0,1.8,0.8,1.8,1.8C20.3,11.5,19.5,12.3,18.5,12.3z"></path>
		                                                    <path d="M16.3,13.8L16.3,13.8c-0.6,1.1-1.5,2-2.6,2.6c0,0,0,0,0,0c0,0.6,0.3,1.1,0.7,1.6c0.5,0.5,1.1,0.7,1.7,0.7   c0.6,0,1.3-0.2,1.7-0.7c1-1,1-2.5,0-3.5C17.5,14,16.9,13.8,16.3,13.8z M17.4,17.4c-0.3,0.3-0.8,0.5-1.2,0.5s-0.9-0.2-1.2-0.5   c-0.2-0.2-0.4-0.5-0.4-0.7c0.9-0.6,1.6-1.3,2.2-2.2c0.2,0.1,0.5,0.2,0.7,0.4c0.3,0.3,0.5,0.8,0.5,1.2   C17.9,16.7,17.7,17.1,17.4,17.4z"></path>
		                                                    <path d="M16.4,7.2L16.4,7.2C17,7.2,17.5,7,17.9,6.5c1-1,1-2.5,0-3.5c-0.5-0.5-1.1-0.7-1.7-0.7c-0.6,0-1.3,0.2-1.7,0.7   C14,3.5,13.8,4,13.8,4.5c0,0,0,0,0,0C14.9,5.2,15.8,6.1,16.4,7.2z M14.9,3.6c0.3-0.3,0.8-0.5,1.2-0.5c0.5,0,0.9,0.2,1.2,0.5   c0.3,0.3,0.5,0.8,0.5,1.2c0,0.5-0.2,0.9-0.5,1.2c-0.2,0.2-0.4,0.3-0.7,0.4c-0.6-0.9-1.3-1.7-2.2-2.3C14.6,4,14.8,3.7,14.9,3.6z"></path>
		                                                </g>
	                                            	<?php elseif ($aromatic_note_value == 'Citrus'): ?>
	                                            		<g class="aromatic-notes aromatic-citrus">
		                                                    <path d="M11.5,0C5.2,0,0,5.2,0,11.5v1h1h21h1v-1C23,5.2,17.8,0,11.5,0z M1,11.5C1,5.7,5.7,1,11.5,1C17.3,1,22,5.7,22,11.5H1z"></path>
		                                                    <path d="M15.1,3.8l0.1-0.1v0c0-0.1,0.1-0.2,0.1-0.3c0-0.3-0.1-0.5-0.4-0.6c0,0-0.1,0-0.1-0.1c-1-0.4-2.1-0.6-3.2-0.6   c-1.1,0-2.2,0.2-3.2,0.6c0,0-0.1,0-0.1,0C7.9,2.9,7.8,3.2,7.8,3.4c0,0.1,0,0.2,0.1,0.3v0l0.1,0.1l3.6,6.2l0,0l0,0l0,0l0,0l0,0l0,0   L15.1,3.8z"></path>
		                                                    <path d="M7.2,4.3L7.1,4.1l0,0C7,4,7,4,6.9,3.9C6.7,3.8,6.4,3.8,6.2,4C6.2,4,6.1,4,6.1,4C5.2,4.7,4.5,5.6,4,6.5   c-0.6,1-0.9,2-1.1,3.1c0,0,0,0.1,0,0.1c0,0.2,0.1,0.5,0.3,0.6c0.1,0,0.2,0.1,0.3,0.1l0,0l0.2,0l7.1,0l0,0l0,0L7.2,4.3z"></path>
		                                                    <path d="M16.9,4c0,0-0.1-0.1-0.1-0.1c-0.2-0.1-0.5-0.1-0.7,0C16,4,16,4,15.9,4.1l0,0l-0.1,0.1l-3.6,6.2l0,0l0,0l7.1,0l0.2,0l0,0   c0.1,0,0.2,0,0.3-0.1c0.2-0.1,0.3-0.4,0.3-0.6c0,0,0-0.1,0-0.1C20,8.5,19.6,7.5,19,6.5C18.5,5.6,17.8,4.7,16.9,4z"></path>
		                                                </g>
	                                            	<?php elseif ($aromatic_note_value == 'Fruity Winy'): ?>
	                                            		<g class="aromatic-notes aromatic-fruity-winy">
		                                                    <path d="M18,15.8c-1.4,0-2.6,1.2-2.6,2.6S16.6,21,18,21s2.6-1.2,2.6-2.6C20.6,16.9,19.5,15.8,18,15.8z M18,20.3   c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9s1.9,0.9,1.9,1.9S19.1,20.3,18,20.3z"></path>
		                                                    <path d="M11.5,15.8c-1.4,0-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6s2.6-1.2,2.6-2.6C14.1,16.9,13,15.8,11.5,15.8z M11.5,20.3   c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9S12.6,20.3,11.5,20.3z"></path>
		                                                    <path d="M5,14.7c-1.4,0-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6s2.6-1.2,2.6-2.6S6.5,14.7,5,14.7z M5,19.2c-1.1,0-1.9-0.9-1.9-1.9   c0-1.1,0.9-1.9,1.9-1.9s1.9,0.9,1.9,1.9C6.9,18.4,6.1,19.2,5,19.2z"></path>
		                                                    <path d="M11,11.6C11,10.2,9.8,9,8.4,9s-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6C9.8,14.2,11,13,11,11.6z M8.4,13.5c-1.1,0-1.9-0.9-1.9-1.9   c0-1.1,0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9C10.3,12.6,9.5,13.5,8.4,13.5z"></path>
		                                                    <path d="M13.2,9.4c1.4,0,2.6-1.2,2.6-2.6s-1.2-2.6-2.6-2.6s-2.6,1.2-2.6,2.6C10.5,8.2,11.7,9.4,13.2,9.4z M13.2,4.8   c1.1,0,1.9,0.9,1.9,1.9c0,1.1-0.9,1.9-1.9,1.9s-1.9-0.9-1.9-1.9C11.2,5.7,12.1,4.8,13.2,4.8z"></path>
		                                                    <path d="M17.1,12.9c0-1.4-1.2-2.6-2.6-2.6s-2.6,1.2-2.6,2.6s1.2,2.6,2.6,2.6S17.1,14.3,17.1,12.9z M14.5,14.8   c-1.1,0-1.9-0.9-1.9-1.9c0-1.1,0.9-1.9,1.9-1.9c1.1,0,1.9,0.9,1.9,1.9S15.5,14.8,14.5,14.8z"></path>
		                                                    <path d="M3.4,9.2C3.6,7.8,5,7.5,8.2,5.7C10.6,4.4,8.9,0,8.9,0C8.8,1.8,4.3,3.2,4.3,3.2S-0.1,4.8,0,7.5c0.1,3.3,4.2,3.6,4.2,3.6   S3.2,10.3,3.4,9.2z M2.7,10c-0.9-0.3-2-1.1-2-2.6c0-1.8,2.8-3.2,3.8-3.6c0.6-0.1,2.8-0.8,4.1-2C8.8,3,8.9,4.6,7.8,5.2   c-0.7,0.4-1.4,0.7-2,1C4,7.1,2.9,7.7,2.7,9C2.6,9.4,2.6,9.7,2.7,10z"></path>
		                                                </g>
	                                            	<?php elseif ($aromatic_note_value == 'Cocoa' || $aromatic_note_value == 'Biscuits' ): ?>
	                                            		<g class="aromatic-notes aromatic-biscuits aromatic-cocoa">
		                                                    <ellipse cx="5.3" cy="15.1" rx="0.7" ry="0.5"></ellipse>
		                                                    <ellipse cx="6.7" cy="13.7" rx="0.7" ry="0.5"></ellipse>
		                                                    <ellipse cx="7.9" cy="12" rx="0.7" ry="0.5"></ellipse>
		                                                    <ellipse cx="9.1" cy="10.2" rx="0.7" ry="0.5"></ellipse>
		                                                    <ellipse cx="10.3" cy="8.5" rx="0.7" ry="0.5"></ellipse>
		                                                    <ellipse transform="matrix(0.9553 -0.2956 0.2956 0.9553 -1.7135 2.8891)" cx="8.7" cy="7.1" rx="0.5" ry="0.7"></ellipse>
		                                                    <ellipse transform="matrix(0.9554 -0.2954 0.2954 0.9554 -2.2607 2.6101)" cx="7.5" cy="8.8" rx="0.5" ry="0.7"></ellipse>
		                                                    <ellipse transform="matrix(0.9554 -0.2954 0.2954 0.9554 -2.7949 2.2933)" cx="6.2" cy="10.4" rx="0.5" ry="0.7"></ellipse>
		                                                    <ellipse transform="matrix(0.9554 -0.2953 0.2953 0.9554 -3.3551 1.9886)" cx="4.9" cy="12.1" rx="0.5" ry="0.7"></ellipse>
		                                                    <ellipse cx="11.5" cy="6.8" rx="0.7" ry="0.5"></ellipse>
		                                                    <ellipse cx="12.7" cy="5.1" rx="0.7" ry="0.5"></ellipse>
		                                                    <path d="M17.5,0.8C16.9,0.2,16-0.1,14.9,0c-3.4,0.5-6.1,2-9.4,4.2c-3,2.1-5.2,6.7-5.4,9c-0.1,1.3,0.1,4.4,1.4,5.9   C1.8,19.7,2.3,20,2.9,20C3,20,3,20,3.1,20l0,0c2.1,0,7-2,9.5-5.9c0.5-0.8,1.2-1.8,1.9-2.8c1.9-2.7,3.9-5.6,4-7.2   C18.5,4.1,18.6,2,17.5,0.8z M17.8,4.1C17.7,5.6,15.7,8.4,14,11c-0.7,1-1.4,1.9-1.9,2.8c-2.3,3.7-7.1,5.6-9,5.6c-0.1,0-0.1,0-0.1,0   c-0.4,0-0.7-0.2-1.1-0.6c-1-1.2-1.3-3.9-1.2-5.4c0.2-1.9,2.1-6.5,5.1-8.6C9,2.6,11.7,1.1,15,0.7c0.2,0,0.3,0,0.5,0   c0.6,0,1.2,0.2,1.5,0.6C17.9,2.3,17.8,4.1,17.8,4.1z"></path>
		                                                    <path d="M3,16c0-0.1,0-5.3,2.9-8c2.7-2.5,5-4,7.8-4.8C13.9,3.1,14,2.9,14,2.8c-0.1-0.2-0.2-0.3-0.4-0.2c-2.9,0.9-5.3,2.4-8.1,5   c-3.1,2.9-3.1,8.3-3.1,8.5s0.1,0.3,0.3,0.3S3,16.2,3,16z"></path>
		                                                    <path d="M14.8,3.4c-0.2,0-0.3,0.2-0.3,0.4c0,0,0.2,1.3-0.3,2.1c-0.2,0.3-0.5,0.8-1,1.4c-1.1,1.5-2.8,3.8-4.1,5.9   c-1.8,2.8-6,4.3-6.1,4.3c-0.2,0.1-0.3,0.3-0.2,0.4c0,0.1,0.2,0.2,0.3,0.2h0.1c0.2-0.1,4.5-1.6,6.4-4.6c1.4-2.1,3-4.3,4.1-5.9   c0.4-0.6,0.8-1.1,1-1.4c0.7-1,0.5-2.5,0.5-2.5C15.2,3.5,15,3.4,14.8,3.4z"></path>
		                                                </g>
	                                            	<?php elseif ($aromatic_note_value == 'Malted' || $aromatic_note_value == 'Cereals' ): ?>
	                                            		<g class="aromatic-notes aromatic-cereals aromatic-malted">
		                                                    <path d="M6.3,7.9C9.7,2.8,6.1,0,6.1,0C2.9,4.7,6.3,7.9,6.3,7.9z M6.2,0.9c0.7,0.8,1.8,2.8,0,5.9C5.6,5.9,4.6,3.8,6.2,0.9z"></path>
		                                                    <path d="M6.3,10.5c0,0-0.7-4.6-6.3-4.7C0,5.8,0.2,10.3,6.3,10.5z M5.5,9.8C2,9.5,1,7.5,0.7,6.4C3.9,6.7,5.1,8.7,5.5,9.8z"></path>
		                                                    <path d="M0,10.1c0,0,0.2,4.5,6.3,4.7C6.3,14.8,5.7,10.1,0,10.1z M0.7,10.7C3.9,11,5.1,13,5.5,14.1C2,13.8,1,11.8,0.7,10.7z"></path>
		                                                    <path d="M0,14.3c0,0,0.2,4.5,6.3,4.7C6.3,19,5.7,14.4,0,14.3z M0.7,15c3.2,0.3,4.4,2.3,4.8,3.4C2,18,1,16,0.7,15z"></path>
		                                                    <path d="M12.7,5.8c-5.7,0-6.4,4.7-6.4,4.7C12.5,10.3,12.7,5.8,12.7,5.8z M7.1,9.8C7.5,8.7,8.7,6.7,12,6.4   C11.7,7.5,10.7,9.5,7.1,9.8z"></path>
		                                                    <path d="M6.3,14.8c6.1-0.2,6.3-4.7,6.3-4.7C7,10.1,6.3,14.8,6.3,14.8z M12,10.7c-0.3,1.1-1.3,3.1-4.8,3.4C7.5,13,8.7,11,12,10.7z"></path>
		                                                    <path d="M6.3,19c6.1-0.2,6.3-4.7,6.3-4.7C7,14.4,6.3,19,6.3,19z M12,15c-0.3,1.1-1.3,3.1-4.8,3.4C7.5,17.3,8.7,15.3,12,15z"></path>
		                                                </g>
	                                            	<?php elseif ($aromatic_note_value == 'Roasted'): ?>
	                                            		<g class="aromatic-notes aromatic-roasted">
		                                                    <path d="M20.6,16.4L20.6,16.4L20.6,16.4c-1-1.3-2.3-2.3-3.8-2.9c-1.3-0.5-2.8-0.8-4.5-0.8c-1,0-2,0.1-3.1,0.3   c-3.4,0.6-6.8,2.9-8.4,5.7c-0.9,1.6-1.1,3.2-0.6,4.6c0.5,1.4,1.7,2.5,3.3,3.2c1.4,0.6,3.1,0.9,5,0.9c2,0,4.2-0.4,5.6-0.9   c3.3-1.4,5.5-3.2,6.4-5.5C21.5,18.5,20.6,16.5,20.6,16.4z M20,20.7c-0.8,2.1-2.9,3.9-6.1,5.2c-1.3,0.5-3.4,0.9-5.3,0.9   c-1.8,0-3.5-0.3-4.8-0.8c-1.5-0.6-2.5-1.6-3-2.9c-0.4-1.2-0.2-2.6,0.6-4.1c1.5-2.6,4.8-4.8,8-5.4c1-0.2,2-0.3,3-0.3   c1.6,0,3,0.3,4.3,0.8c1.4,0.6,2.5,1.4,3.4,2.6C20.2,16.9,20.8,18.6,20,20.7z"></path>
		                                                    <path d="M9.5,3.6C9.6,3.7,9.7,3.8,9.8,4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.3l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3C10,3,9.9,2.8,9.9,2.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9C10.3,0.1,10.4,0,10.4,0s-0.1,0.1-0.3,0.3C9.9,0.4,9.7,0.7,9.5,1.1   C9.4,1.3,9.3,1.6,9.2,1.8c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C9.3,3,9.4,3.3,9.5,3.6z"></path>
		                                                    <path d="M5.3,7.1c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4C7.2,8.7,7.1,8.4,7,8.1   C6.9,8,6.8,7.8,6.7,7.7L6.4,7.3L6.2,7.1C6.1,6.9,6,6.9,6,6.8C5.8,6.6,5.7,6.3,5.7,6.1c0-0.1,0-0.2-0.1-0.3c0-0.1,0-0.2,0-0.3   c0-0.2,0-0.4,0.1-0.6C5.8,4.5,5.9,4.1,6,3.9c0.1-0.2,0.2-0.3,0.2-0.3S6,3.7,5.9,3.8C5.7,4,5.4,4.3,5.3,4.7C5.2,4.9,5.1,5.2,5,5.4   c0,0.1,0,0.3,0,0.4C5,6,5,6.1,5,6.3C5.1,6.6,5.2,6.9,5.3,7.1z"></path>
		                                                    <path d="M17.6,16.5c-0.5,0.3-1.4,0.7-3,0.9c-0.4,0-0.7,0.1-1.1,0.1c-0.7,0-1.4-0.1-2.1-0.1c-0.7-0.1-1.3-0.1-1.9-0.1   c-1.1,0-2.2,0.2-3.2,0.6c-1.7,0.6-2.8,2.1-3.4,3.2c-0.7,1.2-0.9,2.3-0.9,2.3l-0.1,0.5l0.3-0.4c2.8-4.3,5-4.5,10.1-4.7   c1.5,0,3.5-0.3,5.1-1.5c0.9-0.7,1.2-1.3,1.2-1.4l-0.1-0.1C18.3,15.8,18.1,16.1,17.6,16.5z"></path>
		                                                </g>
	                                            	<?php elseif ($aromatic_note_value == 'Honey'): ?>
	                                            		<g class="aromatic-notes aromatic-honey">
		                                                	<path d="M20.5,17.7l0-6.1l-4.9-2.9l0-5.7L10.2,0L4.9,3.1l0,5.7l-5,2.9l0,6.1l5.3,3.1l4.9-2.9l5,2.9L20.5,17.7z M10.2,0.8l4.6,2.7  l0,5.3l-4.6,2.7L5.6,8.7l0-5.3L10.2,0.8z M5.3,20l-4.6-2.7l0-5.3l4.6-2.6L9.9,12l0,5.3L5.3,20z M10.6,12l4.6-2.7l4.6,2.6l0,5.3  L15.2,20l-4.6-2.7L10.6,12z"></path>
		                                                </g>
	                                                <?php elseif ($aromatic_note_value == 'Spicy'): ?>
		                                                <g class="aromatic-notes aromatic-spicy">
		                                                    <path d="M17.7,13.7c-0.3,0-0.6,0-0.9,0c-0.2,0-0.3,0-0.5,0c-2.3,0-3.2-1.7-3.8-3c-0.7-1.3-1.3-5.2-2.5-7C9.4,3,8.6,2.6,7.9,2.6   C7.2,2.6,6.5,2.9,6,3.4C5.9,3.5,5.7,3.6,5.6,3.7c1.5,0.7,2,2.8,2,2.8c-1-1.7-3-2.6-3-2.6l0,0C4.3,3.9,4,3.8,3.6,3.8   c-0.4,0-0.8,0.1-1.3,0.5C0,6,2.9,7.8,3.6,8.1C5,8.7,6.3,10,8.1,12.8c1.7,2.9,4.2,4,6.3,4c0.7,0,1.4-0.1,2-0.4   c0.3-0.1,0.5-0.1,0.8-0.1c1.6,0,3,1.4,3,1.4c0.1,0.1,0.2,0.1,0.3,0.1c0.4,0,0.5-0.6,0.5-0.9C21,16.2,21.2,13.7,17.7,13.7z    M20.4,16.9c0,0.1,0,0.1,0,0.2c-0.5-0.4-1.8-1.4-3.2-1.4c-0.4,0-0.7,0.1-1,0.2c-0.5,0.2-1.2,0.3-1.8,0.3c-2.3,0-4.4-1.3-5.8-3.7   c-1.7-2.8-3.1-4.3-4.7-5c-0.5-0.2-1.7-1-1.8-1.7c0-0.3,0.2-0.7,0.6-1c0.5-0.3,0.7-0.4,0.9-0.4c0.2,0,0.4,0.1,0.7,0.1l0.1,0   c0.2,0.1,1.9,0.9,2.7,2.3C7.2,7,7.4,7.2,7.6,7.2c0.1,0,0.1,0,0.2,0C8.1,7,8.3,6.7,8.2,6.4c0-0.1-0.4-1.7-1.6-2.7   C7,3.4,7.5,3.2,7.9,3.2c0.6,0,1.2,0.3,1.6,1c0.7,1,1.2,2.8,1.6,4.3c0.3,1.1,0.6,2,0.8,2.6c0.6,1.2,1.7,3.3,4.4,3.3   c0.2,0,0.4,0,0.5,0c0.3,0,0.6,0,0.8,0c1,0,1.8,0.2,2.2,0.7c0.5,0.5,0.5,1.3,0.5,1.7L20.4,16.9z"></path>
		                                                    <path d="M2.8,1.1c1.4,0.3,2,2.1,2,2.1C5.5,2.4,6,2.3,6,2.3S5.3,0.3,3.1,0C0.9-0.2,0,1.1,0,1.1l0.4,0.4C0.4,1.5,1.6,0.8,2.8,1.1z"></path>
		                                                </g>
	                                            	<?php elseif ($aromatic_note_value == 'Woody'): ?>
	                                            		<g class="aromatic-notes aromatic-woody">
		                                                    <path d="M23,4.6h-0.5H19h-1.5L13,0.2C13,0.1,12.9,0.1,12.9,0c0,0-0.1,0-0.1,0c-0.1,0-0.1,0-0.2,0c0,0,0,0,0,0c0,0,0,0-0.1,0   c-0.1,0-0.1,0-0.2,0c0,0-0.1,0-0.1,0c-0.1,0-0.2,0.1-0.2,0.1c0,0-0.1,0-0.1,0c-0.1,0.1-0.2,0.1-0.4,0.2c0,0,0,0,0,0   c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.2,0.1-0.3,0.2c0,0-0.1,0.1-0.1,0.1c-0.1,0.1-0.3,0.2-0.4,0.3   C9.2,2.4,8.7,3.7,9.1,4.1l0.5,0.5H2.8C1.2,4.6,0,6.8,0,9.4s1.2,4.8,2.8,4.8H19h3.6H23c1.1,0,2-2.2,2-4.8S24.1,4.6,23,4.6z    M2.8,13.5c-0.9,0-1.9-1.7-1.9-4s1-4,1.9-4c0.3,0,0.5,0.2,0.8,0.4c0.6,0.6,1.1,2,1.1,3.6c0,1.6-0.5,3-1.1,3.6   C3.3,13.3,3,13.5,2.8,13.5z M23,13.5h-0.5H19H4.3c0.7-0.9,1.2-2.4,1.2-4c0-1.7-0.5-3.2-1.2-4h5.3h1.9L10.1,4L9.7,3.6   c0-0.3,0.4-1,1.1-1.7c0.7-0.7,1.4-1.1,1.7-1.1l4.3,4.3l0.2,0.2h0.3H19h3.6H23c0.3,0,1.2,1.4,1.2,4C24.2,12.1,23.3,13.5,23,13.5z"></path>
		                                                    <rect x="7.5" y="7.5" width="14.4" height="0.5"></rect>
		                                                    <rect x="13" y="11.3" width="8.9" height="0.5"></rect>
		                                                    <rect x="16" y="9" width="3.8" height="0.5"></rect>
		                                                    <rect x="9.7" y="10" width="11" height="0.5"></rect>
		                                                </g>
	                                                <?php elseif ($aromatic_note_value == 'Intensely Roasted'): ?>
		                                                <g>
															<path d="M20.6,16.4L20.6,16.4L20.6,16.4c-1-1.3-2.3-2.3-3.8-2.9c-1.3-0.5-2.8-0.8-4.5-0.8c-1,0-2,0.1-3.1,0.3   c-3.4,0.6-6.8,2.9-8.4,5.7c-0.9,1.6-1.1,3.2-0.6,4.6c0.5,1.4,1.7,2.5,3.3,3.2c1.4,0.6,3.1,0.9,5,0.9c2,0,4.2-0.4,5.6-0.9   c3.3-1.4,5.5-3.2,6.4-5.5C21.5,18.5,20.6,16.5,20.6,16.4z M20,20.7c-0.8,2.1-2.9,3.9-6.1,5.2c-1.3,0.5-3.4,0.9-5.3,0.9   c-1.8,0-3.5-0.3-4.8-0.8c-1.5-0.6-2.5-1.6-3-2.9c-0.4-1.2-0.2-2.6,0.6-4.1c1.5-2.6,4.8-4.8,8-5.4c1-0.2,2-0.3,3-0.3   c1.6,0,3,0.3,4.3,0.8c1.4,0.6,2.5,1.4,3.4,2.6C20.2,16.9,20.8,18.6,20,20.7z"></path>
															<path d="M9.5,3.6C9.6,3.7,9.7,3.8,9.8,4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.3l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3C10,3,9.9,2.8,9.9,2.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9C10.3,0.1,10.4,0,10.4,0s-0.1,0.1-0.3,0.3C9.9,0.4,9.7,0.7,9.5,1.1   C9.4,1.3,9.3,1.6,9.2,1.8c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C9.3,3,9.4,3.3,9.5,3.6z"></path>
															<path d="M5.3,7.1c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4C7.2,8.7,7.1,8.4,7,8.1   C6.9,8,6.8,7.8,6.7,7.7L6.4,7.3L6.2,7.1C6.1,6.9,6,6.9,6,6.8C5.8,6.6,5.7,6.3,5.7,6.1c0-0.1,0-0.2-0.1-0.3c0-0.1,0-0.2,0-0.3   c0-0.2,0-0.4,0.1-0.6C5.8,4.5,5.9,4.1,6,3.9c0.1-0.2,0.2-0.3,0.2-0.3S6,3.7,5.9,3.8C5.7,4,5.4,4.3,5.3,4.7C5.2,4.9,5.1,5.2,5,5.4   c0,0.1,0,0.3,0,0.4C5,6,5,6.1,5,6.3C5.1,6.6,5.2,6.9,5.3,7.1z"></path>
															<path d="M17.6,16.5c-0.5,0.3-1.4,0.7-3,0.9c-0.4,0-0.7,0.1-1.1,0.1c-0.7,0-1.4-0.1-2.1-0.1c-0.7-0.1-1.3-0.1-1.9-0.1   c-1.1,0-2.2,0.2-3.2,0.6c-1.7,0.6-2.8,2.1-3.4,3.2c-0.7,1.2-0.9,2.3-0.9,2.3l-0.1,0.5l0.3-0.4c2.8-4.3,5-4.5,10.1-4.7   c1.5,0,3.5-0.3,5.1-1.5c0.9-0.7,1.2-1.3,1.2-1.4l-0.1-0.1C18.3,15.8,18.1,16.1,17.6,16.5z"></path>
														</g>
		                                            <?php else: ?>
		                                            	<g class="aromatic-notes aromatic-intensely-rosted">
		                                                    <path d="M20.6,16.4L20.6,16.4L20.6,16.4c-1-1.3-2.3-2.3-3.8-2.9c-1.3-0.5-2.8-0.8-4.5-0.8c-1,0-2,0.1-3.1,0.3   c-3.4,0.6-6.8,2.9-8.4,5.7c-0.9,1.6-1.1,3.2-0.6,4.6c0.5,1.4,1.7,2.5,3.3,3.2c1.4,0.6,3.1,0.9,5,0.9c2,0,4.2-0.4,5.6-0.9   c3.3-1.4,5.5-3.2,6.4-5.5C21.5,18.5,20.6,16.5,20.6,16.4z M20,20.7c-0.8,2.1-2.9,3.9-6.1,5.2c-1.3,0.5-3.4,0.9-5.3,0.9   c-1.8,0-3.5-0.3-4.8-0.8c-1.5-0.6-2.5-1.6-3-2.9c-0.4-1.2-0.2-2.6,0.6-4.1c1.5-2.6,4.8-4.8,8-5.4c1-0.2,2-0.3,3-0.3   c1.6,0,3,0.3,4.3,0.8c1.4,0.6,2.5,1.4,3.4,2.6C20.2,16.9,20.8,18.6,20,20.7z"></path>
		                                                    <path d="M17.6,16.5c-0.5,0.3-1.4,0.7-3,0.9c-0.4,0-0.7,0-1.1,0c-0.7,0-1.4-0.1-2.1-0.1c-0.7-0.1-1.3-0.1-1.9-0.1   c-1.1,0-2.2,0.2-3.2,0.6c-1.7,0.6-2.8,2.1-3.4,3.2c-0.7,1.2-0.9,2.3-0.9,2.3l-0.1,0.5l0.3-0.4c2.8-4.3,5-4.5,10.1-4.7   c1.5,0,3.5-0.3,5.1-1.5c0.9-0.7,1.2-1.3,1.2-1.4l-0.1-0.1C18.3,15.8,18.1,16.1,17.6,16.5z"></path>
		                                                    <path d="M14,5.9c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3C14.9,7,15,7.1,15,7.2c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6C15.2,9.5,15.1,9.8,15,10c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9C15.8,9,15.9,8.8,16,8.5c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.4l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3c-0.1-0.2-0.2-0.4-0.3-0.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9c0.1-0.2,0.2-0.3,0.2-0.3s-0.1,0.1-0.3,0.3   c-0.2,0.2-0.4,0.5-0.6,0.9c-0.1,0.2-0.2,0.4-0.2,0.7c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C13.8,5.3,13.8,5.6,14,5.9z"></path>
		                                                    <path d="M9.5,3.6C9.6,3.7,9.7,3.8,9.8,4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4c-0.1-0.3-0.2-0.6-0.3-0.9   c-0.1-0.1-0.2-0.3-0.3-0.4l-0.3-0.3l-0.3-0.3c-0.1-0.1-0.1-0.2-0.2-0.3C10,3,9.9,2.8,9.9,2.6c0-0.1,0-0.2-0.1-0.3   c0-0.1,0-0.2,0-0.3c0-0.2,0-0.4,0.1-0.6c0.1-0.4,0.2-0.7,0.3-0.9C10.3,0.1,10.4,0,10.4,0s-0.1,0.1-0.3,0.3C9.9,0.4,9.7,0.7,9.5,1.1   C9.4,1.3,9.3,1.6,9.2,1.8c0,0.1,0,0.3,0,0.4c0,0.1,0,0.3,0,0.4C9.3,3,9.4,3.3,9.5,3.6z"></path>
		                                                    <path d="M5.3,7.1c0.1,0.1,0.2,0.3,0.3,0.4l0.3,0.3l0.3,0.3c0.1,0.1,0.1,0.2,0.2,0.3c0.1,0.2,0.2,0.4,0.3,0.6c0,0.1,0,0.2,0.1,0.3   c0,0.1,0,0.2,0,0.3c0,0.2,0,0.4-0.1,0.6c-0.1,0.4-0.2,0.7-0.3,0.9c-0.1,0.2-0.2,0.3-0.2,0.3s0.1-0.1,0.3-0.3   c0.2-0.2,0.4-0.5,0.6-0.9c0.1-0.2,0.2-0.4,0.2-0.7c0-0.1,0-0.3,0-0.4c0-0.1,0-0.3,0-0.4C7.2,8.7,7.1,8.4,7,8.1   C6.9,8,6.8,7.8,6.7,7.7L6.4,7.3L6.2,7.1C6.1,6.9,6,6.9,6,6.8C5.8,6.6,5.7,6.3,5.7,6.1c0-0.1,0-0.2-0.1-0.3c0-0.1,0-0.2,0-0.3   c0-0.2,0-0.4,0.1-0.6C5.8,4.5,5.9,4.1,6,3.9c0.1-0.2,0.2-0.3,0.2-0.3S6,3.7,5.9,3.8C5.7,4,5.4,4.3,5.3,4.7C5.2,4.9,5.1,5.2,5,5.4   c0,0.1,0,0.3,0,0.4C5,6,5,6.1,5,6.3C5.1,6.6,5.2,6.9,5.3,7.1z"></path>
		                                                </g>
		                                            <?php endif; ?>
			                                    </svg>
			                                    <span class="v_titleAromaticNote"><?= $aromatic_note_value ?></span>
			                                </a>
			                                <span class="v_visually_hidden">Choose a coffee</span>
			                                <div class="v_mobileSlider">
			                                    <ul class="v_coffees">
				                        		<?php foreach ($products as $category => $product):
				                        			foreach ($product as $k => $product):
					                        			if ($aromatic_note_value == @$product->aromatic_notes):?>
					                        				<li
					                        					<?php
				                            					$class_size_of_cup = '';
				                            					if(is_array($product->size_of_cup)):
											                        foreach ($product->size_of_cup as $size_of_cup):
											                        	$class_size_of_cup .= " v-cupsize-" . $size_of_cup;
											             			endforeach;
												                else:
												                	$class_size_of_cup .= " v-cupsize-" . $size_of_cup;
												                endif; ?>

												                <?php
				                            					if(is_array($product->size_of_cup)):
											                        $cupSize = implode(",", $product->size_of_cup);
											                    else:
											                       $cupSize = $value;
											                    endif;?>

				                            					class="v-capsules  v-capsules-active <?= $class_size_of_cup ?> v-capsule-category-<?= strtolower(str_replace(' ','-',$category))?> v-intensity-<?= $product->intensity ?> v-aromatic-profile-<?= strtolower($product->aromatic_profile) ?> v-aromatic-notes-<?=  strtolower(str_replace(' ','-',$aromatic_note_value)) ?>"
												                data-cupsize="<?= $cupSize?>"
												                data-intensity="<?= $product->intensity?>"
				                            					data-aromaticprofile="<?= strtolower($product->aromatic_profile)?>"
				                            					data-category="<?=  strtolower(str_replace(' ','-',$category)) ?>"
				                            					data-aromaticnotes="<?=  strtolower(str_replace(' ','-',$aromatic_note_value)) ?>"
					                        				>
					                                            <a href="#aromatic-profile-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>" aria-expanded="false" class="capsules_description" data-capsule="<?= strtolower(str_replace(' ','-',$product->name)) ?>">
					                                                <div data-product-item-id="7701.40">
					                                                    <h4 class="v_titleCoffee"><strong class="v_brand" term="<?= ucwords(strtolower($product->name))?>"><?= ucwords(strtolower($product->name))?></strong></h4>
					                                                    <div class="v_caps">
					                                                        <div class="v_capsPlaceholder"></div>
					                                                        <img alt="" src="<?= $product->image[0] ?>" lazy="loaded">
					                                                    </div>
					                                                    <span class="v_intensity"><?= $product->intensity ?></span>
					                                                </div>
					                                            </a>
					                                            <div class="v_coffeeDescription v_coffeeDescription_<?= strtolower(str_replace(' ','-',$product->name)) ?>">
							                                        <div class="m_circle m_circle_<?= strtolower(str_replace(' ','-',$product->name)) ?> <?= strtolower(str_replace(' ','-',$product->name)) ?>"></div>
							                                        <div id="range-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>" class="v_slider hide capsule-description capsule-description-<?= strtolower(str_replace(' ','-',$product->name)) ?>">
																	    <div class="v_overflow" style="height: 646px;">
																	        <div class="v_padding">
																	            <a class="v_btnRoundSM v_btnCloseFixed close-modal" data-capsule="<?= strtolower(str_replace(' ','-',$product->name)) ?>"> <i class="fn_close"></i> <span>Close panel</span> </a>
																	            <div class="v_row">
																	                <div class="v_col33"> <img alt="" src="<?= $product->product_view_image['url'] ?>" lazy="loaded">  <!-- <img alt="" src="/ecom/medias/sys_master/public/10252256870430.png" lazy="loaded"> --> </div>
																	                <div class="v_col66">
																	                    <h5><strong class="v_brand" term="<?= $product->post_id?>"><?=$product->name?></strong></h5>
																	                    <div class="mobile-other">
																	                    	<p class=""><?= wc_price($product->price)?> </p>
																		                    <div class="mobile-add-to-cart">
																		                    	<button class="btn btn-icon btn-block btn-green btn-icon-right btn-coffee-origin"
																						                data-id="<?=$product->post_id?>"
																						                data-cart="true"
																						                data-name="<?=$product->name?>"
																						                data-price="<?=$product->price?>"
																						                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
																						                data-type="<?=$product->product_type?>"
																						                data-vat="1"
																						                data-qty-step="10"
																						                data-url="<?= $product->url ?>"
																						                data-aromatic-profile="<?= $product->aromatic_profile?>"
																						            >
																						            <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
																						        </button>
																		                    </div>
																	                    </div>
																	                    <p class="v_wysiwyg">
																	                    <?= substr(strip_tags($product->description, '<p>'), 0, 300) . ' . . . ' ?> <br>
																	                    <a class="v_link v_iconLeft" href="<?= $product->url ?>"> <i class="fn_arrowLink"></i> <span>Read more</span> </a>
																	                    </p>
																	                    <div class="v_row">
																						    <ul class="v_inlineTable">
																						        <li class="v_features"> <strong>Intensity</strong>
																						            <div class="v_intensityLevel">
																						                <ul class="v_inline">
																						                    <li class="v_value"><?= $product->intensity ?></li>
																						                    <?php for ($i=0; $i < $product->intensity; $i++): ?>
																						                    	<li class="v_filled"></li>
																						                    <?php endfor; ?>
																						                </ul>
																						            </div>
																						        </li>
																						        <li class="v_features"> <strong>Cup size</strong>
																						            <div class="v_intensityLevel v_level">
																						                <div class="cupsize">
																						                    <?php if(is_array($product->size_of_cup)):
																						                        foreach ($product->size_of_cup as $key => $value): ?>
																						                            <i class="cupsize__<?=$value?>"></i>
																						                        <?php endforeach; ?>
																						                    <?php else: ?>
																						                        <i class="cupsize__<?=$product->size_of_cup?>"></i>
																						                    <?php endif; ?>

																						                </div>
																						            </div>
																						        </li>
																						        <!-- <li class="v_features"> <strong>Bitterness</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Acidity</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">1</li>
																						                    <li class="v_filled"></li>
																						                    <li></li>
																						                    <li></li>
																						                    <li></li>
																						                    <li></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Body</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						        <!-- <li class="v_features"> <strong>Roasting</strong>
																						            <div class="v_intensityLevel v_level">
																						                <ul class="v_inline">
																						                    <li class="v_value">5</li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                    <li class="v_filled"></li>
																						                </ul>
																						            </div>
																						        </li> -->
																						    </ul>
																						    <ul class="v_inlineTable">
																						        <!-- <li class="v_features"> <strong>Cup size</strong> <span>Ristretto 25 ml<br>Espresso 40 ml</span> </li> -->
																						        <li class="v_features"> <strong>Aromatic profile</strong> <span><?=$product->aromatic_profile?></span> </li>
																						        <li class="v_features"> <strong>Aromatic notes</strong> <span><?= $product->aromatic_notes ?></span> </li>
																						    </ul>
																						</div>
																	                    <div class="v_row v_more">
																							<div></div>
																						    <div>
																						        <div class="v_addToCart">
																						            <div class="v_priceAndButton">
																						                <p class="v_productPrice"><?= wc_price($product->price)?> </p>
																						                <div class="product-add pull-right" style="position: relative;">

																									        <button class="btn btn-icon btn-block btn-green btn-icon-right btn-coffee-origin"
																									                data-id="<?=$product->post_id?>"
																									                data-cart="true"
																									                data-name="<?=$product->name?>"
																									                data-price="<?=$product->price?>"
																									                data-image-url="<?=$product->image ? $product->image[0] : ''?>"
																									                data-type="<?=$product->product_type?>"
																									                data-vat="1"
																									                data-qty-step="10"
																									                data-url="<?= $product->url ?>"
																									                data-aromatic-profile="<?= $product->aromatic_profile?>"
																									            >
																									            <i class="icon-basket"></i><span class="btn-add-qty"></span><span class="text">Add to basket</span>&nbsp;<i class="icon-plus text"></i>
																									        </button>

																									    </div>
																						            </div>
																						        </div>
																						    </div>
																						</div>
																	                </div>
																	            </div>
																	        </div>
																	    </div>
																	</div>
							                                    </div>
					                                        </li>
					                        			<?php
					                        			endif;
					                        		endforeach;
				                        		endforeach; ?>
			                                    </ul>
			                                </div>
			                            </li>
		                        	<?php endforeach ?>
		                        </ul>
		                    </li>
		                <?php endforeach; ?>
		                </ul>
		            </div>
		        </div>
		    </div>
		</section>
		<!-- End Aromatic Profile Section -->
		<section class="vue_magicContent v_discover v_sectionCenter v_sectionOverflow" pv-height="auto" id="discover" data-label="Discover ranges">
		    <a href="#discover" class="v_btnRoundScroll"> <i class="fn_angleDown"></i> </a>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">Discover our Coffee Ranges</h2>
		                <p class="v_headline wow" data-wow=""><strong class="v_brand" term="nespresso">Nespresso</strong> only selects the best beans from the world’s finest crops, known as “Gourmet Coffees”, as they offer the most sophisticated aromas.</p>
		                <div class="v_row3 v_gridList" slot="content">
		                    <div class="v_gridItem">
		                        <a class="v_imageContainer v_imageRound" data-wow="" href="javascript:void(0)" data-wow-delay="0">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/intenso_XL.jpg');" lazy="loaded"></div>
		                        </a>
		                        <h3 data-wow="" data-wow-delay="0">Intenso</h3>
		                        <p data-wow="" data-wow-delay="0">These multi-blends feature a variety of intense characteristics, best enjoyed in a short cup, 25ml (<strong class="v_brand" term="ristretto">Ristretto</strong>) or 40ml (Espresso) cup.</p>
		                        <!-- <a class="v_link v_iconLeft" data-wow="" href="#" data-wow-delay="0"> <i class="fn_arrowLink"></i> <span>Discover the range</span> </a> -->
		                    </div><div class="v_gridItem">
		                        <a class="v_imageContainer v_imageRound" data-wow="" href="javascript:void(0)" data-wow-delay="1">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/espresso_XL.jpg');" lazy="loaded"></div>
		                        </a>
		                        <h3 data-wow="" data-wow-delay="1">Espresso</h3>
		                        <p data-wow="" data-wow-delay="1">Four blends explore the range of flavours presented in a mild, round and balanced style, to be enjoyed in a 40ml cup (Espresso).</p>
		                        <!-- <a class="v_link v_iconLeft" data-wow="" href="#" data-wow-delay="1"> <i class="fn_arrowLink"></i> <span>Discover the range</span> </a> -->
		                    </div><!--
		                    <div class="v_gridItem">
		                        <a class="v_imageContainer v_imageRound" data-wow="" href="javascript:void(0)" data-wow-delay="2">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/pure-origin_XL.jpg');" lazy="loaded"></div>
		                        </a>
		                        <h3 data-wow="" data-wow-delay="2">Pure Origin</h3>
		                        <p data-wow="" data-wow-delay="2">Three Espressos (40 ml) and one Lungo (110 ml) with unique and striking characters. Each sourced by the <strong class="v_brand" term="nespresso">Nespresso</strong> Coffee Experts from a single country of origin in specially selected territories.</p>
		                    </div> -->
		                    <div class="v_gridItem">
		                        <a class="v_imageContainer v_imageRound" data-wow="" href="javascript:void(0)" data-wow-delay="0">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lungo_XL.jpg');" lazy="loaded"></div>
		                        </a>
		                        <h3 data-wow="" data-wow-delay="0">Lungo</h3>
		                        <p data-wow="" data-wow-delay="0">Three Grands Crus offering a wide range of taste profiles and intensities, specifically designed to be enjoyed in a long cup, 110ml.</p>
		                        <!-- <a class="v_link v_iconLeft" data-wow="" href="#" data-wow-delay="0"> <i class="fn_arrowLink"></i> <span>Discover the range</span> </a> -->
		                    </div><div class="v_gridItem">
		                        <a class="v_imageContainer v_imageRound" data-wow="" href="javascript:void(0)" data-wow-delay="1">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/decaffeinato_XL.jpg');" lazy="loaded"></div>
		                        </a>
		                        <h3 data-wow="" data-wow-delay="1">Decaffeinato</h3>
		                        <p data-wow="" data-wow-delay="1">True <strong class="v_brand" term="nespresso">Nespresso</strong> coffees, each with its own rich aroma and natural characters preserved through the decaffeination process.</p>
		                        <!-- <a class="v_link v_iconLeft" data-wow="" href="/tw/en/coffee-range#!/decaffeinato" data-wow-delay="1"> <i class="fn_arrowLink"></i> <span>Discover the range</span> </a> -->
		                    </div>
		                    <!-- <div class="v_gridItem">
		                        <a class="v_imageContainer v_imageRound" data-wow="" href="javascript:void(0)" data-wow-delay="2">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/variations_XL.jpg');" lazy="loaded"></div>
		                        </a>
		                        <h3 data-wow="" data-wow-delay="2">Variations</h3>
		                        <p data-wow="" data-wow-delay="2">Three gourmet variations on the <strong class="v_brand" term="livanto">Livanto</strong> Grand Cru, flavoured with natural aromas of exceptional finesse. Best enjoyed in a short cup, 40ml.</p>

		                    </div> -->
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_testimonial v_sectionLight v_sectionAutoHeight" id="testimonial1" data-label="Testimonial 1">
		    <div class="bg_normal" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/background_XL.jpg');" lazy="loaded"></div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_tableRow">
		                <div class="v_col20">
		                    <div class="v_image wow" data-wow="" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/testimonial1_XL.jpg');" lazy="loaded"></div>
		                </div>
		                <div class="v_col80">
		                    <div class="v_wysiwyg v_quote wow" data-wow="" data-wow-delay="1">For each of their Grands Crus, <strong class="v_brand" term="nespresso">Nespresso</strong> select only some of the best beans in the world.</div>
		                    <p class="v_sign wow" data-wow="" data-wow-delay="2"><strong><strong class="v_brand" term="nespresso">Nespresso</strong> Quality specialist</strong>, <span>Green Coffee</span></p>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_magicContent v_excellence v_sectionLeft v_sectionBottom" pv-height="auto" id="excellence" data-label="Excellence">
		    <div class="bg_container" pv-speed="4">
		        <div class="bg_placeholder bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/excellence_placeholder_L.jpg');"></div>
		        <div class="bg_full bg-xl" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/excellence_XL.jpg');" lazy="loaded"></div>

		        <div class="bg_placeholder bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/excellence_placeholder_S.jpg');"></div>
		        <div class="bg_full bg-small" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/excellence_S.jpg');" lazy="loaded"></div>
		    </div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">The Excellence of a Coffee</h2>
		                <p class="v_headline wow" data-wow="">Our passion for perfection is truly reflected in our unsurpassed coffee quality.</p>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p>We leave nothing to chance. Quality is rigorously managed at every step of the <strong class="v_brand" term="nespresso">Nespresso</strong> value chain, from the sustainable sourcing of highest quality coffee from the world’s most well-renowned coffee growing regions to the distribution to consumers worldwide.</p>
		                </div>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p>Our range of exceptional blends and pure, single origin coffee is specially developed to suit taste preference.</p>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_photo v_parallax" id="lifestyle1" data-label="lifestyle1" style="height: calc(30em + 15vw);">
		    <div class="bg_container v_parallaxLayer" pv-speed="4" style="transform: translate3d(0px, 61.8708px, 0px);">
		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lifestyle1_placeholder_L.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lifestyle1_XL.jpg');" lazy="loaded"></div>
		    </div>
		</section>
		<section class="vue_magicContent v_cupSizesSection v_sectionCenter v_sectionOverflow" pv-height="auto" id="cup-sizes" data-label="Cup Sizes">
		    <a href="#cup-sizes" class="v_btnRoundScroll"> <i class="fn_angleDown"></i> </a>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">Cup Sizes</h2>
		                <p class="v_headline wow" data-wow="">Each Grand Cru is developed in a given cup size to deliver an optimal taste and texture.</p>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p>Over-extraction: cup size is critical as too much water through too few grounds results in unbalanced flavour extraction.</p>
		                </div>
		                <div class="v_row4 v_gridList" slot="content">
		                    <div class="v_gridItem">
		                        <div class="v_imageContainer wow" data-wow="" data-wow-delay="0">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/ristretto_XL.png');" lazy="loaded"></div>
		                        </div>
		                        <h3 data-wow="" data-wow-delay="0" class="wow">Ristretto</h3>
		                        <p data-wow="" data-wow-delay="0" class="wow">25 ml</p>
		                    </div><div class="v_gridItem">
		                        <div class="v_imageContainer wow" data-wow="" data-wow-delay="1">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/espresso_XL.png');" lazy="loaded"></div>
		                        </div>
		                        <h3 data-wow="" data-wow-delay="1" class="wow">Espresso</h3>
		                        <p data-wow="" data-wow-delay="1" class="wow">40 ml</p>
		                    </div><div class="v_gridItem">
		                        <div class="v_imageContainer wow" data-wow="" data-wow-delay="2">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lungo_XL.png');" lazy="loaded"></div>
		                        </div>
		                        <h3 data-wow="" data-wow-delay="2" class="wow">Lungo</h3>
		                        <p data-wow="" data-wow-delay="2" class="wow">110 ml</p>
		                    </div><div class="v_gridItem">
		                        <div class="v_imageContainer wow" data-wow="" data-wow-delay="3">
		                            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/americano_XL.png');" lazy="loaded"></div>
		                        </div>
		                        <h3 data-wow="" data-wow-delay="3" class="wow">Americano</h3>
		                        <p data-wow="" data-wow-delay="3" class="wow">150 ml</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_testimonial v_sectionLight v_sectionAutoHeight" id="testimonial2" data-label="Testimonial 2">
		    <div class="bg_normal" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/background_XL.jpg');" lazy="loaded"></div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_tableRow">
		                <div class="v_col20">
		                    <div class="v_image wow" data-wow="" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/testimonial2_XL.jpg');" lazy="loaded"></div>
		                </div>
		                <div class="v_col80">
		                    <div class="v_wysiwyg v_quote wow" data-wow="" data-wow-delay="1">In our AAA Sustainable Quality Program, we’re learning together with our farmers how to improve their farms. At the end of the day, it’s the farmers who best understand their coffee trees.</div>
		                    <p class="v_sign wow" data-wow="" data-wow-delay="2"><strong>JUAN DIEGO</strong>, <span><strong class="v_brand" term="nespresso">Nespresso</strong> AAA Manager in Costa Rica</span></p>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_magicContent v_intensity v_sectionRight v_sectionBottom" pv-height="auto" id="intensity" data-label="Intensity">
		    <div class="bg_container" pv-speed="4">
		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/intensity_placeholder_L.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/intensity_XL.jpg');" lazy="loaded"></div>
		    </div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">The Coffee Intensity</h2>
		                <p class="v_headline wow" data-wow="">Intensity results from the combination of:</p>
		                <div class="vue_accordion" slot="content">
		                    <ul>
		                        <li>
		                            <div class="v_title"> <i class="fn_tickCircle"></i> Body </div>
		                            <div class="v_wysiwyg">
		                                <p>Coffee fullness or weight in the mouth. A coffee with body is thick and dense, whereas a light-bodied coffee is perceived as watery, fluid.</p>
		                            </div>
		                        </li>
		                        <li>
		                            <div class="v_title"> <i class="fn_tickCircle"></i> Bitterness </div>
		                            <div class="v_wysiwyg">
		                                <p>Think of dark chocolate, baking cocoa, aspirin, chicory. Coffee is bitter.</p>
		                            </div>
		                        </li>
		                        <li>
		                            <div class="v_title"> <i class="fn_tickCircle"></i> Roastiness </div>
		                            <div class="v_wysiwyg">
		                                <p>Perception of a coffee roast flavor, independent of the roast level.</p>
		                            </div>
		                        </li>
		                    </ul>
		                </div>
		                <div class="v_wysiwyg" data-wow="">
		                    <p>At <strong class="v_brand" term="nespresso">Nespresso</strong>, we define intensity as being a combination of roastiness, body and bitterness, not caffeine content.</p>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_photo v_parallax" id="lifestyle2" data-label="lifestyle2" style="height: calc(30em + 15vw);">
		    <div class="bg_container v_parallaxLayer" pv-speed="4" style="transform: translate3d(0px, 144.721px, 0px);">
		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lifestyle2_placeholder_L.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lifestyle2_XL.jpg');" lazy="loaded"></div>
		    </div>
		</section>
		<section class="vue_magicContent v_aromaticNotes v_sectionCenter v_sectionOverflow" pv-height="auto" id="aromatic-notes" data-label="Aromatic Notes">
		    <a  href="#aromatic-notes" class="v_btnRoundScroll"> <i class="fn_angleDown"></i> </a>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow">Aromatic Notes</h2>
		                <p class="v_headline wow" data-wow="">Dominant aromatic notes define the aromatic profile – the character of the coffee, regardless of its intensity.</p>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p>Aromatic notes are clustered into 3 main families:</p>
		                </div>
		                <div class="v_row3 v_gridList" slot="content">
		                    <div class="v_gridItem">
		                        <div class="v_imageContainer v_imageRound wow" data-wow="" data-wow-delay="0">
		                            <div class="v_image v_imageRound" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/fruity_XL.jpg');" lazy="loaded"></div>
		                        </div>
		                        <h3 data-wow="" data-wow-delay="0" class="wow">Fruity</h3>
		                        <p data-wow="" data-wow-delay="0" class="wow">Fruity winy, Flowery, Citrus</p>
		                    </div><div class="v_gridItem">
		                        <div class="v_imageContainer v_imageRound wow" data-wow="" data-wow-delay="1">
		                            <div class="v_image v_imageRound" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/balanced_XL.jpg');" lazy="loaded"></div>
		                        </div>
		                        <h3 data-wow="" data-wow-delay="1" class="wow">Balanced</h3>
		                        <p data-wow="" data-wow-delay="1" class="wow">Biscuits, Cereals, Roasted, Honey</p>
		                    </div><div class="v_gridItem">
		                        <div class="v_imageContainer v_imageRound wow" data-wow="" data-wow-delay="2">
		                            <div class="v_image v_imageRound" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/intense_XL.jpg');" lazy="loaded"></div>
		                        </div>
		                        <h3 data-wow="" data-wow-delay="2" class="wow">Intense</h3>
		                        <p data-wow="" data-wow-delay="2" class="wow">Spicy, Malted, Woody, Cocoa, Intensely roasted</p>
		                    </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="v_sectionAutoHeight v_sectionLight vue_tryCoffeeSelector" id="try-coffee-selector" data-label="Try coffee selector">
		    <div class="bg_normal" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/background_XL.jpg');" lazy="loaded"></div>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_image" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/tryCoffeeSelector_XL.png');" lazy="loaded"></div>
		            <h2>Find the range that suits you best<br> and enjoy the ultimate coffee experience.</h2>
		            <a class="v_btn v_btnOutline" href="/coffee-list"> <span>Order your capsules</span> </a>
		        </div>
		    </div>
		</section>
		<section class="vue_photo v_parallax" id="lifestyle3" data-label="lifestyle3" style="height: calc(30em + 15vw);">
		    <div class="bg_container v_parallaxLayer" pv-speed="4" style="transform: translate3d(0px, 150.146px, 0px);">
		        <div class="bg_placeholder" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lifestyle3_placeholder_L.jpg');"></div>
		        <div class="bg_full" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/lifestyle3_XL.jpg');" lazy="loaded"></div>
		    </div>
		</section>
		<section class="vue_magicContent v_decafProcess v_sectionCenter v_sectionOverflow v_sectionAutoHeight" pv-height="auto" id="decaffeination-process" data-label="Decaffeination process">
		    <a href="#decaffeination-process" class="v_btnRoundScroll"> <i class="fn_angleDown"></i> </a>
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_text">
		                <h2 data-wow="" class="wow"><strong class="v_brand" term="nespresso">Nespresso</strong>’s decaffeination process</h2>
		                <p class="v_headline wow" data-wow="">Our passion for perfection is truly reflected in our unsurpassed coffee quality.</p>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p><strong class="v_brand" term="nespresso">Nespresso</strong>’s expertise in blending and roasting allows to recreate the same character and aromas of your favorite GC, in a decaffeinated version.</p>
		                </div>
		                <div class="v_wysiwyg wow" data-wow="">
		                    <p><strong class="v_brand" term="nespresso">Nespresso</strong> has chosen decaffeination methods that use only natural ingredients and respect the environment and the coffee bean true nature.</p>
		                </div>
		            </div>
		        </div>
		    </div>
		</section>
		<section class="vue_navigationBottom v_sectionAutoHeight">
		    <div class="v_sectionRestrict">
		        <div class="v_sectionContent">
		            <div class="v_nav">
		                <p class="v_visually_hidden"></p>
		                <ul class="v_row">
		                    <li>
		                        <a href="/coffee-origins">
		                            <div class="v_thumb" style="background-image: url('/wp-content/themes/storefront-child/images/coffee-origins/coffee-range/introduction_thumb2.jpg');" lazy="loaded"></div> <span>Coffee Origins</span> </a>
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </div>
		</section>
	</div>
</main>
