<?php
/**
 * Nespresso Theme developed by Minion Solutions
 * Child theme of Storefront
 * This file is intended to use anything to do with admin (backend)
 *
 * @link https://minionsolutions.com
 *
 * @package Minion Solutions
 * @subpackage Nespresso
 * @since 1.0
 * @version 1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}


if ( isset($_POST['product_color_variant_image_id_remove']) ) :

	$post_id = sanitize_text_field($_POST['post_ID']);

	if ( $post_id ) :

		$remove_image_ids = $_POST['product_color_variant_image_id_remove'];

		if ( is_array($remove_image_ids) ) :

			$product_color_variant = get_post_meta( $post_id, 'product_color_variant');
			$product_color_variant = $product_color_variant ? $product_color_variant[0] : [];

			foreach ( $remove_image_ids as $image_id ) :
				$image_id = sanitize_text_field($image_id);
				unset( $product_color_variant[$image_id] );
			endforeach;

			delete_post_meta( $post_id, 'product_color_variant');
			update_post_meta( $post_id, 'product_color_variant', $product_color_variant);

		endif;
	endif;
endif;

// hack for updating product variant color
if ( isset($_POST['product_color']) &&
	isset($_POST['product_color_variant_image_id'])
) :
	$post_id = sanitize_text_field($_POST['post_ID']);


	if ( $post_id ) :

		$product_color = sanitize_text_field($_POST['product_color']);
		$product_color_variant_image_id = sanitize_text_field($_POST['product_color_variant_image_id']);
		$product_color_variant_previous_image_id = sanitize_text_field($_POST['product_color_variant_previous_image_id']);

		if ( $product_color && $product_color_variant_image_id ) :

			$product_color_variant = get_post_meta( $post_id, 'product_color_variant');

			$product_color_variant = $product_color_variant ? $product_color_variant[0] : [];

			if ( $product_color_variant_previous_image_id )
				unset( $product_color_variant[$product_color_variant_previous_image_id] );

			$new_color_variant = [];

			if ( $product_color_variant ) :
				foreach ( $product_color_variant as $k => $variant ) :
					if ( $k == $product_color_variant_image_id ) {
						$product_color_variant[$k] = [
							'color' => $product_color,
							'image_id' => $product_color_variant_image_id
						];
						unset($product_color_variant_image_id);
					}
				endforeach;
			endif;

			if ( isset($product_color_variant_image_id) ) {
				$product_color_variant[$product_color_variant_image_id] = [
					'color' => $product_color,
					'image_id' => $product_color_variant_image_id
				];
			}

			delete_post_meta( $post_id, 'product_color_variant');

			update_post_meta( $post_id, 'product_color_variant', $product_color_variant);

		endif;

	endif;

endif;

/**
 * add a custom metabox about color variants of the product
 */
add_action( 'add_meta_boxes', 'nespresso_machine_group_meta_box' );
function nespresso_machine_group_meta_box() {

    global $woocommerce, $order, $post;

    add_meta_box( 'nespresso-container-machine-group', __('Machine Group'), 'nespresso_machine_group_box', 'product', 'normal', 'high' );
}
function nespresso_machine_group_box( $post ) {
	include __DIR__ . '/../components/meta-boxes/machine-group.php';
}

/**
 * add hook scripts for products update or create
 *
 * @param  string $hook - url filename
 * @return void
 */
function enqueue_products_update_or_create_scripts($hook) {

	// make sure for post and post new only
    if ( 'post.php' != $hook &&
    	'post-new.php' != $hook
    ) {
        return;
    }

    wp_enqueue_script('jquery');

    /**
     * add the scripts on footer
     */
	function footer_products_update_or_create_scripts() {

		global $product_types;

		$product_types = json_encode($product_types);

		$script_src = get_template_directory_uri() . '/js/products-update-or-create-script.js';

		echo '
			<script type="text/javascript">
				(function (jQuery) {
				    window.$ = jQuery.noConflict();
				})(jQuery);
				$product_types = '.$product_types.';
			</script>
			<script type="text/javascript" src="'.$script_src.'"></script>
		';
	} // footer_products_update_or_create_scripts()

    add_action('admin_footer', 'footer_products_update_or_create_scripts');

} // enqueue_products_update_or_create_scripts()

/**
 * add the footer script
 */
// add_action( 'admin_enqueue_scripts', 'enqueue_products_update_or_create_scripts' );

function get_positive_cup() {
	// get the positive cup
	global $wpdb;
	$posts = $wpdb->get_results( $wpdb->prepare( "SELECT * FROM $wpdb->posts WHERE post_name LIKE '%s' ", '%nespresso-positive-cup%') );

	$post = $posts ? $posts[0] : null;

	$positive_cup = null;
	if ( $post ) :
	    $post_meta = get_post_meta($post->ID, '_nespresso_positive_cup');
	    $post_meta = $post_meta ? $post_meta[0] : null;
	    $positive_cup = json_decode($post_meta);
	    $positive_cup->post_id = $post->ID;
	endif;

	return $positive_cup;
}
