<?php
/**
 * Nespresso Custom Theme developed by Minion Solutions
 * Child theme of Storefront
 *
 * @link https://minionsolutions.com
 *
 * @since 1.0
 * @version 1.0
 */

?>


<?php get_header(); ?>

<?php
if ( is_checkout() ) :
$checkout_page_id = get_option( 'woocommerce_checkout_page_id' );
function hide_header_and_footer_enque_style() {?>
	<style>
/*		header#header,
		#footer
		{
			display: none;
		}*/

		#main-accessibility {
			display: none;
		}
	</style>
<?php
}

add_action( 'wp_head', 'hide_header_and_footer_enque_style' );
endif;

$baseUrl = basename(get_permalink());

?>

<?php if (in_array($baseUrl, ['cart'])): ?>
<div class="container">
<?php endif; ?>

<!-- <div id="primary" class="content-area"> -->
	<!-- <main id="main" class="site-main" role="main"> -->

		<?php while ( have_posts() ) : the_post();

			do_action( 'storefront_page_before' );

			get_template_part( 'content', 'page' );

			/**
			 * Functions hooked in to storefront_page_after action
			 *
			 * @hooked storefront_display_comments - 10
			 */
			// do_action( 'storefront_page_after' );

		endwhile; // End of the loop. ?>

	<!--</main>--><!-- #main -->
<!--</div>--><!-- #primary -->

<?php if (in_array($baseUrl, ['cart'])): ?>
</div>
<?php endif; ?>

<!-- get_footer -->
<?php get_footer(); ?>
