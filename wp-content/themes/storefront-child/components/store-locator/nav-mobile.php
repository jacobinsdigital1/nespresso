<div class="slocator-switchers">
    <div class="slocator-switchers__tab active" data-show="#slocator-locations">
        <span class="slocator-switchers__icon fa fa-bars"></span>List
    </div>
    <div class="slocator-switchers__tab" data-show="#slocator-container-map">
        <span class="slocator-switchers__icon fa fa-map-o"></span>Map
    </div>
</div>
